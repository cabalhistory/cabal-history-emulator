﻿using System;
using System.Net;
using Shared.Packet;
using Shared.Telepathy;
using LoginServer.Daemons.LoginServer.Logic;
using LoginServer.Daemons.LoginServer;
using Shared.Extensions;
using Shared.Context;
using static Shared.Protodef;

namespace LoginServer
{
    class LoginServerDaemon : ServerDaemon
    {
        public LoginServerDaemon(IPAddress sv_addr, ushort sv_port, ushort sv_threads, ushort sv_frequency)
            : base(sv_addr, sv_port, sv_threads, sv_frequency)
        {
            // Execute before server starts

            // Register Packets
            Procs.Register(_packetsHandler);
        }

        protected override void OnConnect(Connection connection)
        {
            base.OnConnect(connection);
        }

        protected override void OnReceiveData(Connection connection, PacketReader reader)
        {
            base.OnReceiveData(connection, reader);

            // Execute logic
            _packetsHandler.Execute(connection, reader);
        }

        protected override void OnDisconnect(Connection connection)
        {
            // Queue save user data
            // Address not available in this time because TcpClient has closed and disposed
            if (Auth.authenticatedConnections.TryRemove(connection.CID, out Connection removedConnection))
            {
                if(removedConnection.GetAccountContext(out AccountContext accountCtx))
                {
                    var ipsSetAccountStt = new IPS_SET_ACCOUNT_STT();
                    ipsSetAccountStt.UserNum = accountCtx.UserNum;
                    ipsSetAccountStt.GroupIdx = 0;
                    ipsSetAccountStt.PlayTime = accountCtx.PlayTime;

                    Program.GlobalDBAgentClientDaemon.Send(Shared.Opcode.IPC_SET_ACCOUNT_STT, ipsSetAccountStt);
                }
            }

            base.OnDisconnect(connection);
        }
    }
}
