﻿using System.Timers;

namespace LoginServer.Daemons.GlobalMgrClient.Logic
{
    static class ServerList
    {
        public static byte[] ServerListData = new byte[0];

        static Timer _serverListPoolTimer;

        public static void StartPool()
        {
            _serverListPoolTimer = new Timer(12000);
            _serverListPoolTimer.Elapsed += RequestServerList;
            _serverListPoolTimer.Start();
        }

        public static void StopPool()
        {
            _serverListPoolTimer.Stop();
        }

        public static void RequestServerList(object sender, ElapsedEventArgs e)
        {
            Program.GlobalMgrClientDaemon.Send(Shared.Opcode.IPC_SERVERLIST, new byte[0]);
        }

        public static void SetServerListData(byte[] serverListData)
        {
            ServerListData = serverListData;
        }
    }
}
