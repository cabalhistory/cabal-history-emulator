﻿using Shared.Packet;
using LoginServer.Daemons.GlobalMgrClient.Proc;

namespace LoginServer.Daemons.GlobalMgrClient.IPC
{
    static class Procs
    {
        public static void Register(PacketHandler packetsHandler)
        {
            packetsHandler.Register(Shared.Opcode.IPC_CONNECT, IPCConnection.OnIPC_CONNECT);

            packetsHandler.Register(Shared.Opcode.IPC_SERVERLIST, GameServer.OnIPC_SERVERLIST);

            packetsHandler.Register(Shared.Opcode.IPC_REQ_LINK, Link.OnIPC_REQ_LINK);
            packetsHandler.Register(Shared.Opcode.IPC_LINK, Link.OnIPC_LINK);
        }
    }
}
