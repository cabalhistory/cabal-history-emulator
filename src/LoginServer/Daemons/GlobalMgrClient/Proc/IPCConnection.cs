﻿using Shared;
using Shared.Packet;
using LoginServer.Daemons.GlobalMgrClient.Logic;
using static Shared.Protodef;

namespace LoginServer.Daemons.GlobalMgrClient.Proc
{
    static class IPCConnection
    {
        public static void OnIPC_CONNECT(PacketReader reader)
        {
            Logger.Log("GlobalMgrClient IAC authenticated, registering service...");

            var ipsRegisterChannel = new IPS_REGISTER_SERVICE();

            ipsRegisterChannel.GroupIdx = 128;
            ipsRegisterChannel.ServerIdx = Config.ServerIdx;

            ipsRegisterChannel.IP = Config.IP.GetAddressBytes();
            ipsRegisterChannel.Port = Config.Port;

            ipsRegisterChannel.MaxConnections = ushort.MaxValue;

            ipsRegisterChannel.Type = 0;

            Program.GlobalMgrClientDaemon.Send(Shared.Opcode.IPC_REGISTER_SERVICE, ipsRegisterChannel);

            ServerList.StartPool();
        }
    }
}
