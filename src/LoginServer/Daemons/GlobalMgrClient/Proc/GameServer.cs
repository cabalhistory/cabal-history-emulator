﻿using System;
using LoginServer.Daemons.GlobalMgrClient.Logic;
using Shared.Packet;

namespace LoginServer.Daemons.GlobalMgrClient.Proc
{
    static class GameServer
    {
        public static void OnIPC_SERVERLIST(PacketReader reader)
        {
            byte[] serverListData = new byte[reader.Payloadsize];
            Array.ConstrainedCopy(reader.Data, reader.Headersize, serverListData, 0, reader.Payloadsize);

            ServerList.SetServerListData(serverListData);
        }
    }
}
