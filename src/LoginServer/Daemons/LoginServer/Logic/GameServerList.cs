﻿using System.Timers;
using LoginServer.Daemons.GlobalMgrClient.Logic;

namespace LoginServer.Daemons.LoginServer.Logic
{
    static class GameServerList
    {
        static Timer _broadcastPoolTimer;
        
        public static void StartBroadCastPool()
        {
            _broadcastPoolTimer = new Timer(7000);
            _broadcastPoolTimer.Elapsed += BroadCastServerList;

            _broadcastPoolTimer.Start();
        }

        public static void StopBroadCastPool()
        {
            _broadcastPoolTimer.Stop();
        }

        public static void SendServerListToConnection(ushort connectionId)
        {
            Program.LoginServerDaemon.Send(Opcode.NFY_SERVERSTATE, connectionId, ServerList.ServerListData);
        }

        private static void BroadCastServerList(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            Program.LoginServerDaemon.Send(Opcode.NFY_SERVERSTATE, Auth.authenticatedConnections.Keys, ServerList.ServerListData);
        }
    }
}
