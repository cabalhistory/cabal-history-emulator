﻿using Shared.Packet;
using Shared.Telepathy;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using static LoginServer.Protodef;

namespace LoginServer.Daemons.LoginServer.Logic
{
    static class Auth
    {
        public static ConcurrentDictionary<ushort, Connection> authenticatedConnections = new ConcurrentDictionary<ushort, Connection>();

        public static void SendLoginTimer(ushort cid)
        {
            NFY_LOGINTIMER s2cPacket = new NFY_LOGINTIMER();
            s2cPacket.Milliseconds = Config.LoginTimer;

            Program.LoginServerDaemon.Send(Opcode.NFY_LOGINTIMER, cid, s2cPacket);
        }

        public static void SendURLs(ushort cid)
        {
            NFY_URLTOCLIENT s2cPacket = new NFY_URLTOCLIENT();
            s2cPacket.Unk0 = 326;
            s2cPacket.Unk1 = 324;
            s2cPacket.URLCashLen = Config.CashShopURL.Length;
            s2cPacket.URLCash = Config.CashShopURL;
            s2cPacket.Unk2 = 0;
            s2cPacket.URLChargeLen = Config.CashChargeURL.Length;
            s2cPacket.URLCharge = Config.CashChargeURL;
            s2cPacket.URLGuildLen = Config.GuildURL.Length;
            s2cPacket.URLGuild = Config.GuildURL;
            s2cPacket.Unk3 = 0;

            Program.LoginServerDaemon.Send(Opcode.NFY_URLTOCLIENT, cid, s2cPacket);
        }

        public static void SendSystemMessage(ushort cid)
        {
            // Response
            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.NFY_SYSTEMMESSG);

                builder.Write((byte)0x09); // Unknown ( maybe message type or id ? )
                builder.Write(new byte[2]); // Empty space

                Program.LoginServerDaemon.Send(cid, builder.Data);
            }
        }
    }
}
