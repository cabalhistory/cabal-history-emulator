﻿using Shared;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace LoginServer.Daemons.LoginServer.Logic
{
    static class SubpasswordReminder
    {
        // UserNum => Reminder
        static Timer _freePool;
        static ConcurrentDictionary<int, Reminder> _trustedUsers = new ConcurrentDictionary<int, Reminder>();

        public static bool Register(int userNum, string ipAddress, int interval)
        {
            try
            {
                if (_trustedUsers.TryAdd(userNum, new Reminder(ipAddress, interval)))
                    return true;

                _trustedUsers[userNum].IP = ipAddress;
                _trustedUsers[userNum].AddTime(interval);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static int GetExpiration(int userNum, string ipAddress)
        {
            if (!_trustedUsers.TryGetValue(userNum, out Reminder reminder))
                return 0;

            if (reminder.IP != ipAddress)
                return 0;

            return reminder.UntilUnixTimestamp;
        }

        public static void StartFreePool()
        {
            _freePool = new Timer(1000);
            _freePool.Elapsed += FreeOldSubpasswordsMemory;
            _freePool.Enabled = true;
        }

        public static void StopFreePool()
        {
            _freePool.Enabled = false;
        }

        static void FreeOldSubpasswordsMemory(object sender, ElapsedEventArgs e)
        {
            foreach(var old in _trustedUsers.Where(_ => _.Value.UntilUnixTimestamp < Utils.UnixTimestamp))
            {
                ((IDictionary<int, Reminder>)_trustedUsers).Remove(old.Key);
            }
        }
    }

    internal class Reminder
    {
        public string IP;
        public int UntilUnixTimestamp;

        public Reminder(string ipAddress, int interval)
        {
            IP = ipAddress;

            AddTime(interval);
        }

        public void AddTime(int interval)
        {
            UntilUnixTimestamp = Utils.ConvertDateTimeToUnixTimestamp(DateTime.Now.AddHours(interval));
        }
    }
}
