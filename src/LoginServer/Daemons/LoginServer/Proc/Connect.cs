﻿using System;
using Shared;
using Shared.Packet;
using Shared.Telepathy;
using static LoginServer.Protodef;
using static Shared.Protodef;

namespace LoginServer.Daemons.LoginServer.Proc
{
    static class Connect
    {
        const uint _key = 0x4C4DE297;
        const ushort _step = 0x1FFC;

        public static void OnCSC_CONNECT2SVR(Connection connection, PacketReader reader)
        {
            var c2sPacket = reader.Packet<C2S_CONNECT2SERV>();

            // Change connection encryption
            connection.Cryption.ChangeKeychain(_key, _step);

            var s2cPacket = new S2C_CONNECT2SERV();
            s2cPacket.AuthKey = connection.Tick;
            s2cPacket.Index = connection.CID;
            s2cPacket.RecvXorKeyIdx = _step;
            s2cPacket.Recv2ndXorSeed = _key;

            connection.Send(Opcode.CSC_CONNECT2SVR, s2cPacket);
        }

        public static void OnCSC_CHECKVERSION(Connection connection, PacketReader reader)
        {
            var c2sPacket = reader.Packet<C2S_CHECKVERSION>();

            var s2cPacket = new S2C_CHECKVERSION();
            s2cPacket.ServerVersion = Config.IgnoreClientVersion ? c2sPacket.Version : Config.ClientVersion;
            s2cPacket.Debug = 0;
            s2cPacket.Reserved0 = 0;
            s2cPacket.Reserved1 = 0;

            connection.Send(Opcode.CSC_CHECKVERSION, s2cPacket);
        }
    }
}
