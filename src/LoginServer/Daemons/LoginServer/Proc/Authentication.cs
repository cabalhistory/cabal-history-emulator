﻿using System;
using System.Text;
using Shared.Packet;
using Shared.Telepathy;
using static Shared.Protodef;
using static LoginServer.Protodef;
using Shared;

namespace LoginServer.Daemons.LoginServer.Proc
{
    static class Authentication
    {
        public static void OnCSC_PRE_AUTHENTICATE(Connection connection, PacketReader reader) // Maybe ImageAuth function check!!!!
        { 
            var c2sPacket = reader.Packet<C2S_PRE_AUTHENTICATE>();

            var s2cPacket = new S2C_PRE_AUTHENTICATE();

            string username = c2sPacket.Username;

            connection.Log($"Attemping to login with ID: {username}");

            connection.Send(Opcode.CSC_PRE_AUTHENTICATE, s2cPacket);
        }

        public static void OnCSC_RSA_PUBLIC_KEY(Connection connection, PacketReader packet)
        {
            var s2cPacket = new S2C_RSA_PUBLIC_KEY();

            // Init RSA encryption for ingame credentials encrypt
            connection.InitRSA();

            s2cPacket.Status = 1;
            s2cPacket.KeyLength = (ushort)connection.RSA.PublicKey.Length;
            s2cPacket.PublicKey = connection.RSA.PublicKey;

            connection.Send(Opcode.CSC_RSA_PUBLIC_KEY, s2cPacket);
        }

        public static void OnCSC_PREAUTH_STATUS(Connection connection, PacketReader packet)
        {
            // Response
            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_PREAUTH_STATUS);

                builder.Write(0); // Server status (0 = ok, 1 = ? .... )

                connection.Send(builder);
            }

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.S2C_UNK2005);

                builder.Write(120000); // Login timer???? see packet opcode 2009
                builder.Write(new byte[5]);

                connection.Send(builder);
            }
        }

        public static void OnCSC_AUTHENTICATE(Connection connection, PacketReader reader)
        {
            var c2sPacket = reader.Packet<C2S_AUTHENTICATE>();

            byte[] details = connection.RSA.Decrypt(c2sPacket.EncryptedData);

            byte[] tmpUsername = new byte[Typedef.MAX_ACCOUNT_USERNAME_LEN];
            byte[] tmpPassword = new byte[Typedef.MAX_ACCOUNT_PASSWORD_LEN];

            Array.Copy(details, tmpUsername, Typedef.MAX_ACCOUNT_USERNAME_LEN);
            Array.Copy(details, Typedef.MAX_ACCOUNT_USERNAME_LEN, tmpPassword, 0, Typedef.MAX_ACCOUNT_PASSWORD_LEN);

            var ipsGameAuthAccount = new IPS_GAME_AUTH_ACCOUNT();
            ipsGameAuthAccount.FromLoginTick = connection.Tick;
            ipsGameAuthAccount.FromLoginCID = connection.CID;
            ipsGameAuthAccount.Username = Encoding.UTF8.GetString(tmpUsername);
            ipsGameAuthAccount.Password = Encoding.UTF8.GetString(tmpPassword);
            ipsGameAuthAccount.IP = connection.Address.GetAddressBytes();

            Program.GlobalDBAgentClientDaemon.Send(Shared.Opcode.IPC_GAME_AUTH_ACCOUNT, ipsGameAuthAccount);
        }

        public static void Unk3383(Connection connection, PacketReader packet)
        {
            // Response
            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_UNK3383);

                builder.Write(new byte[8]);

                connection.Send(builder);
            }
        }
    }
}
