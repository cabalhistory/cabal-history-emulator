﻿using System;
using Shared.Packet;
using Shared.Telepathy;
using Shared.Extensions;
using Shared.Context;
using static Shared.Protodef;

namespace LoginServer.Daemons.LoginServer.Proc
{
    static class Verify
    {
        public static void OnCSC_VERIFYLINKS(Connection connection, PacketReader reader)
        {
            var c2sPacket = reader.Packet<C2S_VERIFYLINKS>();

            if (!connection.GetAccountContext(out AccountContext accountContext))
                return;

            // Magic Key Check
            if (!Config.IgnoreClientVersion && (c2sPacket.NormalClientMagicKey != Config.ClientMagicKey))
            {
                connection.LogError($"Bad client MagicKey (c: {c2sPacket.NormalClientMagicKey}, s: {Config.ClientMagicKey}, UserNum: {accountContext?.UserNum})");
                connection.Send(Opcode.CSC_VERIFYLINKS, new S2C_VERIFYLINKS());
                return;
            }

            var ipsLinkWorldLogin = new IPS_REQ_LINK();
            ipsLinkWorldLogin.SrcTick = connection.Tick;
            ipsLinkWorldLogin.SrcCID = connection.CID;

            ipsLinkWorldLogin.DstTick = c2sPacket.TargetTick;
            ipsLinkWorldLogin.DstCID = c2sPacket.TargetCID;

            ipsLinkWorldLogin.GroupIdx = c2sPacket.GroupIdx;
            ipsLinkWorldLogin.ServerIdx = c2sPacket.ServerIdx;
            
            ipsLinkWorldLogin.LinkData = new _IPS_LINK_DATA();
            ipsLinkWorldLogin.LinkData.UserNum = accountContext.UserNum;
            ipsLinkWorldLogin.LinkData.ID = accountContext.ID;

            ipsLinkWorldLogin.LinkData.SubpasswordExpiration = accountContext.ACSubPasswordExpiration;

            ipsLinkWorldLogin.LinkData.QtdChars = accountContext.QtdChars;
            ipsLinkWorldLogin.LinkData.CharSlotOrder = accountContext.CharSlotOrder;

            ipsLinkWorldLogin.LinkData.UseACSUB = Convert.ToByte(accountContext.UseACSUB);
            ipsLinkWorldLogin.LinkData.UseWHSUB = Convert.ToByte(accountContext.UseWHSUB);
            ipsLinkWorldLogin.LinkData.UseEQSUB = Convert.ToByte(accountContext.UseEQSUB);
            ipsLinkWorldLogin.LinkData.IsWHLOCK = Convert.ToByte(accountContext.IsWHLOCK);
            ipsLinkWorldLogin.LinkData.IsEQLOCK = Convert.ToByte(accountContext.IsEQLOCK);

            ipsLinkWorldLogin.LinkData.ServiceKind = accountContext.ServiceKind;
            ipsLinkWorldLogin.LinkData.ExpireDate = accountContext.ExpireDate;
            ipsLinkWorldLogin.LinkData.ExtendedCharCreation = accountContext.ExtendedCharCreation;

            Program.GlobalMgrClientDaemon.Send(Shared.Opcode.IPC_REQ_LINK, ipsLinkWorldLogin);
        }
    }
}
