﻿using Shared.Packet;
using LoginServer.Daemons.LoginServer.Proc;

namespace LoginServer.Daemons.LoginServer
{
    static class Procs
    {
        public static void Register(PacketHandler packetsHandler)
        {
            // All connections
            packetsHandler.Register(Opcode.CSC_CONNECT2SVR, Connect.OnCSC_CONNECT2SVR, false);
            packetsHandler.Register(Opcode.CSC_CHECKVERSION, Connect.OnCSC_CHECKVERSION, false);

            packetsHandler.Register(Opcode.CSC_PRE_AUTHENTICATE, Authentication.OnCSC_PRE_AUTHENTICATE, false);
            packetsHandler.Register(Opcode.CSC_RSA_PUBLIC_KEY, Authentication.OnCSC_RSA_PUBLIC_KEY, false);
            packetsHandler.Register(Opcode.CSC_PREAUTH_STATUS, Authentication.OnCSC_PREAUTH_STATUS, false);
            packetsHandler.Register(Opcode.CSC_AUTHENTICATE, Authentication.OnCSC_AUTHENTICATE, false);

            packetsHandler.Register(Opcode.CSC_VERIFYLINKS, Verify.OnCSC_VERIFYLINKS);

            packetsHandler.Register(Opcode.CSC_UNK3383, Authentication.Unk3383);
        }
    }
}
