﻿using Shared;
using Shared.Packet;

namespace LoginServer.Daemons.GlobalDBAgentClient.Proc
{
    static class IPCConnection
    {
        public static void OnIPC_CONNECT(PacketReader reader)
        {
            Logger.Log("GlobalDBAgentClient IAC authenticated");
        }
    }
}
