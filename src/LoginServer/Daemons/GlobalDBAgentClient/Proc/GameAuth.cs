﻿using System;
using LoginServer.Daemons.LoginServer.Logic;
using Shared.Packet;
using Shared.Telepathy;
using Shared;
using Shared.Extensions;
using Shared.Context;
using static Shared.Protodef;
using static LoginServer.Protodef;

namespace LoginServer.Daemons.GlobalDBAgentClient.Proc
{
    static class GameAuth
    {
        public static void OnIPC_GAME_AUTH_ACCOUNT(PacketReader reader)
        {
            IPS_GAME_AUTH_ACCOUNT_REPLY ipsReply = reader.Packet<IPS_GAME_AUTH_ACCOUNT_REPLY>();

            if (!Program.LoginServerDaemon.GetConnection(ipsReply.DstCID, ipsReply.DstTick, out Connection connection))
                return;

            S2C_AUTHENTICATE_HEADER s2cAuthenticateHeader = new S2C_AUTHENTICATE_HEADER();
            s2cAuthenticateHeader.Status = ipsReply.Status;
            s2cAuthenticateHeader.AuthStatus = ipsReply.AuthStatus;

            connection.Send(Opcode.CSC_AUTHENTICATE, s2cAuthenticateHeader);

            if (ipsReply.Status == 1)
            {
                // Init accountContext
                if (!connection.InitAccountContext(out AccountContext accountContext))
                    return;

                accountContext.UserNum = ipsReply.UserNum;
                accountContext.ID = ipsReply.ID;

                accountContext.UseACSUB = Convert.ToBoolean(ipsReply.UseACSUB);
                accountContext.UseWHSUB = Convert.ToBoolean(ipsReply.UseWHSUB);
                accountContext.UseEQSUB = Convert.ToBoolean(ipsReply.UseEQSUB);
                accountContext.IsWHLOCK = Convert.ToBoolean(ipsReply.IsWHLOCK);
                accountContext.IsEQLOCK = Convert.ToBoolean(ipsReply.IsEQLOCK);

                accountContext.ServiceKind = ipsReply.ServiceKind;
                accountContext.ExpireDate = ipsReply.ExpireDate;
                accountContext.ExtendedCharCreation = ipsReply.ExtendedCharCreation;

                // Send to client
                S2C_AUTHENTICATE s2cAuthenticate = new S2C_AUTHENTICATE();

                s2cAuthenticateHeader.AuthMode = 13;
                s2cAuthenticate.Header = s2cAuthenticateHeader;

                s2cAuthenticate.UserNum = ipsReply.UserNum;
                s2cAuthenticate.Unk0 = 10753;
                s2cAuthenticate.ServiceKind = ipsReply.ServiceKind;
                s2cAuthenticate.ExpireDate = ipsReply.ExpireDate;
                s2cAuthenticate.Unk3 = 1;
                s2cAuthenticate.AuthKey = ipsReply.Authkey;

                // Trust connection
                connection.Trusted = true;

                Auth.authenticatedConnections.TryAdd(connection.CID, connection);

                // Send LoginTimer
                Auth.SendLoginTimer(connection.CID);

                // Send URLs
                Auth.SendURLs(connection.CID);

                // Send authstate
                connection.Send(Opcode.CSC_AUTHENTICATE, s2cAuthenticate);

                // Send system message
                Auth.SendSystemMessage(connection.CID);

                // Send Servers list
                GameServerList.SendServerListToConnection(connection.CID);
            }
        }
    }
}
