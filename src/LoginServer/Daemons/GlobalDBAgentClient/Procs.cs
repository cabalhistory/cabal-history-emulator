﻿using Shared.Packet;
using LoginServer.Daemons.GlobalDBAgentClient.Proc;

namespace LoginServer.Daemons.GlobalDBAgentClient.IPC
{
    static class Procs
    {
        public static void Register(PacketHandler packetsHandler)
        {
            packetsHandler.Register(Shared.Opcode.IPC_CONNECT, IPCConnection.OnIPC_CONNECT);

            packetsHandler.Register(Shared.Opcode.IPC_GAME_AUTH_ACCOUNT, GameAuth.OnIPC_GAME_AUTH_ACCOUNT);
        }
    }
}
