﻿using System;
using System.IO;
using LoginServer.Daemons.GlobalMgrClient.Logic;
using LoginServer.Daemons.LoginServer.Logic;
using Shared;

namespace LoginServer
{
    class Program
    {
        public static LoginServerDaemon LoginServerDaemon { get; private set; }
        public static GlobalMgrClientDaemon GlobalMgrClientDaemon { get; private set; }
        public static GlobalDBAgentClientDaemon GlobalDBAgentClientDaemon { get; private set; }

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledException.HandleUnhandledException;

            // Load .ini configuration
            Config.Load($"LoginServer.ini");

            // Start logger service
            Logger.Start("LoginServer", Config.LogPath, Config.LogLevel);

            // GlobalMgr Daemon
            GlobalMgrClientDaemon = new GlobalMgrClientDaemon(Config.GMSIP, Config.GMSPort, Config.GMSConnectionTimeOut);

            GlobalMgrClientDaemon.OnStop += (sender, e) =>
            {
                ServerList.StopPool();
            };

            // GlobalDBAgent Daemon
            GlobalDBAgentClientDaemon = new GlobalDBAgentClientDaemon(Config.GDAIP, Config.GDAPort, Config.GDAConnectionTimeOut);
            GlobalDBAgentClientDaemon.PersistentConnection = true;
            
            // LoginServer Daemon
            LoginServerDaemon = new LoginServerDaemon(Config.IP, Config.Port, Config.Threads, Config.Frequency);

            LoginServerDaemon.OnStart += (sender, e) => 
            {
                GameServerList.StartBroadCastPool();
                SubpasswordReminder.StartFreePool();
            };

            LoginServerDaemon.OnStop += (sender, e) =>
            {
                GameServerList.StopBroadCastPool();
                SubpasswordReminder.StopFreePool();
            };

            // Register Daemons and start them all
            DaemonManager.Register(GlobalMgrClientDaemon);
            DaemonManager.Register(GlobalDBAgentClientDaemon);
            DaemonManager.Register(LoginServerDaemon);
            DaemonManager.StartAll();
        }
    }
}
