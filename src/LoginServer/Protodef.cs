﻿using Shared;
using Shared.Packet;

namespace LoginServer
{
    public static class Protodef
    {
        #region CSC_CHECKVERSION
        public struct C2S_CHECKVERSION
        {
            public int Version; 
        }

        public struct S2C_CHECKVERSION
        {
            public int ServerVersion;
            public int Debug;
            public int Reserved0;
            public int Reserved1;
        }
        #endregion

        #region CSC_PRE_AUTHENTICATE
        public struct C2S_PRE_AUTHENTICATE
        {
            [Field(Length = 35)]
            public string Username;
        }
        
        public struct S2C_PRE_AUTHENTICATE
        {
            [Field(Length = 4108)]
            public byte[] Unk0;
        }
        #endregion

        #region CSC_RSA_PUBLIC_KEY
        public struct S2C_RSA_PUBLIC_KEY
        {
            public byte Status;

            public ushort KeyLength;

            [Field(LengthFromField = "KeyLength")]
            public byte[] PublicKey;
        }
        #endregion

        #region CSC_AUTHENTICATE
        public struct C2S_AUTHENTICATE
        {
            public int Unk0;

            [Field(Length = (Typedef.RSA_KEYLENGTH / 8))]
            public byte[] EncryptedData;

        }

        public struct S2C_AUTHENTICATE_HEADER
        {
            public byte Status;

            public int Unk0;

            public int AuthMode;
            public int AuthStatus;
        }

        public struct S2C_AUTHENTICATE
        {
            public S2C_AUTHENTICATE_HEADER Header;

            public int UserNum;

            public int Unk0; // Aways 10753 ?

            [Field(Length = 6)]
            public byte[] Unk1;

            public int ServiceKind;
            public int ExpireDate;

            [Field(Length = 5)]
            public byte[] Unk2;

            public long Unk3; // Aways 1 ?

            [Field(Length = 32)]
            public string AuthKey;

            [Field(Length = 259)]
            public byte[] Unk16;
        }
        #endregion

        #region NFY_LOGINTIMER
        public struct NFY_LOGINTIMER
        {
            public int Milliseconds;

        }
        #endregion

        #region NFY_URLTOCLIENT
        public struct NFY_URLTOCLIENT
        {
            public short Unk0;
            public short Unk1;

            public int URLCashLen;

            [Field(LengthFromField = "URLCashLen")]
            public string URLCash;

            public int Unk2;

            public int URLChargeLen;

            [Field(LengthFromField = "URLChargeLen")]
            public string URLCharge;

            public int URLGuildLen;

            [Field(LengthFromField = "URLGuildLen")]
            public string URLGuild;

            public int Unk3;
        }
        #endregion
    }
}
