﻿using Shared;
using System;
using System.Net;

namespace LoginServer
{
    class Config
    {
        public static IPAddress IP { get; set; }
        public static ushort Port { get; set; }

        public static Logger.LOG_LEVEL LogLevel { get; set; }
        public static string LogPath { get; set; }
        public static ushort Threads { get; set; }
        public static ushort Frequency { get; set; }

        public static byte ServerIdx { get; set; }
        public static bool IgnoreClientVersion { get; set; }
        public static int ClientVersion { get; set; }
        public static int ClientMagicKey { get; set; }
        public static int LoginTimer { get; set; }
        public static string CashShopURL { get; set; }
        public static string CashChargeURL { get; set; }
        public static string GuildURL { get; set; }

        public static IPAddress GMSIP { get; set; }
        public static ushort GMSPort { get; set; }
        public static int GMSConnectionTimeOut { get; set; }

        public static IPAddress GDAIP { get; set; }
        public static ushort GDAPort { get; set; }
        public static int GDAConnectionTimeOut { get; set; }

        public static void Load(string iniFileName)
        {
            IniReader parser = new IniReader(iniFileName);

            const string LISTEN_SECTION = "Listen";
            IP = IPAddress.Parse(parser.GetValue(LISTEN_SECTION, "IP"));
            Port = ushort.Parse(parser.GetValue(LISTEN_SECTION, "Port"));

            const string SERVER_SECTION = "Server";
            Enum.TryParse(parser.GetValue(SERVER_SECTION, "LogLevel"), out Logger.LOG_LEVEL _logLevel);

            LogLevel = _logLevel;
            LogPath = parser.GetValue(SERVER_SECTION, "LogPath");
            Threads = ushort.Parse(parser.GetValue(SERVER_SECTION, "Threads"));
            Frequency = ushort.Parse(parser.GetValue(SERVER_SECTION, "Frequency"));

            const string LOGINSERVER_SECTION = "LoginServer";
            ServerIdx = byte.Parse(parser.GetValue(LOGINSERVER_SECTION, "ServerIdx"));
            IgnoreClientVersion = bool.Parse(parser.GetValue(LOGINSERVER_SECTION, "IgnoreClientVersion"));
            ClientVersion = int.Parse(parser.GetValue(LOGINSERVER_SECTION, "ClientVersion"));
            ClientMagicKey = int.Parse(parser.GetValue(LOGINSERVER_SECTION, "ClientMagicKey"));
            LoginTimer = int.Parse(parser.GetValue(LOGINSERVER_SECTION, "LoginTimer"));
            CashShopURL = parser.GetValue(LOGINSERVER_SECTION, "CashShopURL");
            CashChargeURL = parser.GetValue(LOGINSERVER_SECTION, "CashChargeURL");
            GuildURL = parser.GetValue(LOGINSERVER_SECTION, "GuildURL");

            const string GLOBALMGRSERVER_SECTION = "GlobalMgrServer";
            GMSIP = IPAddress.Parse(parser.GetValue(GLOBALMGRSERVER_SECTION, "GMSIP"));
            GMSPort = ushort.Parse(parser.GetValue(GLOBALMGRSERVER_SECTION, "GMSPort"));
            GMSConnectionTimeOut = int.Parse(parser.GetValue(GLOBALMGRSERVER_SECTION, "GMSConnectionTimeOut"));

            const string GLOBALDBASERVER_SECTION = "GlobalDBAgent";
            GDAIP = IPAddress.Parse(parser.GetValue(GLOBALDBASERVER_SECTION, "GDAIP"));
            GDAPort= ushort.Parse(parser.GetValue(GLOBALDBASERVER_SECTION, "GDAPort"));
            GDAConnectionTimeOut = int.Parse(parser.GetValue(GLOBALDBASERVER_SECTION, "GDAConnectionTimeOut"));
        }
    }
}
