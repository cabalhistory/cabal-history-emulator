﻿namespace WorldServer
{
    public enum Opcode : ushort
    {
        // CONNECT
        CSC_CONNECT2SVR = 140, // C2S_CONNECT2SVR, S2C_CONNECT2SVR
        CSC_VERIFYLINKS = 141, // C2S_VERIFYLINKS, S2C_VERIFYLINKS

        // SERVER
        CSC_GETSVRTIME = 148, // C2S_GETSVRTIME, S2C_GETSVRTIME
        CSC_SERVERENV = 464, // C2S_SERVERENV, S2C_SERVERENV
        CSC_PING = 2558, // C2S_PING, S2C_PING


        // ------------------------------------------------------------------------- //
        // CHARACTER LOBBY
        // ------------------------------------------------------------------------- //
        // CAHRACTERS
        CSC_GETMYCHARTR = 133, // C2S_GETMYCHARTR, S2C_GETMYCHARTR
        CSC_NEWCHARSCRE = 2156,

        // SUBPASSWORD
        CSC_SUBPASSWORD_SET = 1030, // C2S_SUBPASSWORD_SET, S2C_SUBPASSWORD_SET
        CSC_SUBPASSWORD_ASK = 1032, // C2C_SUBPASSWORD_ASK, S2C_SUBPASSWORD_ASK
        CSC_SUBPASSWORD_AUTH = 1034, // C2S_SUBPASSWORD_AUTH, S2C_SUBPASSWORD_AUTH
        CSC_SUBPASSWORD_GET_QA = 1036, // C2S_SUBPASSWORD_GET_QA, S2C_SUBPASSWORD_GET_QA
        CSC_SUBPASSWORD_ANSWER_QA = 1038, // C2S_SUBPASSWORD_ANSWER_QA, S2C_SUBPASSWORD_ANSWER_QA
        CSC_SUBPASSWORD_DEL_AUTH = 1040, // C2S_SUBPASSWORD_DEL_AUTH, S2C_SUBPASSWORD_DEL_AUTH
        CSC_SUBPASSWORD_DEL = 1042, // C2S_SUBPASSWORD_DEL, S2C_SUBPASSWORD_DEL
        CSC_SUBPASSWORD_CHANGE_QA_AUTH = 1044, // C2S_SUBPASSWORD_CHANGE_QA_AUTH, S2C_SUBPASSWORD_CHANGE_QA_AUTH
        CSC_SUBPASSWORD_CHANGE_QA = 1046, // C2S_SUBPASSWORD_CHANGE_QA, S2C_SUBPASSWORD_CHANGE_QA

        // CHARACTER MANAGEMENT
        CSC_CHARACTER_SLOTORDER = 2001, // CSC_CHARACTER_SLOTORDER
        // ------------------------------------------------------------------------- //


        // ENTER/QUIT WORLD
        CSC_INITIALIZED = 142, // C2S_INITIALIZED, S2C_INITIALIZED
        CSC_UNINITIALZE = 143, // C2S_UNINITIALZE, S2C__UNINITIALZE
        CSC_ITEMLOOTING = 153,
        CSC_EQUIP_UNQUIP_ITEM = 2165,
        CSC_CHANGEITEM = 2166,
        CSC_DROPITEM = 2168,

        // ------------------------------------------------------------------------- //
        // COMMON CSC
        // ------------------------------------------------------------------------- //
        // COMBAT
        CSC_SKILLTOMOBS = 174, // CSC_SKILLTOMOBS
        CSC_SKILLTOUSER = 175, // CSC_SKILLTOUSER
        CSC_ATTCKTOMOBS = 176, // CSC_ATTCKTOMOBS
        CSC_ATTCKTOUSER = 177, // CSC_ATTCKTOUSER
        // ------------------------------------------------------------------------- //


        // ------------------------------------------------------------------------- //
        // COMMON MOVEMENTS
        // ------------------------------------------------------------------------- //
        REQ_MOVEBEGINED = 190,  // RES_MOVEBEGINED
        REQ_MOVEENDED00 = 191,  // RES_MOVEENDED00
        REQ_MOVECHAGNED = 192,  // RES_MOVECHANGED

        REQ_MOVEWAYPNTS = 193,  // RES_MOVEWAYPNTS
        REQ_MOVETILEPOS = 194,  // RES_MOVETILEPOS

        // CHAT?
        REQ_MESSAGEEVNT = 195,  // RES_MESSAGEEVNT

        // PARTY
        REQ_PARTYINVITE = 196,  // RES_PARTYINVITE
        REQ_PARTYMESSAG = 197,	// RES_PARTYMESSAG
        // ------------------------------------------------------------------------- //


        // ------------------------------------------------------------------------- //
        // COMMON NFY
        // ------------------------------------------------------------------------- //
        // LISTS
        NFY_NEWUOBJLIST = 198, // NFY_NEWUOBJLIST
        NFY_DELUOBJLIST = 199, // NFY_DELUOBJLIST
        NFY_NEWUSERLIST = 200, // NFY_NEWUSERLIST
        NFY_DELUSERLIST = 201, // NFY_DELUSERLIST
        NFY_NEWMOBSLIST = 202, // NFY_NEWMOBSLIST
        NFY_DELMOBSLIST = 203, // NFY_DELMOBSLIST
        NFY_NEWITEMLIST = 204, // NFY_NEWITEMLIST
        NFY_DELITEMLIST = 205, // NFY_DELITEMLIST

        // USER EQUIP
        NFY_ITEMEQUIPS0 = 206, // NFY_ITEMEQUIPS0
        NFY_ITEMUNEQUIP = 207, // NFY_ITEMUNEQUIP

        // USER MOVEMENT
        NFY_MOVEBEGINED = 210, // NFY_MOVEBEGINED
        NFY_MOVEENDED00 = 211, // NFY_MOVEENDED00
        NFY_MOVECHANGED = 212, // NFY_MOVECHANGED

        // MOBS MOVEMENT
        NFY_MOBSMOVEBGN = 213, // NFY_MOBSMOVEBGN
        NFY_MOBSMOVEEND = 214, // NFY_MOBSMOVEEND
        NFY_MOBSCHASBGN = 215, // NFY_MOBSCHASBGN
        NFY_MOBSCHASEND = 216, // NFY_MOBSCHASEND

        // CHAT
        NFY_MESSAGEEVNT = 217, // NFY_MESSAGEEVNT

        // COMBAT
        NFY_SKILLTOMOBS = 220, // NFY_SKILLTOMOBS
        NFY_SKILLTOUSER = 221, // NFY_SKILLTOUSER
        NFY_SKILLTOMTON = 222, // NFY_SKILLTOMTON
        NFY_SKILLBYMOBS = 223, // NFY_SKILLBYMOBS
        NFY_SKILLBYMTON = 224, // NFY_SKILLBYMTON
        NFY_ATTCKTOMOBS = 225, // NFY_ATTCKTOMOBS
        NFY_ATTCKTOUSER = 226, // NFY_ATTCKTOUSER
        NFY_ATTCKMOTION = 227, // NFY_ATTCKMOTION
        // ------------------------------------------------------------------------- //


        // WARP
        CSC_WARPCOMMAND = 244, // C2S_WARPCOMMAND, S2C_WARPCOMMAND

        // ITEM
        CSC_ITEMUSING00 = 285,  // C2S_ITEMUSING00, S2C_ITEMUSING00
        CSC_ITEMSPLITE = 286,  // C2S_ITEMSPLITE, S2C_ITEMSPLITE

        // ACTS
        REQ_SKILLTOACTS = 310, // REQ_SKILLTOACTS
        NFY_SKILLTOACTS = 311, // NFY_SKILLTOACTS
        

        // QUESTS
        CSC_QUESTOPNEVT = 282, // CSC_QUESTOPNEVT
        CSC_QUESTCLSEVT = 283, // CSC_QUESTCLSEVT
        CSC_QUESTCCLEVT = 284, // CSC_QUESTCCLEVT
        CSC_QSTNPCACTIN = 320, // CSC_QSTNPCACTIN

        // STYLE
        CSC_CHANGESTYLE = 322, // CSC_CHANGESTYLE
        NFY_CHANGESTYLE = 323, // NFY_CHANGESTYLE

        // CHARGE INFO
        CSC_CHARGEINFO = 324, // CSC_CHARGEINFO
        NFY_CHARGEINFO = 325, // NFY_CHARGEINFO

        // CASHITEM
        CSC_QUERYCASHITEM = 418, // CSC_QUERYCASHITEM
        CSC_USECASHITEM = 419, // CSC_USECASHITEM

        // PCBANG
        CSC_ESTPCBANGALERT = 480, // CSC_ESTPCBANGALERT
        CSC_ESTPCBANGEXPIRED = 481, // CSC_ESTPCBANGEXPIRED
        NFY_ESTPCBANGPOINT = 482, // NFY_ESTPCBANGPOINT

        // NPCSHOP
        BuySkillNpc = 2003, // BuySkillNpc
        LoadNpcShop = 2181, // LoadNpcShop
        SetLoadNpcShop = 2182, // SetLoadNpcShop
        CSC_NPCSHOP_ITEMS = 2180,
        CSC_NPCSHOP_BUY = 161,
        CSC_CLOSENPC = 2559,

        // FORCECALIBUR
        NFY_FORCE_CALIBUR_OWNER = 2195, // S2C_FORCE_CALIBUR_OWNER

        // UNK
        Unk2224 = 2224,
        Unk2518 = 2518,
        Unk2528 = 2528,
        Unk2756 = 2756,
        Unk2248 = 2248,
        Unk3305 = 3305,
        Unk2106 = 2106,
        Unk2766 = 2766,
        Unk2240 = 2240,
        Unk621 = 621,
        Unk994 = 994,
        Unk999 = 999,
        Unk1002 = 1002,
        Unk1008 = 1008,
        Unk2110 = 2110,
        Unk2046 = 2046,
        Unk2056 = 2056,
        Unk1054 = 1054,
        Unk2571 = 2571,
        Unk2568 = 2568,
        Unk2237 = 2237,
        Unk2566 = 2566,
        Unk2253 = 2253,
        Unk2179 = 2179,
        Unk2308 = 2308,
        Unk2502 = 2502,
        Unk322 = 322,
        Unk346 = 346,
    }
}
