﻿using Shared.Telepathy;
using System;
using System.Collections.Generic;
using System.Text;
using WorldServer.Context;

namespace WorldServer.Extensions
{
    static class ConnectionExtensions
    {
        const string CTX_PLAYER_KEY = "CTX_PLAYER";

        public static bool InitPlayerContext(this Connection connection, out PlayerContext playerContext)
        {
            playerContext = new PlayerContext(connection);
            return connection.Metadata.TryAdd(CTX_PLAYER_KEY, playerContext);
        }

        public static bool GetPlayerContext(this Connection connection, out PlayerContext playerContext)
        {
            playerContext = null;

            bool tryGet = connection.Metadata.TryGetValue(CTX_PLAYER_KEY, out object playerContextObj);

            if (tryGet)
                playerContext = (PlayerContext)playerContextObj;

            return tryGet;
        }
    }
}
