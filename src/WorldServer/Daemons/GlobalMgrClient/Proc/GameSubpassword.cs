﻿using Shared;
using Shared.Context;
using Shared.Extensions;
using Shared.Packet;
using Shared.Telepathy;
using System;
using static Shared.Protodef;
using static WorldServer.Protodef;

namespace WorldServer.Daemons.GlobalMgrClient.Proc
{
    static class GameSubpassword
    {
        public static void OnIPC_GAME_SUBPASSWORD_SET(PacketReader reader)
        {
            var ipsGameSubpasswordSetReply = reader.Packet<IPS_GAME_SUBPASSWORD_SET_REPLY>();

            if (!Program.WorldServerDaemon.GetConnection(ipsGameSubpasswordSetReply.DstCID, ipsGameSubpasswordSetReply.DstTick, out Connection connection))
                return;

            if (!connection.GetAccountContext(out AccountContext accountContext))
                return;

            // Status: 1 = Success
            if (ipsGameSubpasswordSetReply.Status == 1)
            {
                if(!Enum.TryParse(ipsGameSubpasswordSetReply.PwType.ToString(), out Typedef.SubpasswordType subpasswordType))
                {
                    connection.Disconnect($"OnIPC_GAME_SUBPASSWORD_SET(): Invalid PwType ({ipsGameSubpasswordSetReply.PwType})");
                    return;
                }

                switch (subpasswordType)
                {
                    case Typedef.SubpasswordType.Account:
                        if (ipsGameSubpasswordSetReply.Mode == (short)Typedef.SubpasswordSetMode.Create)
                            accountContext.ACSubPasswordExpiration = 0;

                        accountContext.UseACSUB = true;
                        break;

                    case Typedef.SubpasswordType.Wharehouse:

                        accountContext.UseWHSUB = true;
                        break;

                    case Typedef.SubpasswordType.Equipment:

                        accountContext.UseEQSUB = true;
                        break;
                }
            }

            var s2cPacket = new S2C_SUBPASSWORD_SET();
            s2cPacket.Status = ipsGameSubpasswordSetReply.Status;
            s2cPacket.PwType = ipsGameSubpasswordSetReply.PwType;
            s2cPacket.Mode = ipsGameSubpasswordSetReply.Mode;

            connection.Send(Opcode.CSC_SUBPASSWORD_SET, s2cPacket);
        }

        public static void OnIPC_GAME_SUBPASSWORD_GET_QA(PacketReader reader)
        {
            var ipsGameSubpasswordGetQaReply = reader.Packet<IPS_GAME_SUBPASSWORD_GET_QA_REPLY>();

            if (!Program.WorldServerDaemon.GetConnection(ipsGameSubpasswordGetQaReply.DstCID, ipsGameSubpasswordGetQaReply.DstTick, out Connection connection))
                return;

            var s2cPacket = new S2C_SUBPASSWORD_GET_QA();
            s2cPacket.QuestionId1 = ipsGameSubpasswordGetQaReply.QuestionId;
            s2cPacket.QuestionId2 = ipsGameSubpasswordGetQaReply.QuestionId;
            s2cPacket.PwType = ipsGameSubpasswordGetQaReply.PwType;

            connection.Send(Opcode.CSC_SUBPASSWORD_GET_QA, s2cPacket);
        }

        public static void OnIPC_GAME_SUBPASSWORD_ANSWER_QA(PacketReader reader)
        {
            var ipsGameSubpasswordAnswerQAReply = reader.Packet<IPS_GAME_SUBPASSWORD_ANSWER_QA_REPLY>();

            if (!Program.WorldServerDaemon.GetConnection(ipsGameSubpasswordAnswerQAReply.DstCID, ipsGameSubpasswordAnswerQAReply.DstTick, out Connection connection))
                return;

            if (!connection.GetAccountContext(out AccountContext accountContext))
                return;

            // Status: 1 = Success
            if (ipsGameSubpasswordAnswerQAReply.Status == 1)
            {
                if (!Enum.TryParse(ipsGameSubpasswordAnswerQAReply.PwType.ToString(), out Typedef.SubpasswordType subpasswordType))
                {
                    connection.Disconnect($"OnIPC_GAME_SUBPASSWORD_ANSWER_QA(): Invalid PwType ({ipsGameSubpasswordAnswerQAReply.PwType})");
                    return;
                }

                switch (subpasswordType)
                {
                    case Typedef.SubpasswordType.Account:

                        accountContext.IsACSecretAnswerAuthorized = true;
                        break;

                    case Typedef.SubpasswordType.Wharehouse:

                        accountContext.IsWHSecretAnswerAuthorized = true;
                        break;

                    case Typedef.SubpasswordType.Equipment:

                        accountContext.IsEQSecretAnswerAuthorized = true;
                        break;
                }
            }

            var s2cPacket = new S2C_SUBPASSWORD_ANSWER_QA();
            s2cPacket.Status = ipsGameSubpasswordAnswerQAReply.Status;
            s2cPacket.PwType = ipsGameSubpasswordAnswerQAReply.PwType;
            s2cPacket.TryCount = ipsGameSubpasswordAnswerQAReply.TryNum;
            
            connection.Send(Opcode.CSC_SUBPASSWORD_ANSWER_QA, s2cPacket);
        }

        public static void OnIPC_GAME_SUBPASSWORD_AUTH(PacketReader reader)
        {
            var ipsGameSubpasswordAuthReply = reader.Packet<IPS_GAME_SUBPASSWORD_AUTH_REPLY>();

            if (!Program.WorldServerDaemon.GetConnection(ipsGameSubpasswordAuthReply.DstCID, ipsGameSubpasswordAuthReply.DstTick, out Connection connection))
                return;

            if (!connection.GetAccountContext(out AccountContext accountContext))
                return;

            // Status: 1 = Success
            if (ipsGameSubpasswordAuthReply.Status == 1)
            {
                if (!Enum.TryParse(ipsGameSubpasswordAuthReply.PwType.ToString(), out Typedef.SubpasswordType subpasswordType))
                {
                    connection.Disconnect($"OnIPC_GAME_SUBPASSWORD_AUTH(): Invalid PwType ({ipsGameSubpasswordAuthReply.PwType})");
                    return;
                }

                switch (subpasswordType)
                {
                    case Typedef.SubpasswordType.Account:
                        
                        accountContext.IsACSubPasswordAuthorized = true;
                        break;

                    case Typedef.SubpasswordType.Wharehouse:
                        
                        accountContext.IsWHSubPasswordAuthorized = true;
                        break;

                    case Typedef.SubpasswordType.Equipment:
                        
                        accountContext.IsEQSubPasswordAuthorized = true;
                        break;
                }
            }

            if (ipsGameSubpasswordAuthReply.IsFastAuth == 1)
            {
                var s2cPacket = new S2C_SUBPASSWORD_AUTH_FAST();
                s2cPacket.Status = ipsGameSubpasswordAuthReply.Status;
                s2cPacket.PwType = ipsGameSubpasswordAuthReply.PwType;
                s2cPacket.TryNum = ipsGameSubpasswordAuthReply.TryNum;

                connection.Send(Opcode.CSC_SUBPASSWORD_DEL_AUTH, s2cPacket);
            }
            else
            {
                var s2cPacket = new S2C_SUBPASSWORD_AUTH();
                s2cPacket.Status = ipsGameSubpasswordAuthReply.Status;
                s2cPacket.PwType = ipsGameSubpasswordAuthReply.PwType;
                s2cPacket.TryNum = ipsGameSubpasswordAuthReply.TryNum;

                connection.Send(Opcode.CSC_SUBPASSWORD_AUTH, s2cPacket);
            }
        }

        public static void OnIPC_GAME_SUBPASSWORD_DEL(PacketReader reader)
        {
            var ipsGameSubpasswordDelReply = reader.Packet<IPS_GAME_SUBPASSWORD_DEL_REPLY>();

            if (!Program.WorldServerDaemon.GetConnection(ipsGameSubpasswordDelReply.DstCID, ipsGameSubpasswordDelReply.DstTick, out Connection connection))
                return;

            if (!connection.GetAccountContext(out AccountContext accountContext))
                return;

            // Status: 1 = Success
            if (ipsGameSubpasswordDelReply.Status == 1)
            {
                if (!Enum.TryParse(ipsGameSubpasswordDelReply.PwType.ToString(), out Typedef.SubpasswordType subpasswordType))
                {
                    connection.Disconnect($"OnIPC_GAME_SUBPASSWORD_DEL(): Invalid PwType ({ipsGameSubpasswordDelReply.PwType})");
                    return;
                }

                switch (subpasswordType)
                {
                    case Typedef.SubpasswordType.Account:

                        accountContext.UseACSUB = false;
                        break;

                    case Typedef.SubpasswordType.Wharehouse:

                        accountContext.UseWHSUB = false;
                        accountContext.IsWHLOCK = false;
                        break;

                    case Typedef.SubpasswordType.Equipment:

                        accountContext.UseEQSUB = false;
                        accountContext.IsEQLOCK = false;
                        break;
                }
            }

            accountContext.ResetSubpasswordAuthorizations();

            var s2cPacket = new S2C_SUBPASSWORD_DEL();
            s2cPacket.Status = ipsGameSubpasswordDelReply.Status;
            s2cPacket.PwType = ipsGameSubpasswordDelReply.PwType;

            connection.Send(Opcode.CSC_SUBPASSWORD_DEL, s2cPacket);
        }
    }
}
