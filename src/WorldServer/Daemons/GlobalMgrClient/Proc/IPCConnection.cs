﻿using Shared;
using Shared.Packet;
using static Shared.Protodef;

namespace WorldServer.Daemons.GlobalMgrClient.Proc
{
    static class IPCConnection
    {
        public static void OnIPC_CONNECT(PacketReader reader)
        {
            Logger.Log("GlobalMgrClient IAC authenticated, registering service...");

            var ipsRegisterChannel = new IPS_REGISTER_SERVICE();
            ipsRegisterChannel.GroupIdx = Config.GroupIdx;
            ipsRegisterChannel.ServerIdx = Config.ServerIdx;
            ipsRegisterChannel.IP = Config.IP.GetAddressBytes();
            ipsRegisterChannel.Port = Config.Port;
            ipsRegisterChannel.MaxConnections = Config.MaxPlayers;
            ipsRegisterChannel.Type = Config.ChannelType;

            Program.GlobalMgrClientDaemon.Send(Shared.Opcode.IPC_REGISTER_SERVICE, ipsRegisterChannel);
        }
    }
}
