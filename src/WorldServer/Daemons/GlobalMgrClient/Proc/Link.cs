﻿using System;
using Shared.Context;
using Shared.Extensions;
using Shared.Packet;
using Shared.Telepathy;
using static Shared.Protodef;

namespace WorldServer.Daemons.GlobalMgrClient.Proc
{
    static class Link
    {
        public static void OnIPC_REQ_LINK(PacketReader reader)
        {
            var ipsReqLinkReply = reader.Packet<IPS_REQ_LINK_REPLY>();

            if (!Program.WorldServerDaemon.GetConnection(ipsReqLinkReply.DstCID, ipsReqLinkReply.DstTick, out Connection connection))
                return;

            var s2cPacket = new S2C_VERIFYLINKS();
            s2cPacket.ServerIdx = ipsReqLinkReply.ServerIdx;
            s2cPacket.GroupIdx = ipsReqLinkReply.GroupIdx;
            s2cPacket.Status = ipsReqLinkReply.Status;

            connection.Send(Opcode.CSC_VERIFYLINKS, s2cPacket);
        }

        public static void OnIPC_LINK(PacketReader reader)
        {
            var ipsLink = reader.Packet<IPS_LINK>();

            if (!Program.WorldServerDaemon.GetConnection(ipsLink.DstCID, ipsLink.DstTick, out Connection connection))
                return;

            if (!connection.InitAccountContext(out AccountContext accountContext))
                return;

            accountContext.UserNum = ipsLink.LinkData.UserNum;
            accountContext.ID = ipsLink.LinkData.ID;

            accountContext.ACSubPasswordExpiration = ipsLink.LinkData.SubpasswordExpiration;

            accountContext.QtdChars = ipsLink.LinkData.QtdChars;
            accountContext.CharSlotOrder = ipsLink.LinkData.CharSlotOrder;

            accountContext.UseACSUB = Convert.ToBoolean(ipsLink.LinkData.UseACSUB);
            accountContext.UseWHSUB = Convert.ToBoolean(ipsLink.LinkData.UseWHSUB);
            accountContext.UseEQSUB = Convert.ToBoolean(ipsLink.LinkData.UseEQSUB);
            accountContext.IsWHLOCK = Convert.ToBoolean(ipsLink.LinkData.IsWHLOCK);
            accountContext.IsEQLOCK = Convert.ToBoolean(ipsLink.LinkData.IsEQLOCK);

            accountContext.ServiceKind = ipsLink.LinkData.ServiceKind;
            accountContext.ExpireDate = ipsLink.LinkData.ExpireDate;
            accountContext.ExtendedCharCreation = ipsLink.LinkData.ExtendedCharCreation;

            var ipsLinkReply = new IPS_LINK_REPLY();
            ipsLinkReply.TargetGroupIdx = ipsLink.CallerGroupIdx;
            ipsLinkReply.TargetServerIdx = ipsLink.CallerServerIdx;

            ipsLinkReply.DstTick = ipsLink.SrcTick;
            ipsLinkReply.DstCID = ipsLink.SrcCID;

            ipsLinkReply.Status = 1;

            Program.GlobalMgrClientDaemon.Send(Shared.Opcode.IPC_LINK, ipsLinkReply);
        }
    }
}
