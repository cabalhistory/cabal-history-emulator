﻿using Shared.Packet;
using WorldServer.Daemons.GlobalMgrClient.Proc;

namespace WorldServer.Daemons.GlobalMgrClient
{
    static class Procs
    {
        public static void Register(PacketHandler packetsHandler)
        {
            packetsHandler.Register(Shared.Opcode.IPC_CONNECT, IPCConnection.OnIPC_CONNECT);

            packetsHandler.Register(Shared.Opcode.IPC_REQ_LINK, Link.OnIPC_REQ_LINK);
            packetsHandler.Register(Shared.Opcode.IPC_LINK, Link.OnIPC_LINK);

            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_SET, GameSubpassword.OnIPC_GAME_SUBPASSWORD_SET);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_GET_QA, GameSubpassword.OnIPC_GAME_SUBPASSWORD_GET_QA);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_ANSWER_QA, GameSubpassword.OnIPC_GAME_SUBPASSWORD_ANSWER_QA);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_AUTH, GameSubpassword.OnIPC_GAME_SUBPASSWORD_AUTH);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_DEL, GameSubpassword.OnIPC_GAME_SUBPASSWORD_DEL);
        }
    }
}
