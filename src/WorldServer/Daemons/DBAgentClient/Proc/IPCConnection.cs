﻿using Shared;
using Shared.Packet;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorldServer.Daemons.DBAgentClient.Proc
{
    static class IPCConnection
    {
        public static void OnIPC_CONNECT(PacketReader reader)
        {
            Logger.Log("DBAgentClient IAC authenticated");
        }
    }
}
