﻿using System;
using Shared.Packet;
using Shared.Telepathy;
using WorldServer.Context;
using WorldServer.Extensions;
using Shared.Extensions;
using Shared.Context;
using static Shared.Protodef;
using static WorldServer.Protodef;

namespace WorldServer.Daemons.DBAgentClient.Proc
{
    static class LobbyCharacters
    {
        public static void OnIPC_REQ_LIST_CHARACTERS(PacketReader reader)
        {
            var ipsReqListCharactersReply = reader.Packet<IPS_REQ_LIST_CHARACTERS_REPLY>();

            if (!Program.WorldServerDaemon.GetConnection(ipsReqListCharactersReply.ToWorldCID, ipsReqListCharactersReply.ToWorldTick, out Connection connection))
                return;

            if (!connection.GetAccountContext(out AccountContext accountContext))
                return;

            var s2cPacket = new S2C_GETMYCHARTR();

            accountContext.QtdChars = ipsReqListCharactersReply.QtdChars;
            accountContext.CharSlotOrder = ipsReqListCharactersReply.SlotOrder;

            s2cPacket.UseACSUB = Convert.ToByte(accountContext.UseACSUB);

            s2cPacket.Unk1 = 0;

            s2cPacket.LastCharacter = ipsReqListCharactersReply.LastCharacter;
            s2cPacket.SlotOrder = ipsReqListCharactersReply.SlotOrder;
            s2cPacket.ExtendedCharCreation = accountContext.ExtendedCharCreation;

            var s2cCharacters = new S2C_GETMYCHARTR_CHARACTER[ipsReqListCharactersReply.QtdChars];
            for (int i = 0; i < ipsReqListCharactersReply.QtdChars; i++)
            {
                s2cCharacters[i].CharacterIdx = ipsReqListCharactersReply.Characters[i].CharacterIdx;
                s2cCharacters[i].CreateDate = ipsReqListCharactersReply.Characters[i].CreateDate;
                s2cCharacters[i].Style = ipsReqListCharactersReply.Characters[i].Style;
                s2cCharacters[i].Level = ipsReqListCharactersReply.Characters[i].LEV;
                s2cCharacters[i].Unk0 = 1; // ????
                s2cCharacters[i].WorldIdx = (byte)ipsReqListCharactersReply.Characters[i].WorldIdx;
                s2cCharacters[i].PosX = (short)ipsReqListCharactersReply.Characters[i].PosX;
                s2cCharacters[i].PosY = (short)ipsReqListCharactersReply.Characters[i].PosY;

                // TODO: EQUIP SECTION
                //s2cCharacters[i].HeadItemIdx = 0;
                //s2cCharacters[i].EquipUnk0 = 0;

                s2cCharacters[i].NameLen = (byte)(ipsReqListCharactersReply.Characters[i].Name.Length + 1);
                s2cCharacters[i].Name = ipsReqListCharactersReply.Characters[i].Name + Char.MinValue; // String with null byte termination
            }

            s2cPacket.Characters = s2cCharacters;

            connection.Send(Opcode.CSC_GETMYCHARTR, s2cPacket);
        }

        public static void OnIPC_SET_CHAR_SLOTORDER(PacketReader reader)
        {
            var ipsSetCharSlotorderReply = reader.Packet<IPS_SET_CHAR_SLOTORDER_REPLY>();

            if (!Program.WorldServerDaemon.GetConnection(ipsSetCharSlotorderReply.ToWorldCID, ipsSetCharSlotorderReply.ToWorldTick, out Connection connection))
                return;

            if (!connection.GetPlayerContext(out PlayerContext playerContext))
                return;

            var s2cPacket = new S2C_CHARACTER_SLOTORDER();
            s2cPacket.Status = ipsSetCharSlotorderReply.Status;

            connection.Send(Opcode.CSC_CHARACTER_SLOTORDER, s2cPacket);
        }
    }
}
