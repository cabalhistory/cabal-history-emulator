﻿using Shared.Packet;
using WorldServer.Daemons.DBAgentClient.Proc;

namespace WorldServer.Daemons.DBAgentClient
{
    static class Procs
    {
        public static void Register(PacketHandler packetHandler)
        {
            packetHandler.Register(Shared.Opcode.IPC_CONNECT, IPCConnection.OnIPC_CONNECT);

            packetHandler.Register(Shared.Opcode.IPC_REQ_LIST_CHARACTERS, LobbyCharacters.OnIPC_REQ_LIST_CHARACTERS);
        }
    }
}
