﻿using Shared.Packet;
using WorldServer.Daemons.WorldServer.Proc;

namespace WorldServer.Daemons.WorldServer
{
    static class Procs
    {
        public static void Register(PacketHandler packetsHandler)
        {
            // CONNECT
            packetsHandler.Register(Opcode.CSC_CONNECT2SVR, Connect.OnCSC_CONNECT2SVR, false);
            packetsHandler.Register(Opcode.CSC_VERIFYLINKS, Verify.OnCSC_VERIFYLINKS, false);

            // SERVER
            packetsHandler.Register(Opcode.CSC_GETSVRTIME, ServerSystem.OnCSC_GETSVRTIME);
            packetsHandler.Register(Opcode.CSC_SERVERENV, ServerSystem.OnCSC_SERVERENV);
            packetsHandler.Register(Opcode.CSC_PING, ServerSystem.OnCSC_PING);


            // ------------------------------------------------------------------------- //
            // CHARACTER LOBBY
            // ------------------------------------------------------------------------- //
            // CAHRACTERS
            packetsHandler.Register(Opcode.CSC_GETMYCHARTR, Lobby.OnCSC_GETMYCHARTR);
            packetsHandler.Register(Opcode.CSC_NEWCHARSCRE, Lobby.OnCSC_NEWCHARSCRE);

            // SUBPASSWORD
            packetsHandler.Register(Opcode.CSC_SUBPASSWORD_ASK, Subpassword.OnCSC_SUBPASSWORD_ASK);
            packetsHandler.Register(Opcode.CSC_SUBPASSWORD_SET, Subpassword.OnCSC_SUBPASSWORD_SET);
            packetsHandler.Register(Opcode.CSC_SUBPASSWORD_GET_QA, Subpassword.OnCSC_SUBPASSWORD_GET_QA);
            packetsHandler.Register(Opcode.CSC_SUBPASSWORD_ANSWER_QA, Subpassword.OnCSC_SUBPASSWORD_ANSWER_QA);
            packetsHandler.Register(Opcode.CSC_SUBPASSWORD_AUTH, Subpassword.OnCSC_SUBPASSWORD_AUTH);
            packetsHandler.Register(Opcode.CSC_SUBPASSWORD_DEL_AUTH, Subpassword.OnCSC_SUBPASSWORD_AUTH);
            packetsHandler.Register(Opcode.CSC_SUBPASSWORD_DEL, Subpassword.OnCSC_SUBPASSWORD_DEL);
            packetsHandler.Register(Opcode.CSC_SUBPASSWORD_CHANGE_QA_AUTH, Subpassword.OnCSC_SUBPASSWORD_AUTH);
            packetsHandler.Register(Opcode.CSC_SUBPASSWORD_CHANGE_QA, Subpassword.OnCSC_SUBPASSWORD_CHANGE_QA);


            // CHARACTER MANAGEMENT
            packetsHandler.Register(Opcode.CSC_CHARACTER_SLOTORDER, Lobby.OnCSC_CHARACTER_SLOTORDER);
            // ------------------------------------------------------------------------- //


            // ENTER/QUIT WORLD
            packetsHandler.Register(Opcode.CSC_INITIALIZED, Character.EnterWorld);
            packetsHandler.Register(Opcode.CSC_UNINITIALZE, Character.ExitWorld);

            // ------------------------------------------------------------------------- //
            // COMMON CSC
            // ------------------------------------------------------------------------- //
            // COMBAT

            // ------------------------------------------------------------------------- //


            // ------------------------------------------------------------------------- //
            // COMMON MOVEMENTS
            // ------------------------------------------------------------------------- //
            packetsHandler.Register(Opcode.REQ_MOVEBEGINED, Movement.OnREQ_MOVEBEGINED);
            packetsHandler.Register(Opcode.REQ_MOVETILEPOS, Movement.OnREQ_MOVETILEPOS);
            packetsHandler.Register(Opcode.REQ_MOVECHAGNED, Movement.OnREQ_MOVECHANGED);
            packetsHandler.Register(Opcode.REQ_MOVEENDED00, Movement.OnREQ_MOVEENDED00);

            // ------------------------------------------------------------------------- //


            // WARP
            packetsHandler.Register(Opcode.CSC_WARPCOMMAND, World.WarpCommand);


            // ITEM
            packetsHandler.Register(Opcode.CSC_ITEMUSING00, Inventory.UseItem);
            packetsHandler.Register(Opcode.CSC_EQUIP_UNQUIP_ITEM, Inventory.OnCSC_EQUIP_UNQUIP_ITEM);
            packetsHandler.Register(Opcode.CSC_DROPITEM, Inventory.OnCSC_DROPITEM);
            packetsHandler.Register(Opcode.CSC_ITEMLOOTING, Inventory.OnCSC_ITEMLOOTING);
            packetsHandler.Register(Opcode.CSC_CHANGEITEM, Inventory.OnCSC_CHANGEITEM);

            // ACTS
            packetsHandler.Register(Opcode.REQ_SKILLTOACTS, Acts.OnREQ_SKILLTOACTS);

            // QUESTS
            packetsHandler.Register(Opcode.CSC_QUESTOPNEVT, Quest.QuestStart);
            packetsHandler.Register(Opcode.CSC_QUESTCLSEVT, Quest.QuestEnd);
            packetsHandler.Register(Opcode.CSC_QSTNPCACTIN, Quest.QuestNextStep);
            // MISSING CSC_QUESTCCLEVT = 284


            // STYLE


            // CHARGE INFO
            packetsHandler.Register(Opcode.CSC_CHARGEINFO, Charge.OnCSC_CHARGEINFO);


            // CASHITEM
            packetsHandler.Register(Opcode.CSC_QUERYCASHITEM, Inventory.QueryCashItem);
            packetsHandler.Register(Opcode.CSC_USECASHITEM, Inventory.ReceiveQueryCashItem);

            // PCBANG


            // NPCSHOP
            packetsHandler.Register(Opcode.BuySkillNpc, NpcShop.BuySkillNpc);
            packetsHandler.Register(Opcode.LoadNpcShop, NpcShop.LoadNpcShop);
            packetsHandler.Register(Opcode.CSC_NPCSHOP_ITEMS, NpcShop.CSC_NPCSHOP_ITEMS);
            packetsHandler.Register(Opcode.CSC_NPCSHOP_BUY, NpcShop.CSC_NPCSHOP_BUY);
            packetsHandler.Register(Opcode.CSC_CLOSENPC, NpcShop.CSC_CLOSENPC);






            // Unknown
            packetsHandler.Register(Opcode.Unk2571, Unknown.Unk2571);
            packetsHandler.Register(Opcode.Unk2568, Unknown.Unk2568);
            packetsHandler.Register(Opcode.Unk2237, Unknown.Unk2237);
            packetsHandler.Register(Opcode.Unk2566, Unknown.Unk2566);
            packetsHandler.Register(Opcode.Unk2253, Unknown.Unk2253);
            packetsHandler.Register(Opcode.Unk2179, Unknown.Unk2179);
            packetsHandler.Register(Opcode.Unk2308, Unknown.Unk2308);
            packetsHandler.Register(Opcode.Unk2502, Unknown.Unk2502);
            packetsHandler.Register(Opcode.Unk322, Unknown.Unk322);
            packetsHandler.Register(Opcode.Unk346, Unknown.Unk346);
        }
    }
}
