﻿using System;
using Shared.Packet;
using Shared.Telepathy;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class Character
    {
        public static void EnterWorld(Connection connection, PacketReader packet)
        {
            // TEST!
            connection.Metadata["WorldIdx"] = (short)1;

            Unknown.Unk2224(connection.CID);
            Unknown.Unk2518(connection.CID);
            Unknown.Unk2528(connection.CID);
            Unknown.Unk2756(connection.CID);
            Unknown.Unk2248(connection.CID);

            // Response
            using (PacketBuilder builder = new PacketBuilder(true))
            {
                builder.WriteOpcode(Opcode.CSC_INITIALIZED);

                builder.Write(new byte[58]); // Unk1
                builder.Write((byte)0x14); // Unk2

                builder.Write((byte)1); // Server id
                builder.Write((byte)1); // Channel id

                builder.Write(new byte[23]); // Unk3
                builder.Write((byte)0xFF); // Unk4

                builder.Write((short)100); // Channel max players
                builder.Write(BitConverter.ToUInt32(Config.IP.GetAddressBytes(), 0)); // channel ip
                builder.Write(Config.Port); // channel port

                builder.Write(0); // Unk5
                builder.Write(0); // Unk6
                builder.Write(16827875); // Unk7

                builder.Write(1); // map id
                builder.Write(0); // unk8
                builder.Write((short)36); // x
                builder.Write((short)22); // y

                builder.Write((ulong)666); // exp
                builder.Write((ulong)500000000); // alz
                builder.Write((ulong)0); // wexp
                builder.Write(170); // level
                builder.Write(0); // Unk9
                builder.Write(500); // str
                builder.Write(500); // dex
                builder.Write(500); // int
                builder.Write(5); // points

                builder.Write((byte)0x01); // sword rank
                builder.Write((byte)0x00); // magic rank **UNUSED**

                builder.Write(new byte[6]); // Unk10

                builder.Write((uint)5000); // max hp
                builder.Write((uint)5000); // curr hp
                builder.Write((uint)1000); // max mp
                builder.Write((uint)1000); // curr mp
                builder.Write((ushort)1000); // max sp
                builder.Write((ushort)1000); // curr sp
                builder.Write(0); // max rage

                builder.Write((uint)0);  // DP

                builder.Write((ushort)10800); // Unk11
                builder.Write((ushort)0); // Unk12
                builder.Write((uint)0); // Unk13

                builder.Write((uint)0); //SkillExpBars Count
                builder.Write((uint)1152); //SkillExp
                builder.Write((uint)0); //SkillPoints

                builder.Write((ushort)2135); // Unk14
                builder.Write((uint)200000); //Honor Points

                builder.Write(0); // Unk15
                builder.Write(0); // Unk16
                builder.Write(0); // Unk17
                builder.Write(0); // Unk18
                builder.Write(0); // Unk19
                builder.Write((ushort)0); // Unk20
                builder.Write((ushort)0); // Unk21
                builder.Write(0); // Unk22

                // Maybe party server
                builder.Write(Config.IP.GetAddressBytes()); // unk server ip
                builder.Write((ushort)777); // unk server port

                // Port 38141 from official server
                builder.Write(Config.IP.GetAddressBytes()); // unk server ip Possible AuctionServer
                builder.Write((ushort)888); // unk server port Possible AuctionServer

                // Port 38110 from official server
                builder.Write(Config.IP.GetAddressBytes()); // unk server ip
                builder.Write((ushort)999); // unk server port

                builder.Write((ushort)48110); // Unk23 ( maybe another PORT )

                builder.Write(new byte[5]); // Unk24

                builder.Write(7); // Unk25
                builder.Write(7); // Unk26

                builder.Write((uint)7 + 160); // Character Style
                builder.Write((uint)0); // Unk27

                builder.Write(new byte[240]); // Unk28

                builder.Write((ushort)6); // equip count    
                builder.Write((ushort)8); // items count
                builder.Write((ushort)0); // Unk29
                builder.Write((ushort)18); // skills count
                builder.Write((ushort)5); // quickslots count

                builder.Write(new byte[1304]); // empty1

                builder.Write(1); // Unk30
                builder.Write(0); // Unk31
                builder.Write(0); // Unk32
                builder.Write(0); // Unk33
                builder.Write(0); // Unk34
                builder.Write(0); // Unk35
                builder.Write(1); // Unk36

                builder.Write(new byte[79]); // Unk37

                builder.Write(1); // Unk38

                builder.Write(new byte[62]); // Unk39

                builder.Write(8); // Unk40
                builder.Write(8); // Unk41

                builder.Write(new byte[48]); // Unk42

                builder.Write(10); // Unk43
                builder.Write(1); // Unk44

                builder.Write(new byte[9]); // Unk45

                builder.Write(1000); // Unk46
                builder.Write(59); // Unk47

                builder.Write(new byte[2091]); // Unk48

                builder.Write(1); // Unk49
                builder.Write(125); // G. Arcanas

                builder.Write(0); // OverlordLevel
                builder.Write((ulong)0); // OverlordEXP

                builder.Write(new byte[17]); // Unk50
                builder.Write((byte)192); // Unk51
                builder.Write((byte)168); // Unk52

                builder.Write(new byte[19]); // Unk53

                string name = "History";
                builder.Write((byte)(name.Length + 1)); // name length + 1 
                builder.Write(name); // name

                // foreach equips
                /*builder.Write(new byte[] {
                    0x69, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x96, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x56, 0x01, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x56, 0x01, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC3, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00,
                    0x00
                });

                // foreach items
                builder.Write(new byte[] {
                    0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x7D, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7C, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x43, 0x05, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB4, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7D, 0x29, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2C, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xD0, 0x3E, 0x00, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x4C, 0x19, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                });

                // foreach skills
                builder.Write(new byte[] {
                    0x60, 0x01, 0x01, 0x48, 0x00, 0x61, 0x01, 0x01, 0x49, 0x00, 0x8F, 0x01, 0x01, 0x4B, 0x00, 0x8B, 0x01, 0x01, 0x4E, 0x00, 0x8C, 0x01, 0x01, 0x4F, 0x00, 0x8D, 0x01, 0x01, 0x50, 0x00, 0x8E,
                    0x01, 0x01, 0x51, 0x00, 0xA5, 0x01, 0x01, 0x66, 0x00, 0xA6, 0x01, 0x01, 0x67, 0x00, 0xDA, 0x01, 0x01, 0x68, 0x00, 0xAC, 0x00, 0x01, 0x69, 0x00, 0xAD, 0x00, 0x01, 0x6A, 0x00, 0xAE, 0x00,
                    0x01, 0x6B, 0x00, 0x4C, 0x00, 0x01, 0x00, 0x00, 0x4D, 0x00, 0x01, 0x01, 0x00, 0xB1, 0x00, 0x01, 0x43, 0x00, 0xAB, 0x00, 0x01, 0x4A, 0x00, 0x4D, 0x01, 0x01, 0x46, 0x00
                });

                // foreach quickslots
                builder.Write(new byte[] {
                    0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0xED, 0x00, 0x09, 0x00, 0xEF, 0x00, 0x0A, 0x00, 0x82, 0x00, 0x0B, 0x00
                });

                // UNKNOWN
                builder.Write(new byte[]
                {
                    0x35, 0x04, 0x1A, 0x00, 0x00, 0x00, 0x13, 0x71, 0x36, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x8A, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11,
                    0x27, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x13, 0x27, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x15, 0x27, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1C, 0x27,
                    0x23, 0x03, 0x00, 0x00, 0x90, 0x7A, 0x22, 0x27, 0x02, 0x00, 0x00, 0x00, 0x90, 0xEA, 0x23, 0x27, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x24, 0x27, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x1C, 0x27, 0x00, 0x00, 0x00, 0x00, 0x22, 0x27, 0x00, 0x00, 0x00, 0x00, 0x01
                });*/

                connection.Send(builder);
            }

            Unknown.Unk3305(connection.CID);
            Unknown.Unk2106(connection.CID);
            Unknown.Unk2766(connection.CID);
            Unknown.Unk2240(connection.CID);

            ServerSystem.SendForcecaliburOwner(connection.CID);

            Unknown.Unk621(connection.CID);
            Unknown.Unk994(connection.CID);
            Unknown.Unk999(connection.CID);
            Unknown.Unk1002(connection.CID);
            Unknown.Unk1008(connection.CID);
            Unknown.Unk2110(connection.CID);
            Unknown.Unk2046(connection.CID);
            Unknown.Unk2056(connection.CID);
            Unknown.Unk1054(connection.CID);
        }

        public static void ExitWorld(Connection connection, PacketReader packet)
        {

            int Unk1 = packet.ReadInt();

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_UNINITIALZE);
                builder.Write((byte)0x00);
                connection.Send(builder);
            }

        }
    }
}
