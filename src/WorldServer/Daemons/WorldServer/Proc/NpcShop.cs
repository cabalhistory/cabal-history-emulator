﻿using Shared.Packet;
using Shared.Telepathy;
using System.Linq;
using System.Timers;
using WorldServer.Data;
using static WorldServer.Data.WorldData;
using static WorldServer.Protodef;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class NpcShop
    {
        public static void BuySkillNpc(Connection connection, PacketReader reader)
        {
            //reader.Skip(1);

            reader.ReadByte();

            short pNpcSlotId = reader.ReadShort();
            short pSkillID = reader.ReadShort();
            short pInventorySlotID = reader.ReadShort(); // TODO: Check if space is empty

            short currentWorldIdx = (short)connection.Metadata["WorldIdx"];

            if (!Worlds[currentWorldIdx].NPC.SkillLst.ContainsKey(pNpcSlotId))
            {
                connection.Disconnect($"Trying to buy invalid skill NpcSlotId (WorldIdx: {currentWorldIdx}, NpcSlotId: {pNpcSlotId})");
                return;
            }

            SkillLst skillLst = Worlds[currentWorldIdx].NPC.SkillLst[pNpcSlotId];
            if (skillLst.SkillIdx != pSkillID)
            {
                connection.Disconnect($"Buy skill mismatch (c: {pSkillID}, s:{skillLst.SkillIdx})");
                return;
            }

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.BuySkillNpc);
                builder.Write(0); //Status 0 = Sucesso , 1 = Erro
                builder.Write(skillLst.SkillBook); // Retorna o Id do item relacionado ao livro da skill
                builder.Write(new byte[20]); // Não sei o que é, mas e padrão em todo retorno do servidor

                connection.Send(builder);
            }
        }

        public static void LoadNpcShop(Connection connection, PacketReader reader)
        {
            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.SetLoadNpcShop);
                builder.Write(0);
                connection.Send(builder);
            }
        }

        public static void CSC_NPCSHOP_ITEMS(Connection connection, PacketReader reader)
        {
            C2S_LISTNPCITEMS c2sPacket = reader.Packet<C2S_LISTNPCITEMS>();

            var npcShops = NpcShopData.ShopSection.Values.Where(t => t.Pool_ID == c2sPacket.PoolID);
            int shopCount = npcShops.Count();

            S2C_LISTNPCITEMS s2cNpcShop = new S2C_LISTNPCITEMS();
            s2cNpcShop.PoolID = c2sPacket.PoolID;
            s2cNpcShop.QtdItem = (short)shopCount;

            S2C_LISTNPCITEMS_ITEM[] s2cListItemList = new S2C_LISTNPCITEMS_ITEM[shopCount];
            int i = 0;
            foreach (var npcShop in npcShops)
            {
                s2cListItemList[i].Unk1 = 0;
                s2cListItemList[i].SlotID = npcShop.SlotID;
                s2cListItemList[i].ItemKind = npcShop.ItemKind;
                s2cListItemList[i].ItemOpt = npcShop.ItemOpt;
                s2cListItemList[i].Duration = npcShop.DurationIdx;
                s2cListItemList[i].Unk2 = 0;
                s2cListItemList[i].MinLevel = npcShop.MinLevel;
                s2cListItemList[i].MinLevel = npcShop.MaxLevel;
                s2cListItemList[i].Unk3 = 0;
                s2cListItemList[i].Reputation = npcShop.Reputation;
                s2cListItemList[i].MaxReputation = npcShop.MaxReputation;
                s2cListItemList[i].Unk4 = 0;
                s2cListItemList[i].AlzPrice = npcShop.AlzPrice;
                s2cListItemList[i].WexpPrice = npcShop.WExpPrice;
                s2cListItemList[i].DPPrice = npcShop.DPPrice;
                s2cListItemList[i].CashPrice = npcShop.CashPrice;
               
                i++;
            }

            s2cNpcShop.Items = s2cListItemList;

            connection.Send(Opcode.CSC_NPCSHOP_ITEMS, s2cNpcShop);

            //using (PacketBuilder builder = new PacketBuilder())
            //{
            //    builder.WriteOpcode(Opcode.CSC_NPCSHOP_ITEMS);

            //    builder.Write(s2cNpcShop);
            //    builder.Write(s2cListItemList);

            //    connection.Send(builder);
            //}
        }

        public static void CSC_NPCSHOP_BUY(Connection connection, PacketReader reader)
        {
            C2S_NPCSHOP_BUY c2sPacket = reader.Packet<C2S_NPCSHOP_BUY>();

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_NPCSHOP_BUY);
                builder.Write(0);
                builder.Write(c2sPacket.ItemID);
                builder.Write(c2sPacket.ItemOption);
                builder.Write((short)0);
                builder.Write((short)0);
                builder.Write((short)0);
                connection.Send(builder);
            }
        }

        public static void CSC_CLOSENPC(Connection connection, PacketReader reader)
        {
            C2S_CLOSENPC c2sPacket = reader.Packet<C2S_CLOSENPC>();

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_CLOSENPC);
                builder.Write(c2sPacket.Check);
                connection.Send(builder);
            }
        }

    }
}
