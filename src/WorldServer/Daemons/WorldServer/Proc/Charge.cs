﻿using Shared.Packet;
using Shared.Telepathy;
using Shared.Extensions;
using Shared.Context;
using static WorldServer.Protodef;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class Charge
    {
        public static void OnCSC_CHARGEINFO(Connection connection, PacketReader packet)
        {
            if(connection.GetAccountContext(out AccountContext accountContext))
            {
                var s2cPacket = new S2C_CHARGEINFO();
                s2cPacket.ServiceType = 1;
                s2cPacket.ServiceKind = accountContext.ServiceKind;
                s2cPacket.ExpireDate = accountContext.ExpireDate;

                connection.Send(Opcode.CSC_CHARGEINFO, s2cPacket);
            }
        }
    }
}
