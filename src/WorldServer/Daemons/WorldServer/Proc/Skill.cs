﻿using Shared.Packet;
using Shared.Telepathy;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class Skill
    {
        public static void SkillToMobs(Connection connection, PacketReader reader)
        {
            // Response
            short pSkillID = reader.ReadShort();
            byte unk1 = reader.ReadByte();
            int pDmg = reader.ReadInt();
            short pMonsterPosX = reader.ReadShort();
            short pMonsterPosY = reader.ReadShort();
            byte unk2 = reader.ReadByte();
            int unk3 = reader.ReadInt();
            byte unk4 = reader.ReadByte();
            short pCharPosX = reader.ReadShort();
            short pCharPosY = reader.ReadShort();
            short pPosX = reader.ReadShort();
            short pPosY = reader.ReadShort();
            short pMobID = reader.ReadShort();
            byte unk5 = reader.ReadByte();
            byte[] unk6 = reader.ReadBytes(2);
            short unk7 = reader.ReadShort();

            using (PacketBuilder builder = new PacketBuilder())
            {

                builder.WriteOpcode(Opcode.CSC_SKILLTOMOBS);
                builder.Write((short)1); // Skill ID
                builder.Write(100); // HP
                builder.Write(16); // MP
                builder.Write((short)0); 
                builder.Write((short)344); 
                builder.Write((long)0); 
                builder.Write(0);
                builder.Write((short)0);
                builder.Write((long)28); // Skill Exp
                builder.Write(new byte[26]);
                builder.Write(0xFFFFFFFF);
                builder.Write((byte)0);
                builder.Write(100); // HP
                builder.Write(0);
                builder.Write((byte)0);
                builder.Write(pMobID); // MMAP mobid
                builder.Write((byte)0x01); // Map ID
                builder.Write(new byte[2]);
                builder.Write(new byte[2]);
                builder.Write(new byte[2]);
                builder.Write(14); // Atack Result
                builder.Write(111); // DMG
                builder.Write(0); // Current Mob HP
                builder.Write((long)0);
                builder.Write((byte)0x01); 

                connection.Send(builder);
            }
        }


        public static void NormalAtack(Connection connection, PacketReader reader)
        {
            // Response
            short pMonterID = reader.ReadShort();
            byte pMapID = reader.ReadByte();
            byte pUnk0 = reader.ReadByte();
            short pUnk1 = reader.ReadShort();


            using (PacketBuilder builder = new PacketBuilder())
            {

                builder.WriteOpcode(Opcode.CSC_ATTCKTOMOBS);
                builder.Write(pMonterID);
                builder.Write(pMapID);
                builder.Write(new byte [2]);
                builder.Write(53);
                builder.Write(21);
                builder.Write(new byte[2]);
                builder.Write((byte)0x02);
                builder.Write((short)351);
                builder.Write((long)0);
                builder.Write(new byte[6]);
                builder.Write((short)0);
                builder.Write((short)0);
                builder.Write(124); // DMG
                builder.Write((short)100);
                builder.Write(new byte[4]);
                builder.Write((byte)0x00);
                builder.Write(53);
                builder.Write(0);

                connection.Send(builder);
            }
        }


    }
}
