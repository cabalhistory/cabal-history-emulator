﻿using System.Linq;
using Shared.Packet;
using Shared.Telepathy;
using static WorldServer.Data.WarpData;
using static WorldServer.Data.WorldData;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class World
    {

        const byte NPC_GM_WARP = 0xC9;
        const byte NPC_GUIDE_STONE = 0x3E;

        public static void WarpCommand(Connection connection, PacketReader reader)
        {
            byte pNpcIdx = reader.ReadByte();
            byte pNsetIdx = reader.ReadByte();

            short currentWorldIdx = (short)connection.Metadata["WorldIdx"]; // TODO: Get player current world

            // GM Warp
            if (pNpcIdx == NPC_GM_WARP)
            {
                // reader.Skip(5);
                reader.ReadByte();
                reader.ReadByte();
                reader.ReadByte();
                reader.ReadByte();
                reader.ReadByte();

                byte pGmPosY = reader.ReadByte();
                byte pGmPosX = reader.ReadByte();


                reader.ReadByte();
                reader.ReadByte();


                byte pGmWorldIdx = reader.ReadByte();

                WarpPlayer(connection, pGmWorldIdx, pGmPosX, pGmPosY, 0);
                return;
            }

            // Guide Stone
            if (pNpcIdx == NPC_GUIDE_STONE)
            {
                // TODO: Use stone guide from inventory
                Terrain terrainData = Worlds[currentWorldIdx].Terrain.Terrain[0];
                WarpRow warp = WarpSection[terrainData.WarpIdxForRetn];
                WarpPlayer(connection, currentWorldIdx, warp.PosXPnt, warp.PosYPnt, 0);
                return;
            }

            // Normal Warp
            WarpLst warpLst = Worlds[currentWorldIdx].NPC.WarpLst.Values.FirstOrDefault(t => t.NpcsIdx == pNpcIdx && t.NSetIdx == pNsetIdx);
            if (warpLst.Equals(default(WarpLst)))
            {
                connection.Disconnect($"Requested an invalid warp (NpcIdx: {pNpcIdx}, NsetIdx: {pNsetIdx})");
                return;
            }

            // TODO: warpLst.LV check

            short sPosX = 0;
            short sPosY = 0;
            short sWorldId = 0;
            short sType = 0;

            switch (warpLst.Type)
            {
                // Normal warp
                case 0:

                    // TODO: If GM warp or Stone Guide

                    WarpRow warp = WarpSection[warpLst.TargetIdx];
                    if (warp.Equals(default(WarpRow)))
                    {
                        connection.Disconnect($"Cannot find WarpIdx {warpLst.TargetIdx} for WarpLst ({warpLst.NpcsIdx}:{warpLst.NSetIdx})");
                        return;
                    }

                    sPosX = warp.PosXPnt;
                    sPosY = warp.PosYPnt;
                    sWorldId = warp.WorldIdx;

                    break;

                // Death warp ?
                case 1:

                    break;

                // Dungeon entrance warp
                case 2:

                    break;

                default:
                    connection.Disconnect($"Invalid WarpLst type {warpLst.Type}");
                    return;
            }

            sType = warpLst.Type;
            WarpPlayer(connection, sWorldId, sPosX, sPosY, sType);
        }

        public static void WarpPlayer(Connection connection, short destWorldIdx, short destPosX, short destPosY, short destType)
        {
            // TODO: Update player position and worldid in memory
            connection.Metadata["WorldIdx"] = destWorldIdx;

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_WARPCOMMAND);

                builder.Write(destPosX); // Dest PosX
                builder.Write(destPosY); // Dest PosY

                builder.Write(666); // Unk0
                builder.Write(new byte[12]);
                builder.Write(0);
                builder.Write(0);
                builder.Write((short)6179); // Unk4
                builder.Write((byte)0x00);
                builder.Write((byte)0x01); // Status ?

                builder.Write(destType); // Type

                builder.Write(new byte[3]);

                builder.Write(destWorldIdx); // Dest WorldId

                builder.Write(new byte[9]);

                connection.Send(builder);
            }
        }
    }
}
