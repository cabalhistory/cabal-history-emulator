﻿using System;
using Shared.Packet;
using Shared.Telepathy;
using static WorldServer.Protodef;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class Inventory
    {
        public static void UseItem(Connection connection, PacketReader packet)
        {
            short pInventorySlotID = packet.ReadShort();

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_ITEMUSING00);
                builder.Write((byte)0x00); //Status 0 = Sucesso , 1 = Erro
     
                connection.Send(builder);
            }
        }

        public static void QueryCashItem(Connection connection, PacketReader packet)
        {
            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_QUERYCASHITEM);
                builder.Write(1); // Quantidade de items

                // Foreach Qtd Items
                builder.Write(0); // Order
                builder.Write(1); // Item ID
                builder.Write(0); // Item option
                builder.Write(0); // Item option 2
                builder.Write(0); // Item Duration

                connection.Send(builder);
            }
        }

        public static void ReceiveQueryCashItem(Connection connection, PacketReader packet)
        {

            int pItemID = packet.ReadInt();
            short pInventorySlotID = packet.ReadShort();

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_USECASHITEM);
  
                // Foreach Qtd Items
                builder.Write(0); // Order
                builder.Write(1); // Cash Item Item ID
                builder.Write(0); // Item option
                builder.Write(0); // Item option 2
                builder.Write((short)pInventorySlotID); // Iventory Slot

                connection.Send(builder);
            }
        }

        public static void OnCSC_EQUIP_UNQUIP_ITEM(Connection connection, PacketReader reader)
        {
            C2S_EQUIP_UNQUIP_ITEM c2sPacket = reader.Packet<C2S_EQUIP_UNQUIP_ITEM>();

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_EQUIP_UNQUIP_ITEM);
                builder.Write((byte)0x01);
                builder.Write(0);
                connection.Send(builder);
            }
        }

        public static void OnCSC_DROPITEM(Connection connection, PacketReader reader)
        {
            C2S_DROPITEM c2sPacket = reader.Packet<C2S_DROPITEM>();

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_DROPITEM);
                builder.Write((byte)0x01);
                connection.Send(builder);
            }
        }

        public static void OnCSC_ITEMLOOTING(Connection connection, PacketReader reader)
        {
            C2S_ITEMLOOTING c2sPacket = reader.Packet<C2S_ITEMLOOTING>();

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_ITEMLOOTING);
                builder.Write((byte)0x96);
                builder.Write(c2sPacket.ItemID);
                builder.Write((long)0);
                builder.Write(c2sPacket.InventorySlotID);
                builder.Write(0);
                connection.Send(builder);
            }

            // TODO: Send NFY_DELITEMLIST to nearby players about this item
        }

        public static void OnCSC_CHANGEITEM(Connection connection, PacketReader reader)
        {
            C2S_CHANGEITEM c2sPacket = reader.Packet<C2S_CHANGEITEM>();

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_CHANGEITEM);
                builder.Write((byte)0x01);
                connection.Send(builder);
            }

        }
    }
}
