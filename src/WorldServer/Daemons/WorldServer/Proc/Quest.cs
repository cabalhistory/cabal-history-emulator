﻿using Shared.Packet;
using Shared.Telepathy;
using System;
using System.Collections.Generic;
using System.Text;
using static WorldServer.Protodef;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class Quest
    {

        public static void QuestStart(Connection connection, PacketReader reader)
        {
            S2C_QUESTOPNEVT c2sPacket = reader.Packet<S2C_QUESTOPNEVT>();

            S2C_QUESTOPNEVT s2cPacket = new S2C_QUESTOPNEVT();
            s2cPacket.Status = 1;

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_QUESTOPNEVT);

                builder.Write(s2cPacket);

                connection.Send(builder);
            }
        }

        public static void QuestNextStep(Connection connection, PacketReader reader)
        {
            C2S_QUESTCLSEVT c2sPacket = reader.Packet<C2S_QUESTCLSEVT>();

            S2C_QUESTCLSEVT s2cPacket = new S2C_QUESTCLSEVT();
            s2cPacket.QuestID = c2sPacket.QuestID;
            s2cPacket.PreviousWithNext = (short)(c2sPacket.Previous + c2sPacket.Next);
            s2cPacket.QuestIDCheck = c2sPacket.QuestIDCheck;
            s2cPacket.Unk0 = 0;

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_QUESTCLSEVT);
                builder.Write(s2cPacket);
                connection.Send(builder);
            }
        }

        public static void QuestEnd(Connection connection, PacketReader reader)
        {
            C2S_QSTNPCACTIN c2sPacket = reader.Packet<C2S_QSTNPCACTIN>();

            S2C_QSTNPCACTIN s2cPacket = new S2C_QSTNPCACTIN();
            s2cPacket.Status = 1;
            s2cPacket.SlotID = c2sPacket.SlotID;
            s2cPacket.Unk0 = 0;
            s2cPacket.RewardEXP = 135;
            s2cPacket.RewardUnk0 = 0;
            s2cPacket.RewardUnk1 = 0;
            s2cPacket.RewardUNk2 = 0;

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_QSTNPCACTIN);

                builder.Write(s2cPacket);

                connection.Send(builder);
            }
        }
    }
}
