﻿using Shared.Packet;
using Shared.Telepathy;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class Acts
    {
        public static void OnREQ_SKILLTOACTS(Connection connection, PacketReader reader)
        {
            // Response
            int pCharID = reader.ReadInt();
            short pId = reader.ReadShort();
            short pUnk1 = reader.ReadShort();

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.NFY_SKILLTOACTS);

                builder.Write(pCharID);
                builder.Write(pCharID);
                builder.Write(pId);
                builder.Write((short)6462);

                connection.Send(builder);
            }
        }
    }
}
