﻿using Shared;
using Shared.Packet;
using Shared.Telepathy;
using System;
using Shared.Extensions;
using Shared.Context;
using static WorldServer.Protodef;
using static Shared.Protodef;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class Subpassword
    {
        public static void OnCSC_SUBPASSWORD_ASK(Connection connection, PacketReader reader)
        {
            if (connection.GetAccountContext(out AccountContext accountContext))
            {
                var s2cPacket = new S2C_SUBPASSWORD_ASK();
                s2cPacket.NeedEnterSubPassword = Convert.ToInt32(!accountContext.IsACSubPasswordRemembered && accountContext.UseACSUB);

                accountContext.ResetSubpasswordAuthorizations();

                connection.Send(Opcode.CSC_SUBPASSWORD_ASK, s2cPacket);
            }
        }

        public static void OnCSC_SUBPASSWORD_SET(Connection connection, PacketReader reader)
        {
            if (connection.GetAccountContext(out AccountContext accountContext))
            {
                var c2sPacket = reader.Packet<C2S_SUBPASSWORD_SET>();

                if (!Enum.TryParse(c2sPacket.Mode.ToString(), out Typedef.SubpasswordSetMode subpasswordMode))
                {
                    connection.Disconnect($"OnCSC_SUBPASSWORD_SET(): Invalid Mode ({c2sPacket.Mode})");
                    return;
                }

                if (!Enum.TryParse(c2sPacket.PwType.ToString(), out Typedef.SubpasswordType subpasswordType))
                {
                    connection.Disconnect($"OnCSC_SUBPASSWORD_SET(): Invalid PwType ({c2sPacket.PwType})");
                    return;
                }

                if (subpasswordMode == Typedef.SubpasswordSetMode.Create)
                {
                    if (subpasswordType == Typedef.SubpasswordType.Account && accountContext.UseACSUB
                        || subpasswordType == Typedef.SubpasswordType.Wharehouse && accountContext.UseWHSUB
                        || subpasswordType == Typedef.SubpasswordType.Equipment && accountContext.UseEQSUB)
                    {
                        connection.Disconnect($"OnCSC_SUBPASSWORD_SET(): Invalid subpassword creation request ({c2sPacket.PwType})");
                        return;
                    }
                }
                else
                {
                    if ( subpasswordType == Typedef.SubpasswordType.Account && (!accountContext.UseACSUB || !accountContext.IsACSecretAnswerAuthorized)
                         || subpasswordType == Typedef.SubpasswordType.Wharehouse && (!accountContext.UseWHSUB || !accountContext.IsWHSecretAnswerAuthorized)
                         || subpasswordType == Typedef.SubpasswordType.Equipment && (!accountContext.UseEQSUB || !accountContext.IsEQSecretAnswerAuthorized))
                    {
                        connection.Disconnect($"OnCSC_SUBPASSWORD_SET(): Invalid subpassword update request ({c2sPacket.PwType})");
                        return;
                    }
                }

                if(c2sPacket.Question <= 0 || c2sPacket.Question > Typedef.MAX_SUBPASSWORD_QUESTION_ID)
                {
                    connection.Disconnect($"OnCSC_SUBPASSWORD_SET(): Invalid SecretQuestion ({c2sPacket.Question})");
                    return;
                }

                accountContext.ResetSubpasswordAuthorizations();

                var ipsPacket = new IPS_GAME_SUBPASSWORD_SET();
                ipsPacket.GroupIdx = Config.GroupIdx;
                ipsPacket.ServerIdx = Config.ServerIdx;

                ipsPacket.SrcTick = connection.Tick;
                ipsPacket.SrcCID = connection.CID;

                ipsPacket.UserNum = accountContext.UserNum;
                ipsPacket.Password = c2sPacket.Password;
                ipsPacket.Question = c2sPacket.Question;
                ipsPacket.Answer = c2sPacket.Answer;
                ipsPacket.PwType = c2sPacket.PwType;
                ipsPacket.Mode = c2sPacket.Mode;

                Program.GlobalMgrClientDaemon.Send(Shared.Opcode.IPC_GAME_SUBPASSWORD_SET, ipsPacket);
            }
        }

        public static void OnCSC_SUBPASSWORD_GET_QA(Connection connection, PacketReader reader)
        {
            if(connection.GetAccountContext(out AccountContext accountContext))
            {
                var c2sPacket = reader.Packet<C2S_SUBPASSWORD_GET_QA>();

                var ipsGameSubpasswordGetQa = new IPS_GAME_SUBPASSWORD_GET_QA();
                ipsGameSubpasswordGetQa.GroupIdx = Config.GroupIdx;
                ipsGameSubpasswordGetQa.ServerIdx = Config.ServerIdx;

                ipsGameSubpasswordGetQa.SrcTick = connection.Tick;
                ipsGameSubpasswordGetQa.SrcCID = connection.CID;

                ipsGameSubpasswordGetQa.UserNum = accountContext.UserNum;
                ipsGameSubpasswordGetQa.PwType = c2sPacket.PwType;

                Program.GlobalMgrClientDaemon.Send(Shared.Opcode.IPC_GAME_SUBPASSWORD_GET_QA, ipsGameSubpasswordGetQa);
            }
        }

        public static void OnCSC_SUBPASSWORD_ANSWER_QA(Connection connection, PacketReader reader)
        {
            if (connection.GetAccountContext(out AccountContext accountContext))
            {
                var c2sPacket = reader.Packet<C2S_SUBPASSWORD_ANSWER_QA>();

                var ipsGameSubpasswordAnswerQA = new IPS_GAME_SUBPASSWORD_ANSWER_QA();
                ipsGameSubpasswordAnswerQA.GroupIdx = Config.GroupIdx;
                ipsGameSubpasswordAnswerQA.ServerIdx = Config.ServerIdx;

                ipsGameSubpasswordAnswerQA.SrcTick = connection.Tick;
                ipsGameSubpasswordAnswerQA.SrcCID = connection.CID;

                ipsGameSubpasswordAnswerQA.UserNum = accountContext.UserNum;
                ipsGameSubpasswordAnswerQA.PwType = c2sPacket.PwType;
                ipsGameSubpasswordAnswerQA.Answer = c2sPacket.Answer;

                Program.GlobalMgrClientDaemon.Send(Shared.Opcode.IPC_GAME_SUBPASSWORD_ANSWER_QA, ipsGameSubpasswordAnswerQA);
            }
        }

        public static void OnCSC_SUBPASSWORD_AUTH(Connection connection, PacketReader reader)
        {
            if (!connection.GetAccountContext(out AccountContext accountContext))
                return;

            var isFastType = reader.Opcode == (ushort)Opcode.CSC_SUBPASSWORD_DEL_AUTH || reader.Opcode == (ushort)Opcode.CSC_SUBPASSWORD_CHANGE_QA_AUTH;

            var ipsGameSubpasswordAuth = new IPS_GAME_SUBPASSWORD_AUTH();
            ipsGameSubpasswordAuth.GroupIdx = Config.GroupIdx;
            ipsGameSubpasswordAuth.ServerIdx = Config.ServerIdx;

            ipsGameSubpasswordAuth.SrcTick = connection.Tick;
            ipsGameSubpasswordAuth.SrcCID = connection.CID;

            ipsGameSubpasswordAuth.UserNum = accountContext.UserNum;
            ipsGameSubpasswordAuth.IsFastAuth = Convert.ToByte(isFastType);

            if (isFastType)
            {
                var c2sPacket = reader.Packet<C2S_SUBPASSWORD_AUTH_FAST>();
                ipsGameSubpasswordAuth.PwType = c2sPacket.PwType;
                ipsGameSubpasswordAuth.Password = c2sPacket.Password;
            }
            else
            {
                var c2sPacket = reader.Packet<C2S_SUBPASSWORD_AUTH>();
                ipsGameSubpasswordAuth.PwType = c2sPacket.PwType;
                ipsGameSubpasswordAuth.RememberHours = c2sPacket.RememberHours;
                ipsGameSubpasswordAuth.Password = c2sPacket.Password;
            }

            Program.GlobalMgrClientDaemon.Send(Shared.Opcode.IPC_GAME_SUBPASSWORD_AUTH, ipsGameSubpasswordAuth);
        }

        public static void OnCSC_SUBPASSWORD_DEL(Connection connection, PacketReader reader)
        {
            if (!connection.GetAccountContext(out AccountContext accountContext))
                return;

            var c2sPacket = reader.Packet<C2S_SUBPASSWORD_DEL>();

            if (!Enum.TryParse(c2sPacket.PwType.ToString(), out Typedef.SubpasswordType subpasswordType))
            {
                connection.LogError($"OnCSC_SUBPASSWORD_DEL(): Invalid PwType ({c2sPacket.PwType})");
                return;
            }

            if (
                   (subpasswordType == Typedef.SubpasswordType.Account && !accountContext.IsACSubPasswordAuthorized)
                || (subpasswordType == Typedef.SubpasswordType.Wharehouse && !accountContext.IsWHSubPasswordAuthorized)
                || (subpasswordType == Typedef.SubpasswordType.Equipment && !accountContext.IsEQSubPasswordAuthorized)
                )
            {
                connection.LogError($"OnCSC_SUBPASSWORD_DEL(): Unauthorized subpassword deletion ({c2sPacket.PwType})");
                return;
            }

            accountContext.ResetSubpasswordAuthorizations();

            var ipsGameSubpasswordDel = new IPS_GAME_SUBPASSWORD_DEL();
            ipsGameSubpasswordDel.GroupIdx = Config.GroupIdx;
            ipsGameSubpasswordDel.ServerIdx = Config.ServerIdx;

            ipsGameSubpasswordDel.SrcTick = connection.Tick;
            ipsGameSubpasswordDel.SrcCID = connection.CID;

            ipsGameSubpasswordDel.UserNum = accountContext.UserNum;
            ipsGameSubpasswordDel.PwType = c2sPacket.PwType;

            Program.GlobalMgrClientDaemon.Send(Shared.Opcode.IPC_GAME_SUBPASSWORD_DEL, ipsGameSubpasswordDel);
        }

        public static void OnCSC_SUBPASSWORD_CHANGE_QA(Connection connection, PacketReader reader)
        {

        }
    }
}
