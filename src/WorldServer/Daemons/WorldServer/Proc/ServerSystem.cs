﻿using Shared;
using Shared.Packet;
using Shared.Telepathy;
using System;
using static WorldServer.Protodef;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class ServerSystem
    {
        public static void SendForcecaliburOwner(ushort connectionId)
        {
            NFS_FORCE_CALIBUR_OWNER s2cPacket = new NFS_FORCE_CALIBUR_OWNER();

            string ownerPlayerName = "Nicke";
            string ownerPlayerGuildName = "CabalHistory";

            s2cPacket.PlayerNameLen = ownerPlayerName.Length;
            s2cPacket.GuildNameLen = ownerPlayerGuildName.Length;
            s2cPacket.PlayerName = ownerPlayerName;
            s2cPacket.GuildName = ownerPlayerGuildName;

            Program.WorldServerDaemon.Send(Opcode.NFY_FORCE_CALIBUR_OWNER, connectionId, s2cPacket);
        }

        #region Receive Methods
        public static void OnCSC_GETSVRTIME(Connection connection, PacketReader reader)
        {
            NFS_GETSVRTIME s2cPacket = new NFS_GETSVRTIME();
            s2cPacket.UnixTimestamp = Utils.UnixTimestamp;
            s2cPacket.Unk0 = 180;

            connection.Send(Opcode.CSC_GETSVRTIME, s2cPacket);
        }

        public static void OnCSC_SERVERENV(Connection connection, PacketReader reader)
        {
            S2C_SERVERENV s2cPacket = new S2C_SERVERENV();
            s2cPacket.MaxLevel = 200;
            s2cPacket.UseDummy = 1;
            s2cPacket.AllowCashShop = 0;
            s2cPacket.AllowNetCafePoint = 0;
            s2cPacket.NormalChatMinLevel = 10;
            s2cPacket.LoudChatMinLevel = 40;
            s2cPacket.LoudChatMinMasteryLevel = 5;
            s2cPacket.MaxInventoryAlz = 150000000000;
            s2cPacket.MaxWarehouseAlz = 150000000000;
            s2cPacket.Unk0 = 140000000000;
            s2cPacket.Unk1 = 1;
            s2cPacket.Unk2 = 0;
            s2cPacket.Unk3 = 0;
            s2cPacket.Unk4 = 1;
            s2cPacket.Unk5 = 0;
            s2cPacket.Unk6 = 10;
            s2cPacket.Unk7 = 10;
            s2cPacket.MinRank = 257;
            s2cPacket.Unk8 = 1;
            s2cPacket.MaxRank = 2570;
            s2cPacket.Unk9 = 100;
            s2cPacket.Unk10 = 7;
            s2cPacket.Unk11 = 1;
            s2cPacket.Unk12 = 4000000000;
            s2cPacket.Unk13 = -2000000000;
            s2cPacket.Unk14 = 0;
            s2cPacket.Unk15 = 2;
            s2cPacket.Unk16 = 1;
            s2cPacket.Unk17 = 16;
            s2cPacket.Unk18 = 15;
            s2cPacket.Unk19 = 2;
            s2cPacket.Unk20 = 109;
            s2cPacket.Unk21 = 110;
            s2cPacket.Unk22 = new byte[255];
            s2cPacket.Unk23 = 5;
            s2cPacket.Unk24 = 0;
            s2cPacket.Unk25 = 255;

            connection.Send(Opcode.CSC_SERVERENV, s2cPacket);
        }

        public static void OnCSC_PING(Connection connection, PacketReader reader)
        {
            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_PING);
                connection.Send(builder);
            }

        }
        #endregion
    }
}
