﻿using Shared;
using Shared.Packet;
using Shared.Telepathy;
using System;
using static Shared.Protodef;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class Connect
    {
        const uint _key = 0x217E6DB9;
        const ushort _step = 0x37E5;

        public static void OnCSC_CONNECT2SVR(Connection connection, PacketReader reader)
        {
            C2S_CONNECT2SERV c2sPacket = reader.Packet<C2S_CONNECT2SERV>();

            // Change connection encryption
            connection.Cryption.ChangeKeychain(_key, _step);

            connection.Trusted = true;

            // Response
            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(Opcode.CSC_CONNECT2SVR);

                builder.Write(_key);
                builder.Write(connection.Tick);
                builder.Write(connection.CID);
                builder.Write(_step);

                connection.Send(builder);
            }
        }
    }
}
