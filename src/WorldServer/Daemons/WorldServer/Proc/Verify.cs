﻿using System;
using Shared.Context;
using Shared.Extensions;
using Shared.Packet;
using Shared.Telepathy;
using static Shared.Protodef;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class Verify
    {
        public static void OnCSC_VERIFYLINKS(Connection connection, PacketReader reader)
        {
            if (!connection.GetAccountContext(out AccountContext accountContext))
                return;
            
            var c2sPacket = reader.Packet<C2S_VERIFYLINKS>();

            var ipsPacket = new IPS_REQ_LINK();
            ipsPacket.SrcTick = connection.Tick;
            ipsPacket.SrcCID = connection.CID;

            ipsPacket.DstTick = c2sPacket.TargetTick;
            ipsPacket.DstCID = c2sPacket.TargetCID;

            ipsPacket.ServerIdx = c2sPacket.ServerIdx;
            ipsPacket.GroupIdx = c2sPacket.GroupIdx;

            ipsPacket.LinkData = new _IPS_LINK_DATA();
            ipsPacket.LinkData.UserNum = accountContext.UserNum;
            ipsPacket.LinkData.ID = accountContext.ID;

            ipsPacket.LinkData.SubpasswordExpiration = accountContext.ACSubPasswordExpiration;

            ipsPacket.LinkData.QtdChars = accountContext.QtdChars;
            ipsPacket.LinkData.CharSlotOrder = accountContext.CharSlotOrder;

            ipsPacket.LinkData.UseACSUB = Convert.ToByte(accountContext.UseACSUB);
            ipsPacket.LinkData.UseWHSUB = Convert.ToByte(accountContext.UseWHSUB);
            ipsPacket.LinkData.UseEQSUB = Convert.ToByte(accountContext.UseEQSUB);
            ipsPacket.LinkData.IsWHLOCK = Convert.ToByte(accountContext.IsWHLOCK);
            ipsPacket.LinkData.IsEQLOCK = Convert.ToByte(accountContext.IsEQLOCK);

            ipsPacket.LinkData.ServiceKind = accountContext.ServiceKind;
            ipsPacket.LinkData.ExpireDate = accountContext.ExpireDate;
            ipsPacket.LinkData.ExtendedCharCreation = accountContext.ExtendedCharCreation;

            Program.GlobalMgrClientDaemon.Send(Shared.Opcode.IPC_REQ_LINK, ipsPacket);
        }
    }
}
