﻿using Shared;
using Shared.Packet;
using Shared.Telepathy;
using static WorldServer.Protodef;

namespace WorldServer.Daemons.WorldServer.Proc
{
    static class Movement
    {
        public static void OnREQ_MOVEBEGINED(Connection connection, PacketReader reader)
        {
            RES_MOVEBEGINED c2sPacket = reader.Packet<RES_MOVEBEGINED>();

            NFS_MOVEBEGINED s2cPacket = new NFS_MOVEBEGINED();
            s2cPacket.CharIdx = 8;
            s2cPacket.dwMvBgnTime = (uint)Utils.TickCount;
            s2cPacket.wPosXBegin = c2sPacket.wPosXBegin;
            s2cPacket.wPosXEnd = c2sPacket.wPosXEnd;
            s2cPacket.wPosYBegin = c2sPacket.wPosYBegin;
            s2cPacket.wPosYEnd = c2sPacket.wPosYEnd;

            // TODO: SEND nfyPacket no NEARBY players
        }

        public static void OnREQ_MOVETILEPOS(Connection connection, PacketReader reader)
        {
            RES_MOVETILEPOS c2sPacket = reader.Packet<RES_MOVETILEPOS>();
        }

        public static void OnREQ_MOVECHANGED(Connection connection, PacketReader reader)
        {
            RES_MOVECHANGED c2sPacket = reader.Packet<RES_MOVECHANGED>();
        }

        public static void OnREQ_MOVEENDED00(Connection connection, PacketReader reader)
        {
            RES_MOVEENDED00 c2sPacket = reader.Packet<RES_MOVEENDED00>();
        }
    }
}
