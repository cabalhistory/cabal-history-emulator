﻿using Shared;
using Shared.Telepathy;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorldServer.Context
{
    class PlayerContext
    {
        public readonly Connection Connection;

        public uint CharacterIdx { get; set; }

        // WorldInformations
        public int DungeonIdx { get; set; }
        public byte WorldIdx { get; set; }
        public short Cell { get; set; }
        public short PosX { get; set; }
        public short PosY { get; set; }

        public bool Spawned
        {
            get
            {
                return !(CharacterIdx == 0);
            }
        }

        private long TickCount;
        public int PlayTime
        {
            get
            {
                return (int)TimeSpan.FromTicks(Utils.TickCount - TickCount).TotalMinutes;
            }
        }

        #region Public Methods
        // Public Methods
        public PlayerContext(Connection connection)
        {
            Connection = connection;
        }

        public void Spawn()
        {
            TickCount = Utils.TickCount;
        }

        public void Despawn()
        {
            CharacterIdx = 0;
        }
        #endregion
    }
}
