﻿using Shared;
using Shared.Packet;

namespace WorldServer
{
    public static class Protodef
    {
        #region Character Lobby

        #region CSC_GETMYCHARTR
        public struct S2C_GETMYCHARTR
        {
            public byte UseACSUB;

            [Field(Length = 12)]
            public byte[] Unk0;

            public byte Unk1;

            public int LastCharacter;
            public int SlotOrder;
            public int ExtendedCharCreation;

            [Field(Length = 8)]
            public S2C_GETMYCHARTR_CHARACTER[] Characters;
        }

        public struct S2C_GETMYCHARTR_CHARACTER
        {
            public int CharacterIdx;
            public long CreateDate;
            public int Style;
            public int Level;

            public int Unk0;

            [Field(Length = 9)]
            public byte[] Unk1;

            public byte WorldIdx;
            public short PosX;
            public short PosY;

            public long HeadItemIdx;
            public long EquipUnk0;

            public long SuitItemIdx;
            public long EquipUnk1;

            public long HandItemIdx;
            public long EquipUnk2;

            public long BootItemIdx;
            public long EquipUnk3;

            public long RWeaponItemIdx;
            public long EquipUnk4;

            public long LWeaponItemIdx;
            public long EquipUnk5;

            public long EpauletItemIdx;
            public long EquipUnk6;

            [Field(Length = 668)]
            public byte[] Empty0;

            public byte NameLen;

            [Field(LengthFromField = "NameLen")]
            public string Name;

            [Field(Length = 8)]
            public byte[] Empty1;
        }
        #endregion

        #region CSC_CHARACTER_SLOTORDER
        public struct C2S_CHARACTER_SLOTORDER
        {
            public int SlotOrder;
        }

        public struct S2C_CHARACTER_SLOTORDER
        {
            public int Status;
        }
        #endregion

        #endregion

        #region Charge

        #region CSC_CHARGEINFO
        public struct S2C_CHARGEINFO
        {
            public int ServiceType;
            public int ServiceKind;
            public int ExpireDate;
        }
        #endregion

        #endregion

        #region Subpassword

        #region CSC_SUBPASSWORD_ASK
        public struct S2C_SUBPASSWORD_ASK
        {
            public int NeedEnterSubPassword;
        }
        #endregion

        #region CSC_SUBPASSWORD_SET
        public struct C2S_SUBPASSWORD_SET
        {
            [Field(Length = Typedef.MAX_SUBPASSWORD_PASSWORD_LEN)]
            public string Password;

            public int PwType;

            public int Question;

            [Field(Length = Typedef.MAX_SUBPASSWORD_ANSWER_LEN)]
            public string Answer;

            [Field(Length = 112)]
            public string Unk0;

            public int Mode;
        }

        public struct S2C_SUBPASSWORD_SET
        {
            public int Status;
            public int Mode;
            public int PwType;
            public int Unk0;
        }
        #endregion

        #region CSC_SUBPASSWORD_GET_QA
        public struct C2S_SUBPASSWORD_GET_QA
        {
            public int PwType;
        }

        public struct S2C_SUBPASSWORD_GET_QA
        {
            public int QuestionId1;
            public int QuestionId2;
            public int PwType;
        }
        #endregion

        #region CSC_SUBPASSWORD_ANSWER_QA
        public struct C2S_SUBPASSWORD_ANSWER_QA
        {
            public int PwType;
            public int QuestionId;

            [Field(Length = Typedef.MAX_SUBPASSWORD_ANSWER_LEN)]
            public string Answer;
        }

        public struct S2C_SUBPASSWORD_ANSWER_QA
        {
            public int Status;
            public byte TryCount;
            public int PwType;
        }
        #endregion

        #region CSC_SUBPASSWORD_AUTH
        public struct C2S_SUBPASSWORD_AUTH
        {
            [Field(Length = Typedef.MAX_SUBPASSWORD_PASSWORD_LEN)]
            public string Password;

            public int PwType;
            public int RememberHours;
            public byte Unk0;
        }

        public struct S2C_SUBPASSWORD_AUTH
        {
            public int Status;
            public int TryNum;
            public byte Unk0;
            public int PwType;
        }
        #endregion

        #region CSC_SUBPASSWORD_AUTH_FAST
        public struct C2S_SUBPASSWORD_AUTH_FAST
        {
            public int PwType;

            [Field(Length = Typedef.MAX_SUBPASSWORD_PASSWORD_LEN)]
            public string Password;
        }

        public struct S2C_SUBPASSWORD_AUTH_FAST
        {
            public int Status;
            public byte TryNum;
            public int PwType;
        }
        #endregion

        #region CSC_SUBPASSWORD_DEL
        public struct C2S_SUBPASSWORD_DEL
        {
            public int PwType;
        }

        public struct S2C_SUBPASSWORD_DEL
        {
            public int Status;
            public int PwType;
        }
        #endregion

        #endregion

        #region Movement

        #region REQ_MOVEBEGINED
        public struct RES_MOVEBEGINED
        {
            public short wPosXBegin;
            public short wPosYBegin;
            public short wPosXEnd;
            public short wPosYEnd;
            public short wPosXPnt;
            public short wPosYPnt;
            public short wWorldIdx;

        }
        
        public struct NFS_MOVEBEGINED
        {
            public int CharIdx;
            public uint dwMvBgnTime;
            public short wPosXBegin;
            public short wPosYBegin;
            public short wPosXEnd;
            public short wPosYEnd;
        }
        #endregion

        #region REQ_MOVETILEPOS
        public struct RES_MOVETILEPOS
        {
            public short wPosXCur;
            public short wPosYCur;
            public short wPosXPnt;
            public short wPosYPnt;
        }
        #endregion

        #region REQ_MOVECHANGED
        public struct RES_MOVECHANGED
        {
            public short wPosXBegin;
            public short wPosYBegin;
            public short wPosXEnd;
            public short wPosYEnd;
            public short wPosXPnt;
            public short wPosYPnt;
            public short wWorldIdx;
        }
        #endregion

        #region REQ_MOVEENDED00
        public struct RES_MOVEENDED00
        {
            public short wPosXPnt;
            public short wPosYPnt;
        }
        #endregion

        #endregion

        #region Quests

        #region CSC_QUESTOPNEVT
        public struct C2S_QUESTOPNEVT
        {
            public short QuestIdx;
        }
        
        public struct S2C_QUESTOPNEVT
        {
            public int Status;
        }
        #endregion

        #region CSC_QSTNPCACTIN
        public struct C2S_QSTNPCACTIN
        {
            public short QuestID;
            public int QuestIDCheck;
            public short SlotID;
        }
        
        public struct S2C_QSTNPCACTIN
        {
            public byte Status;
            public short SlotID;
            public int Unk0;
            public short RewardEXP;
            public short RewardUnk0;
            public short RewardUnk1;
            public short RewardUNk2;
        }
        #endregion

        #region CSC_QUESTCLSEVT
        public struct C2S_QUESTCLSEVT
        {
            public short QuestID;
            public short QuestIDCheck;

            [Field(Length = 3)]
            public byte[] Unk0;

            public byte Previous;
            public byte Next;
        }
        
        public struct S2C_QUESTCLSEVT
        {
            public short QuestID;
            public short PreviousWithNext;
            public short QuestIDCheck;
            public short Unk0;
        }
        #endregion

        #endregion

        #region NpcShop

        #region CSC_LISTNPCITEMS
        public struct C2S_LISTNPCITEMS
        {
            public short PoolID;
        }
     
        public struct S2C_LISTNPCITEMS
        {
            public short PoolID;
            public short QtdItem;

            public S2C_LISTNPCITEMS_ITEM[] Items;
        }

        public struct S2C_LISTNPCITEMS_ITEM
        {
            public short Unk1;
            public short SlotID;
            public long ItemKind;
            public long ItemOpt;
            public short Duration;
            public short Unk2;
            public byte MinLevel;
            public byte MaxLevel;
            public byte Unk3;
            public sbyte Reputation;
            public byte MaxReputation;
            public short Unk4;
            public long AlzPrice;
            public long WexpPrice;
            public long DPPrice;
            public long CashPrice;
        }
        #endregion

        #region CSC_NPCSHOP_BUY
        public struct C2S_NPCSHOP_BUY
        {
            public byte Unk0;
            public int PoolID;
            public short SlotID;
            public long ItemID;
            public long ItemOption;
            public short SlotID2;
            public long Unk1;
            public int Unk2;

            [Field(Length = 533*4)]
            public byte[] Unk3;

            public int InventorySlotID;
        }
        #endregion

        #region C2S_CLOSENPC
        public struct C2S_CLOSENPC
        {
            public byte Check;
        }
        #endregion

        #endregion

        #region Inventory
        public struct C2S_EQUIP_UNQUIP_ITEM
        {
            public int SrcActionType;
            public int SrcTargetIdx;
            public int DstActionType;
            public int DstTargetIdx;
            public short Unk0;
            public short Unk1;
            public int Unk2;
            public int Unk3;
        }
        
        public struct C2S_DROPITEM
        {
            public int Unk0;
            public int InventorySlotID;
            public short Unk1;
            public short Unk2;
            public int Unk3;
            public int Unk4;
        }

        public struct C2S_ITEMLOOTING
        {
            public int SerialKey;
            public ushort ItemMapIdx;
            public long ItemID;
            public short InventorySlotID;
        }
        
        public struct C2S_CHANGEITEM
        {
            public int Unk0;
            public int InventorySlotID;
            public int Unk1;
            public int CharSlotID;
            public int Unk2;
            public int CharSlotID2;
            public int Unk3;
            public int InventorySlotID2;
            public short Unk4;
            public short Unk5;
            public int Unk6;
            public int Unk7;
        }
        #endregion

        #region Server System

        #region NFY_FORCE_CALIBUR_OWNER
        public struct NFS_FORCE_CALIBUR_OWNER
        {
            public int PlayerNameLen;
            public int GuildNameLen;

            [Field(LengthFromField = "PlayerNameLen")]
            public string PlayerName;

            [Field(LengthFromField = "GuildNameLen")]
            public string GuildName;
        }
        #endregion

        #region NFY_GETSVRTIME
        public struct NFS_GETSVRTIME
        {
            public long UnixTimestamp;
            public short Unk0;
        }
        #endregion

        #region CSC_SERVERENV
        public struct S2C_SERVERENV
        {
            public ushort MaxLevel;
            public byte UseDummy;
            public byte AllowCashShop;
            public byte AllowNetCafePoint;
            public ushort NormalChatMinLevel;
            public ushort LoudChatMinLevel;
            public ushort LoudChatMinMasteryLevel;
            public long MaxInventoryAlz;
            public long MaxWarehouseAlz;
            public long Unk0;
            public byte Unk1;
            public int Unk2;
            public short Unk3;
            public int Unk4;
            public short Unk5;
            public short Unk6;
            public short Unk7;
            public short MinRank;
            public short Unk8;
            public short MaxRank;
            public long Unk9;
            public int Unk10;
            public int Unk11;
            public long Unk12;
            public long Unk13;
            public int Unk14;
            public int Unk15;
            public int Unk16;
            public int Unk17;
            public int Unk18;
            public int Unk19;
            public int Unk20;
            public int Unk21;

            [Field(Length = 255)]
            public byte[] Unk22;

            public int Unk23;
            public int Unk24;
            public int Unk25;
        }
        #endregion

        #endregion
    }
}
