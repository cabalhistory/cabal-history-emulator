﻿using Shared;
using System.Collections.Generic;
using System.Globalization;

namespace WorldServer.Data
{
    static class MobData
    {
        public static Dictionary<int, MobsRow> MobsSection = new Dictionary<int, MobsRow>();

        public static void Load(string filePath)
        {
            SCPReader reader = new SCPReader(filePath);

            Logger.Log("Loading mobs...");

            MobsSection = SCPLoader.ToDictionary<MobsRow>(reader.GetSection("[Mobs]"));

            Logger.Log($"{MobsSection.Count} mobs loaded");
        }

        #region Structs
        public struct MobsRow
        {
            public double MoveSpeed;
            public double ChasSpeed;
            public short Property;
            public short AttkPattern;
            public short Aggressive;
            public short Cooperate;
            public short Escape;
            public short Attack;
            public short Scale;
            public int FindCount;
            public short FindInterval;
            public short MoveInterval;
            public short ChasInterval;
            public short AlertRange;
            public short Limt0Range;
            public short Limt1Range;
            public short LEV;
            public uint EXP;
            public int HP;
            public int Defense;
            public int AttacksR;
            public int DefenseR;
            public int HPRechagR;
            public int Interval1;
            public int PhyAttMin1;
            public int PhyAttMax1;
            public short Reach1;
            public short Range1;
            public short Group1;
            public short Stance1;
            public int Interval2;
            public int PhyAttMin2;
            public int PhyAttMax2;
            public short Reach2;
            public short Range2;
            public short Group2;
            public short Stance2;
            public short Boss;
            public short AtkSignal;
            public double Radius;
            public short canatk;
        }
        #endregion
    }
}
