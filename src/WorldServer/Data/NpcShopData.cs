﻿using Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorldServer.Data
{
    class NpcShopData
    {
        public static Dictionary<int, ShopRow> ShopSection = new Dictionary<int, ShopRow>();
        public static Dictionary<int, NpcRow> NpcSection = new Dictionary<int, NpcRow>();
        public static Dictionary<int, ServerRow> ServerSection = new Dictionary<int, ServerRow>();

        public static void Load(string filePath)
        {
            SCPReader reader = new SCPReader(filePath);

            Logger.Log("Loading NPCShops...");

            ShopSection = SCPLoader.ToDictionary<ShopRow>(reader.GetSection("[Shop]"));
            NpcSection = SCPLoader.ToDictionary<NpcRow>(reader.GetSection("[NPC]"));
            ServerSection = SCPLoader.ToDictionary<ServerRow>(reader.GetSection("[Server]"));

            Logger.Log($"NPCShops loaded");
        }
    }

    #region Structs
    struct ShopRow
    {
        public short Pool_ID;
        public short SlotID;
        public long ItemKind;
        public long ItemOpt;
        public short DurationIdx;
        public byte MinLevel;
        public byte MaxLevel;
        public sbyte Reputation;
        public short OnlyPremium;
        public short OnlyWin;
        public long AlzPrice;
        public long WExpPrice;
        public long DPPrice;
        public long CashPrice;
        public short Renew;
        public short ChracterBuyLimit;
        public short SellLimit;
        public short Marker;
        public byte MaxReputation;
    }

    struct NpcRow
    {
        public short Shop_ID;
        public short World_ID;
        public short Order;
        public short NPC_ID;
        public short Pool_ID1;
        public short Pool_ID2;
    }

    struct ServerRow
    {
        public short Server_Index;
        public short Shop_ID;
    }
    #endregion
}
