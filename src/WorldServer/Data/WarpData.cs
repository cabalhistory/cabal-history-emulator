﻿using Shared;
using System.Collections.Generic;

namespace WorldServer.Data
{
    static class WarpData
    {
        public static Dictionary<int, WarpRow> WarpSection = new Dictionary<int, WarpRow>();

        public static void Load(string filePath)
        {
            SCPReader reader = new SCPReader(filePath);

            Logger.Log("Loading warps...");

            WarpSection = SCPLoader.ToDictionary<WarpRow>(reader.GetSection("[Warp]"));

            Logger.Log($"{WarpSection.Count} warps loaded");
        }

        #region Structs
        public struct WarpRow
        {
            public short WorldIdx;
            public short ProcessIdx;
            public short PosXPnt;
            public short PosYPnt;
            public short Nation1PosXPnt;
            public short Nation1PosYPnt;
            public short Nation2PosXPnt;
            public short Nation2PosYPnt;
            public short LV;
            public short Fee;
        }
        #endregion
    }
}
