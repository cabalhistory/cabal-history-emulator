﻿using Shared;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace WorldServer.Data
{
    static class QuestData
    {
        public static Dictionary<int, QuestRow> QuestSection = new Dictionary<int, QuestRow>();
        public static Dictionary<int, QuestRewardRow> QuestRewardSection = new Dictionary<int, QuestRewardRow>();
        public static Dictionary<int, ExclusiveCraftTypeRow> ExclusiveCraftTypeSection = new Dictionary<int, ExclusiveCraftTypeRow>();
        public static Dictionary<int, NpcActionSetRow> NpcActionSetSection = new Dictionary<int, NpcActionSetRow>();

        public static void Load(string filePath)
        {
            SCPReader reader = new SCPReader(filePath);

            Logger.Log("Loading quests...");

            QuestSection = SCPLoader.ToDictionary<QuestRow>(reader.GetSection("[Quest]"));
            QuestRewardSection = SCPLoader.ToDictionary<QuestRewardRow>(reader.GetSection("[QuestReward]"));
            ExclusiveCraftTypeSection = SCPLoader.ToDictionary<ExclusiveCraftTypeRow>(reader.GetSection("[ExclusiveCraftType]"));
            NpcActionSetSection = SCPLoader.ToDictionary<NpcActionSetRow>(reader.GetSection("[NpcActionSet]"));

            Logger.Log($"{QuestSection.Count} quests loaded");
        }
    }

    #region Structs
    struct QuestRow
    {
        public int QuestIdx;
        public short Level;
        public short maxlv;
        public short MaxRank;
        public short RankType;
        public string BattleStyle;
        public string OpenItem;
        public string OpenSkill;
        public short CancelType;
        public short MinReputationClass;
        public short MaxReputationClass;
        public long PenaltyEXP;
        public short MissionNPCSet;
        public short Reward;
        public short UseDungeon;
        public string MissionItem;
        public string MissionMob;
        public short MissionDungeon;
        public string OpenNpcs;
        public string CloseNpcs;
        public short QuestType;
        public short PartyQuest;
        public short DeleteType;
        public short DailyCount;
        public short Nation_Type;
        public string ExclusiveCraft;
        public short CommonCraftLevel;
        public short Mission_Player;
    }

    struct QuestRewardRow
    {
        public int RwdIdx;
        public short Order;
        public long Exp;
        public long Alz;
        public int MapCode;
        public int WarpCode;
        public int SkillIdx;
        public long RewardItemIdx;
        public uint Reputation;
        public long SkillEXP;
        public long AXP;
        public long CraftEXP;
        public long PetEXP;
        public long GuildEXP;
    }

    struct ExclusiveCraftTypeRow
    {
        public int ExclusiveCraftType;
        public short Main;
        public short Sub;
    }

    struct NpcActionSetRow
    {
        public int SetIdx;
        public short Order;
        public string ActNpc;
        public short ActIdx;
        public long ItemKindCode;
        public long ItemOpt;
        public short Action;
    }
    #endregion
}
