﻿using Shared;
using System.Collections.Generic;
using System.Globalization;

namespace WorldServer.Data
{
    static class WorldData
    {
        public static Dictionary<short, World> Worlds = new Dictionary<short, World>();

        public static void Load(Dictionary<short, CommonWorld> commonWorlds)
        {
            Logger.Log("Loading worlds...");

            foreach(var commonWorld in commonWorlds)
            {
                Worlds.Add(commonWorld.Key, new World()
                {
                    MMap = LoadMMap(commonWorld.Value.MobsMap),
                    NPC = LoadNPC(commonWorld.Value.Npc),
                    Terrain = LoadTerrain(commonWorld.Value.Terrain)
                });
            }

            Logger.Log($"{Worlds.Count} worlds loaded");
        }


        private static MMapData LoadMMap(string filePath)
        {
            SCPReader reader = new SCPReader(filePath);

            return new MMapData()
            {
                MMap = SCPLoader.ToDictionary<MMap>(reader.GetSection("[MMap]")),
                MissionMMAP = SCPLoader.ToDictionary<MissionMMAP>(reader.GetSection("[MissionMMAP]"))
            };
        }

        private static NPCData LoadNPC(string filePath)
        {
            SCPReader reader = new SCPReader(filePath);

            return new NPCData()
            {
                NpcPos = SCPLoader.ToDictionary<NpcPos>(reader.GetSection("[NpcPos]")),
                WarpLst = SCPLoader.ToDictionary<WarpLst>(reader.GetSection("[WarpLst]")),
                SkillLst = SCPLoader.ToDictionary<SkillLst>(reader.GetSection("[SkillLst]"))
            };
        }

        private static TerrainData LoadTerrain(string filePath)
        {
            SCPReader reader = new SCPReader(filePath);

            return new TerrainData()
            {
                Terrain = SCPLoader.ToDictionary<Terrain>(reader.GetSection("[Terrain]")),
                CommDrop = SCPLoader.ToDictionary<CommDrop>(reader.GetSection("[CommDrop]")),
                MobsDrop = SCPLoader.ToDictionary<MobsDrop>(reader.GetSection("[MobsDrop]")),
                MobsDropMission = SCPLoader.ToDictionary<MobsDropMission>(reader.GetSection("[MobsDropMission]"))
            };
        }

        #region Structs
        public struct World
        {
            public MMapData MMap;
            public NPCData NPC;
            public TerrainData Terrain;
        }

        #region MMap Structs
        public struct MMapData
        {
            public Dictionary<int, MMap> MMap;
            public Dictionary<int, MissionMMAP> MissionMMAP;
        }

        public struct MMap
        {
            public short SpeciesIdx;
            public short PosX;
            public short PosY;
            public short Width;
            public short Height;
            public int SpwnInterval;
            public short SpawnDefault;
            public short EvtProperty;
            public short EvtMobs;
            public int EvtInterval;
            public short MissionGate;
            public short PerfectDrop;
            public short Type;
            public short Min;
            public short Max;
            public short Authority;
            public short Server_Mob;
            public int Loot_Delay;
        }

        public struct MissionMMAP
        {
            public short QDungeonIdx;
            public short MMapidx;
            public string Cells;
        }
        #endregion

        #region NPC Structs
        public struct NPCData
        {
            public Dictionary<int, NpcPos> NpcPos;
            public Dictionary<int, WarpLst> WarpLst;
            public Dictionary<int, SkillLst> SkillLst;
        }

        public struct NpcPos
        {
            public short Flags;
            public short Index;
            public short PosX;
            public short PosY;
            public short Type;
            public bool IsRangeCheck;
        }

        public struct WarpLst
        {
            public short NpcsIdx;
            public short NSetIdx;
            public ushort TargetIdx;
            public short LV;
            public short Fee;
            public short Type;
        }

        public struct SkillLst
        {
            public short NSetIdx;
            public short NpcsIdx;
            public short SkillIdx;
            public short SkillLv;
            public int SkillBook;
        }
        #endregion

        #region Terrain Structs
        public struct TerrainData
        {
            public Dictionary<int, Terrain> Terrain;
            public Dictionary<int, CommDrop> CommDrop;
            public Dictionary<int, MobsDrop> MobsDrop;
            public Dictionary<int, MobsDropMission> MobsDropMission;
        }

        public struct Terrain
        {
            public short TerrainX;
            public short TerrainY;
            public int WarpIdxForDead;
            public int WarpIdxForRetn;
            public int WarpIdxForLOut;
            public short DmgMin;
            public short DmgMax;
            public short WarControl;
        }

        public struct CommDrop
        {
            public int TerrainIdx;
            public long ItemKind;
            public long ItemOpt;
            public double DropRate;
            public short MinLv;
            public short MaxLv;
            public short Group;
            public short MaxDropCnt;
            public short OptPoolIdx;
            public short DurationIdx;
        }

        public struct MobsDrop
        {
            public int TerrainIdx;
            public int SpeciesIdx;
            public long ItemKind;
            public long ItemOpt;
            public double DropRate;
            public short MinLv;
            public short MaxDropCnt;
            public short OptPoolIdx;
            public short DurationIdx;
        }

        public struct MobsDropMission
        {
            public int TerrainIdx;
            public int SpeciesIdx;
            public long ItemKind;
            public long ItemOpt;
            public double DropRate;
            public short MaxDropCnt;
        }
        #endregion

        #endregion
    }
}
