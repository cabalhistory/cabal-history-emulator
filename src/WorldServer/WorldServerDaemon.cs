﻿using System;
using System.Net;
using Shared.Packet;
using Shared.Telepathy;
using WorldServer.Daemons.WorldServer;

namespace WorldServer
{
    class WorldServerDaemon : ServerDaemon
    {
        public WorldServerDaemon(IPAddress sv_addr, ushort sv_port, ushort sv_threads, ushort sv_frequency) 
            : base(sv_addr, sv_port, sv_threads, sv_frequency)
        {
            // Execute before server starts

            // Register Packets
            Procs.Register(_packetsHandler);
        }

        protected override void OnConnect(Connection connection)
        {
            base.OnConnect(connection);
        }

        protected override void OnReceiveData(Connection connection, PacketReader reader)
        {
            base.OnReceiveData(connection, reader);

            // Execute logic
            _packetsHandler.Execute(connection, reader);
        }

        protected override void OnDisconnect(Connection connection)
        {
            // Queue save user data
            // Address not available in this time because TcpClient has closed

            base.OnDisconnect(connection);
        }
    }
}
