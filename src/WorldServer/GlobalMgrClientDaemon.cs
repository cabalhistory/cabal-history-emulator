﻿using System;
using System.Net;
using System.Threading;
using Shared;
using Shared.Packet;
using Shared.Telepathy;
using WorldServer.Daemons.GlobalMgrClient;
using static Shared.Protodef;

namespace WorldServer
{
    class GlobalMgrClientDaemon : ClientDaemon
    {
        public GlobalMgrClientDaemon(IPAddress serverAddress, ushort serverPort, int connectionTimeout) 
            : base(serverAddress, serverPort, connectionTimeout)
        {
            Procs.Register(_packetsHandler);
        }

        protected override void OnConnect()
        {
            base.OnConnect();

            IPS_CONNECT ipsPacket = new IPS_CONNECT();
            ipsPacket.IAC = Typedef.IAC_GLOBALMGRSERVER;

            Send(Shared.Opcode.IPC_CONNECT, ipsPacket);
        }

        protected override void OnReceiveData(PacketReader reader)
        {
            base.OnReceiveData(reader);

            _packetsHandler.Execute(reader);
        }

        protected override void OnDisconnect()
        {
            base.OnDisconnect();

            DaemonManager.StopAll();
        }
    }
}
