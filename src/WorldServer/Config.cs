﻿using System;
using System.Net;
using Shared;

namespace WorldServer
{
    class Config
    {
        public static IPAddress IP { get; private set; }
        public static ushort Port { get; private set; }

        public static Logger.LOG_LEVEL LogLevel { get; private set; }
        public static string LogPath { get; private set; }
        public static ushort Threads { get; private set; }
        public static ushort Frequency { get; private set; }

        public static byte GroupIdx { get; private set; }
        public static byte ServerIdx { get; private set; }
        public static string CommonIniPath { get; private set; }
        public static ushort MaxPlayers { get; private set; }
        public static long ChannelType { get; private set; }

        public static IPAddress GMSIP { get; set; }
        public static ushort GMSPort { get; set; }
        public static int GMSConnectionTimeOut { get; set; }

        public static IPAddress DBAIP { get; set; }
        public static ushort DBAPort { get; set; }
        public static int DBAConnectionTimeout { get; set; }

        public static void Load(string iniFileName)
        {
            IniReader parser = new IniReader(iniFileName);

            const string LISTEN_SECTION = "Listen";
            IP = IPAddress.Parse(parser.GetValue(LISTEN_SECTION, "IP"));
            Port = ushort.Parse(parser.GetValue(LISTEN_SECTION, "Port"));

            const string SERVER_SECTION = "Server";
            Enum.TryParse(parser.GetValue(SERVER_SECTION, "LogLevel"), out Logger.LOG_LEVEL _logLevel);

            LogLevel = _logLevel;
            LogPath = parser.GetValue(SERVER_SECTION, "LogPath");
            Threads = ushort.Parse(parser.GetValue(SERVER_SECTION, "Threads"));
            Frequency = ushort.Parse(parser.GetValue(SERVER_SECTION, "Frequency"));

            const string WORLD_SECTION = "WorldServer";
            GroupIdx = byte.Parse(parser.GetValue(WORLD_SECTION, "GroupIdx"));
            ServerIdx = byte.Parse(parser.GetValue(WORLD_SECTION, "ServerIdx"));
            CommonIniPath = parser.GetValue(WORLD_SECTION, "CommonIniPath");
            MaxPlayers = ushort.Parse(parser.GetValue(WORLD_SECTION, "MaxPlayers"));
            ChannelType = long.Parse(parser.GetValue(WORLD_SECTION, "ChannelType"));

            const string GLOBALMGRSERVER_SECTION = "GlobalMgrServer";
            GMSIP = IPAddress.Parse(parser.GetValue(GLOBALMGRSERVER_SECTION, "GMSIP"));
            GMSPort = ushort.Parse(parser.GetValue(GLOBALMGRSERVER_SECTION, "GMSPort"));
            GMSConnectionTimeOut = int.Parse(parser.GetValue(GLOBALMGRSERVER_SECTION, "GMSConnectionTimeOut"));

            const string DBAGENT_SECTION = "DBAgent";
            DBAIP = IPAddress.Parse(parser.GetValue(DBAGENT_SECTION, "DBAIP"));
            DBAPort = ushort.Parse(parser.GetValue(DBAGENT_SECTION, "DBAPort"));
            DBAConnectionTimeout = int.Parse(parser.GetValue(DBAGENT_SECTION, "DBAConnectionTimeout"));
        }
    }
}
