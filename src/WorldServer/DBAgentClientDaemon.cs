﻿using Shared;
using Shared.Packet;
using Shared.Telepathy;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using WorldServer.Daemons.DBAgentClient;
using static Shared.Protodef;

namespace WorldServer
{
    class DBAgentClientDaemon : ClientDaemon
    {
        public DBAgentClientDaemon(IPAddress serverAddress, ushort serverPort, int connectionTimeout)
            : base(serverAddress, serverPort, connectionTimeout)
        {
            Procs.Register(_packetsHandler);
        }

        protected override void OnConnect()
        {
            base.OnConnect();

            var ipsConnect = new IPS_CONNECT();
            ipsConnect.IAC = Typedef.IAC_DBAGENT;

            Send(Shared.Opcode.IPC_CONNECT, ipsConnect);
        }

        protected override void OnReceiveData(PacketReader reader)
        {
            base.OnReceiveData(reader);

            _packetsHandler.Execute(reader);
        }

        protected override void OnDisconnect()
        {
            base.OnDisconnect();
        }
    }
}
