﻿using System;
using System.IO;
using Shared;
using System.Threading;
using WorldServer.Data;
using System.Globalization;

namespace WorldServer
{
    class Program
    {
        public static WorldServerDaemon WorldServerDaemon { get; private set; }
        public static GlobalMgrClientDaemon GlobalMgrClientDaemon { get; private set; }
        public static DBAgentClientDaemon DBAgentClientDaemon { get; private set; }

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledException.HandleUnhandledException;

            // Set culture
            Utils.SetCulture();

            // Load .ini configuration
            Config.Load(args[1]);

            // Start logger service
            Logger.Start(args[0], Config.LogPath, Config.LogLevel);

            // Load common.ini
            Common.Load(Config.CommonIniPath);

            // Data load
            MobData.Load(Common.Mobs);
            WarpData.Load(Common.Warp);
            WorldData.Load(Common.World);
            QuestData.Load(Common.Quest);
            NpcShopData.Load(Common.NPCShop);

            #region Daemons
            WorldServerDaemon = new WorldServerDaemon(Config.IP, Config.Port, Config.Threads, Config.Frequency);
            GlobalMgrClientDaemon = new GlobalMgrClientDaemon(Config.GMSIP, Config.GMSPort, Config.GMSConnectionTimeOut);
            DBAgentClientDaemon = new DBAgentClientDaemon(Config.DBAIP, Config.DBAPort, Config.DBAConnectionTimeout);

            DaemonManager.Register(DBAgentClientDaemon);
            DaemonManager.Register(GlobalMgrClientDaemon);
            DaemonManager.Register(WorldServerDaemon);
            #endregion

            DaemonManager.StartAll();
        }
    }
}
