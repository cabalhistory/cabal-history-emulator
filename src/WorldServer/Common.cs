﻿using Shared;
using System.Collections.Generic;
using System.Linq;

namespace WorldServer
{
    class Common
    {
        public static string Warp { get; private set; }
        public static string Mobs { get; private set; }
        public static string Quest { get; private set; }
        public static string NPCShop { get; private set; }


        public static Dictionary<short, Dictionary<string, string>> Warmap { get; private set; }
        public static Dictionary<short, CommonWorld> World { get; private set; }

        public static void Load(string commonIniPath)
        {
            Warmap = new Dictionary<short, Dictionary<string, string>>();
            World = new Dictionary<short, CommonWorld>();

            IniReader parser = new IniReader(commonIniPath);

            LoadWorldsConfiguration(parser);

            string GLOBAL_SECTION = "Global";
            Warp = parser.GetValue(GLOBAL_SECTION, "Warp");
            Mobs = parser.GetValue(GLOBAL_SECTION, "Mobs");
            Quest = parser.GetValue(GLOBAL_SECTION, "Quest");
            NPCShop = parser.GetValue(GLOBAL_SECTION, "NPCShop");

        }

        private static void LoadWorldsConfiguration(IniReader parser)
        {
            var worldsKeys = parser.sections.Where(s => s.Key.Contains("World")).Select(s => s.Key).ToList();

            worldsKeys.ForEach(key =>
            {
                short.TryParse(parser.GetValue(key, "WorldType"), out var worldType);
                short.TryParse(parser.GetValue(key, "SkillExp"), out var skillExp);

                var world = new CommonWorld();

                world.SkillExp = skillExp;
                world.WorldType = worldType;
                world.MobsMap = parser.GetValue(key, "MobsMap");
                world.Npc = parser.GetValue(key, "Npc");
                world.Terrain = parser.GetValue(key, "Terrain");
                world.ThreadMap = parser.GetValue(key, "ThreadMap");

                short.TryParse(key.Replace("World-", ""), out var keyWorld);

                World.Add(keyWorld, world);
            });
        }
    }

    //struct CommonWarMap
    //{
    //    public string MobsMap;
    //}

    struct CommonWorld
    {
        public short WorldType;
        public string ThreadMap;
        public string MobsMap;
        public string Npc;
        public string Terrain;
        public short SkillExp;
    }
}
