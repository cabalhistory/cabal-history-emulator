﻿// common code used by server and client
using System;
using System.Collections.Concurrent;
using System.Net.Sockets;
using System.Threading;

namespace Shared.Telepathy
{
    public abstract class Common
    {
        // common code /////////////////////////////////////////////////////////
        // incoming message queue of <connectionId, message>
        // (not a HashSet because one connection can have multiple new messages)
        protected SafeQueue<Message> receiveQueue = new SafeQueue<Message>();

        // queue count, useful for debugging / benchmarks
        public int ReceiveQueueCount => receiveQueue.Count;

        // warning if message queue gets too big
        // if the average message is about 20 bytes then:
        // -   1k messages are   20KB
        // -  10k messages are  200KB
        // - 100k messages are 1.95MB
        // 2MB are not that much, but it is a bad sign if the caller process
        // can't call GetNextMessage faster than the incoming messages.
        public static int messageQueueSizeWarning = 100000;

        // removes and returns the oldest message from the message queue.
        // (might want to call this until it doesn't return anything anymore)
        // -> Connected, Data, Disconnected events are all added here
        // -> bool return makes while (GetMessage(out Message)) easier!
        // -> no 'is client connected' check because we still want to read the
        //    Disconnected message after a disconnect
        public bool GetMessages(out Message[] messages)
        {
            return receiveQueue.TryDequeueAll(out messages);
        }

        // NoDelay disables nagle algorithm. lowers CPU% and latency but
        // increases bandwidth
        public bool NoDelay = true;

        // Send would stall forever if the network is cut off during a send, so
        // we need a timeout (in milliseconds)
        public int SendTimeout = 5000;

        // static helper functions /////////////////////////////////////////////
        // send message (via stream) with the <size,content> message structure
        // this function is blocking sometimes!
        // (e.g. if someone has high latency or wire was cut off)
        protected static bool SendMessagesBlocking(NetworkStream stream, Cryption cryption, byte[][] messages)
        {
            // stream.Write throws exceptions if client sends with high
            // frequency and the server stops
            try
            {
                // we might have multiple pending messages. merge into one
                // packet to avoid TCP overheads and improve performance.
                /*int packetSize = 0;
                for (int i = 0; i < messages.Length; ++i)
                    packetSize += messages[i].Length; // header + content

                // create the packet
                byte[] payload = new byte[packetSize];
                int position = 0;
                for (int i = 0; i < messages.Length; ++i)
                {
                    // construct header (size)
                    //byte[] header = Utils.IntToBytesBigEndian(messages[i].Length);

                    // copy header + message into buffer
                    //Array.Copy(header, 0, payload, position, header.Length);
                    Array.Copy(messages[i], 0, payload, position, messages[i].Length);
                    position +=  messages[i].Length;
                }*/

                // write the whole thing
                // Array.Resize(ref payload, payload.Length);
                for (int i = 0; i < messages.Length; ++i)
                {
                    if(cryption != null)
                        stream.Write(cryption.Encrypt(messages[i], messages[i].Length), 0, messages[i].Length);
                    else
                        stream.Write(messages[i], 0, messages[i].Length);
                }

                return true;
            }
            catch (Exception exception)
            {
                // log as regular message because servers do shut down sometimes
                // TODO: Log here!
                Logger.Error("Send: stream.Write exception: " + exception);
                return false;
            }
        }

        // read message (via stream) with the <size,content> message structure
        protected static bool ReadMessageBlocking(NetworkStream stream, Cryption cryption, int headerSize, out byte[] content, out int contentSize)
        {
            content = null;
            contentSize = 0;

            // read header
            byte[] header = new byte[headerSize];
            if (!stream.ReadExactly(header, headerSize))
                return false;

            // get packet size and create content byte array
            contentSize = (cryption != null) ? cryption.GetPacketSize(ref header, 0) : BitConverter.ToUInt16(header, 2);

            content = new byte[contentSize];

            // get payload ( contentSize - header size )
            byte[] payload = new byte[contentSize - headerSize];
            if (!stream.ReadExactly(payload, contentSize - headerSize))
                return false;

            // copy header and payload to content array
            Array.ConstrainedCopy(header, 0, content, 0, headerSize);
            Array.ConstrainedCopy(payload, 0, content, headerSize, payload.Length);

            // decrypt
            if (cryption != null)
                cryption.Decrypt(ref content, 0, contentSize);

            return true;
        }

        // thread receive function is the same for client and server's clients
        // (static to reduce state for maximum reliability)
        protected static void ReceiveLoop(ushort connectionId, TcpClient client, SafeQueue<Message> receiveQueue, Cryption cryption, int headerSize)
        {
            // get NetworkStream from client
            NetworkStream stream = client.GetStream();

            // keep track of last message queue warning
            DateTime messageQueueLastWarning = DateTime.Now;

            // absolutely must wrap with try/catch, otherwise thread exceptions
            // are silent
            try
            {
                // add connected event to queue with ip address as data in case
                // it's needed
                receiveQueue.Enqueue(new Message(connectionId, EventType.Connected, null, 0));

                // let's talk about reading data.
                // -> normally we would read as much as possible and then
                //    extract as many <size,content>,<size,content> messages
                //    as we received this time. this is really complicated
                //    and expensive to do though
                // -> instead we use a trick:
                //      Read(2) -> size
                //        Read(size) -> content
                //      repeat
                //    Read is blocking, but it doesn't matter since the
                //    best thing to do until the full message arrives,
                //    is to wait.
                // => this is the most elegant AND fast solution.
                //    + no resizing
                //    + no extra allocations, just one for the content
                //    + no crazy extraction logic
                while (true)
                {
                    // read the next message (blocking) or stop if stream closed

                    byte[] payload;
                    int payloadSize;

                    if (!ReadMessageBlocking(stream, cryption, headerSize, out payload, out payloadSize))
                        break;

                    // queue it
                    receiveQueue.Enqueue(new Message(connectionId, EventType.Data, payload, payloadSize));
                    

                    // and show a warning if the queue gets too big
                    // -> we don't want to show a warning every single time,
                    //    because then a lot of processing power gets wasted on
                    //    logging, which will make the queue pile up even more.
                    // -> instead we show it every 10s, so that the system can
                    //    use most it's processing power to hopefully process it.
                    if (receiveQueue.Count > messageQueueSizeWarning)
                    {
                        TimeSpan elapsed = DateTime.Now - messageQueueLastWarning;
                        if (elapsed.TotalSeconds > 10)
                        {
                            // TODO: Log here!
                            //Logger.LogWarning("ReceiveLoop: messageQueue is getting big(" + receiveQueue.Count + "), try calling GetNextMessage more often. You can call it more than once per frame!");
                            messageQueueLastWarning = DateTime.Now;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                // something went wrong. the thread was interrupted or the
                // connection closed or we closed our own connection or ...
                // -> either way we should stop gracefully
                // TODO: Log here!
                Logger.Error("ReceiveLoop: finished receive function for connectionId=" + connectionId + " reason: " + exception);
            }

            // clean up no matter what
            stream.Close();
            client.Close();

            // add 'Disconnected' message after disconnecting properly.
            // -> always AFTER closing the streams to avoid a race condition
            //    where Disconnected -> Reconnect wouldn't work because
            //    Connected is still true for a short moment before the stream
            //    would be closed.
            receiveQueue.Enqueue(new Message(connectionId, EventType.Disconnected, null, 0));
        }

        // thread send function
        // note: we really do need one per connection, so that if one connection
        //       blocks, the rest will still continue to get sends
        protected static void SendLoop(ushort connectionId, TcpClient client, SafeQueue<byte[]> sendQueue, ManualResetEvent sendPending, Cryption cryption)
        {
            // get NetworkStream from client
            NetworkStream stream = client.GetStream();

            try
            {
                while (client.Connected) // try this. client will get closed eventually.
                {
                    // reset ManualResetEvent before we do anything else. this
                    // way there is no race condition. if Send() is called again
                    // while in here then it will be properly detected next time
                    // -> otherwise Send might be called right after dequeue but
                    //    before .Reset, which would completely ignore it until
                    //    the next Send call.
                    sendPending.Reset(); // WaitOne() blocks until .Set() again

                    // dequeue all
                    // SafeQueue.TryDequeueAll is twice as fast as
                    // ConcurrentQueue, see SafeQueue.cs!
                    byte[][] messages;
                    if (sendQueue.TryDequeueAll(out messages))
                    {
                        // send message (blocking) or stop if stream is closed
                        if (!SendMessagesBlocking(stream, cryption, messages))
                            return;
                    }

                    // don't choke up the CPU: wait until queue not empty anymore
                    sendPending.WaitOne();
                }
            }
            catch (ThreadAbortException)
            {
                // happens on stop. don't log anything.
            }
            catch (ThreadInterruptedException)
            {
                // happens if receive thread interrupts send thread.
            }
            catch (Exception exception)
            {
                // something went wrong. the thread was interrupted or the
                // connection closed or we closed our own connection or ...
                // -> either way we should stop gracefully
                // TODO: Log here!
                Logger.Error("SendLoop Exception: connectionId=" + connectionId + " reason: " + exception);
            }
        }
    }
}
