﻿// incoming message queue of <connectionId, message>
// (not a HashSet because one connection can have multiple new messages)
// -> a class, so that we don't copy the whole struct each time
namespace Shared.Telepathy
{
    public class Message
    {
        public ushort connectionId;

        public EventType eventType;

        public byte[] payload;
        public int payloadSize;

        public Message(ushort connectionId, EventType eventType, byte[] payload, int payloadSize)
        {
            this.connectionId = connectionId;
            this.eventType = eventType;
            this.payload = payload;
            this.payloadSize = payloadSize;
        }
    }
}