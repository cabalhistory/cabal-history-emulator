﻿namespace Shared.Telepathy
{
    public enum EventType
    {
        Connected,
        Data,
        Disconnected
    }
}