﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Shared.Telepathy
{
    public class Server : Common
    {
        // listener
        public TcpListener listener;
        Thread listenerThread;

        // class with all the client's data. let's call it Token for consistency
        // with the async socket methods.
        class ClientToken
        {
            public TcpClient client;
            public Cryption cryption;

            // send queue
            // SafeQueue is twice as fast as ConcurrentQueue, see SafeQueue.cs!
            public SafeQueue<byte[]> sendQueue = new SafeQueue<byte[]>();

            // ManualResetEvent to wake up the send thread. better than Thread.Sleep
            // -> call Set() if everything was sent
            // -> call Reset() if there is something to send again
            // -> call WaitOne() to block until Reset was called
            public ManualResetEvent sendPending = new ManualResetEvent(false);

            public ClientToken(TcpClient client, Cryption cryption)
            {
                this.client = client;
                this.cryption = cryption;
            }
        }
        // clients with <connectionId, ClientData>
        ConcurrentDictionary<ushort, ClientToken> clients = new ConcurrentDictionary<ushort, ClientToken>();

        int counter = 0;

        public ushort NextConnectionId()
        {
            int id = Interlocked.Increment(ref counter);

            if (id == ushort.MaxValue)
                id = Interlocked.Exchange(ref counter, 0);

            while (clients.ContainsKey((ushort)id))
            {
                id = Interlocked.Increment(ref counter);

                if (id == ushort.MaxValue)
                {
                    id = Interlocked.Exchange(ref counter, 0);
                    continue;
                }
            }

            return (ushort)id;
        }

        // check if the server is running
        public bool Active => listenerThread != null && listenerThread.IsAlive;

        // Cryption
        public bool UseEncryption { get; set; } = true;

        // Header size
        public int HeaderSize { get; set; } = 10;

        // the listener thread's listen function
        // note: no maxConnections parameter. high level API should handle that.
        //       (Transport can't send a 'too full' message anyway)
        void Listen(IPAddress address, int port)
        {
            // absolutely must wrap with try/catch, otherwise thread
            // exceptions are silent
            try
            {
                // keep accepting new clients
                while (true)
                {
                    // wait and accept new client
                    // note: 'using' sucks here because it will try to
                    // dispose after thread was started but we still need it
                    // in the thread
                    TcpClient client = listener.AcceptTcpClient();

                    // generate the next connection id (thread safely)
                    ushort connectionId = NextConnectionId();

                    // add to dict immediately
                    ClientToken token = new ClientToken(client, UseEncryption ? new Cryption() : null);
                    clients[connectionId] = token;

                    // spawn a send thread for each client
                    Thread sendThread = new Thread(() =>
                    {
                        // wrap in try-catch, otherwise Thread exceptions
                        // are silent
                        try
                        {
                            // run the send loop
                            SendLoop(connectionId, client, token.sendQueue, token.sendPending, token.cryption);
                        }
                        catch (ThreadAbortException)
                        {
                            // happens on stop. don't log anything.
                            // (we catch it in SendLoop too, but it still gets
                            //  through to here when aborting. don't show an
                            //  error.)
                        }
                        catch (Exception exception)
                        {
                            // TODO: Log here!
                            Logger.Error("Server client send thread exception: " + exception);
                        }
                    });
                    sendThread.Name = connectionId + " SendThread";
                    sendThread.IsBackground = true;
                    sendThread.Start();

                    // spawn a receive thread for each client
                    Thread receiveThread = new Thread(() =>
                    {
                        // wrap in try-catch, otherwise Thread exceptions
                        // are silent
                        try
                        {
                            // run the receive loop
                            ReceiveLoop(connectionId, client, receiveQueue, token.cryption, HeaderSize);

                            // remove client from clients dict afterwards
                            clients.TryRemove(connectionId, out ClientToken c);
                            
                            // sendthread might be waiting on ManualResetEvent,
                            // so let's make sure to end it if the connection
                            // closed.
                            // otherwise the send thread would only end if it's
                            // actually sending data while the connection is
                            // closed.
                            sendThread.Interrupt();
                        }
                        catch (Exception exception)
                        {
                            // TODO: Log here!
                            Logger.Error("Server client receive thread exception: " + exception);
                        }
                    });
                    receiveThread.Name = connectionId + " ReceiveThread";
                    receiveThread.IsBackground = true;
                    receiveThread.Start();
                }
            }
            catch (ThreadAbortException)
            {
                // UnityEditor causes AbortException if thread is still
                // running when we press Play again next time. that's okay.
                // TODO: Log here!
                // Logger.Warn("Server thread aborted. That's okay. " + exception);
            }
            catch (SocketException)
            {
                // calling StopServer will interrupt this thread with a
                // 'SocketException: interrupted'. that's okay.
                // TODO: Log here!
                // Logger.Log("Server Thread stopped.");
            }
            catch (Exception exception)
            {
                // something went wrong. probably important.
                // TODO: Log here!
                Logger.Error($"Server Exception: {exception.Message}\n{exception.StackTrace}");
            }
        }

        // start listening for new connections in a background thread and spawn
        // a new thread for each one.
        public bool Start(IPAddress address, int port)
        {
            // not if already started
            if (Active)
            {
                Logger.Error("The server is already started");
                return false;
            }

            // clear old messages in queue, just to be sure that the caller
            // doesn't receive data from last time and gets out of sync.
            // -> calling this in Stop isn't smart because the caller may
            //    still want to process all the latest messages afterwards
            receiveQueue = new SafeQueue<Message>();

            // start the listener thread
            // (on low priority. if main thread is too busy then there is not
            //  much value in accepting even more clients

            // start listener
            listener = new TcpListener(new IPEndPoint(address, port));
            listener.Server.NoDelay = NoDelay;
            listener.Server.SendTimeout = SendTimeout;
            listener.Start();

            listenerThread = new Thread(() => { Listen(address, port); });
            listenerThread.Name = "Server Listener Thread";
            listenerThread.IsBackground = true;
            listenerThread.Priority = ThreadPriority.BelowNormal;
            listenerThread.Start();

            return true;
        }

        public void Stop()
        {
            // only if started
            if (!Active) return;

            // stop listening to connections so that no one can connect while we
            // close the client connections
            // (might be null if we call Stop so quickly after Start that the
            //  thread was interrupted before even creating the listener)
            listener?.Stop();

            // kill listener thread at all costs. only way to guarantee that
            // .Active is immediately false after Stop.
            // -> calling .Join would sometimes wait forever
            listenerThread?.Interrupt();
            listenerThread = null;

            // close all client connections
            foreach (KeyValuePair<ushort, ClientToken> kvp in clients)
            {
                TcpClient client = kvp.Value.client;
                // close the stream if not closed yet. it may have been closed
                // by a disconnect already, so use try/catch
                try { client.GetStream().Close(); } catch {}
                client.Close();
            }

            // clear clients list
            clients.Clear();
        }

        // send message to client using socket connection.
        public bool Send(ushort connectionId, byte[] data = null)
        {
            // find the connection
            ClientToken token;
            if (clients.TryGetValue(connectionId, out token))
            {
                // add to send queue and return immediately.
                // calling Send here would be blocking (sometimes for long times
                // if other side lags or wire was disconnected)
                token.sendQueue.Enqueue(data ?? new byte[0]);
                token.sendPending.Set(); // interrupt SendThread WaitOne()
                return true;
            }
            // TODO: Log here!
            Logger.Warn("Server.Send: invalid connectionId: " + connectionId);
            return false;
        }

        // get connection info in case it's needed (IP etc.)
        // (we should never pass the TcpClient to the outside)
        public bool GetAddress(ushort connectionId, out IPAddress address)
        {
            address = null;

            try
            {
                ClientToken token;
                if (clients.TryGetValue(connectionId, out token))
                {
                    address = ((IPEndPoint)token.client.Client.RemoteEndPoint).Address;

                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool GetFullAddress(ushort connectionId, out string address)
        {
            address = null;

            try
            {
                ClientToken token;
                if (clients.TryGetValue(connectionId, out token))
                {
                    address = ((IPEndPoint)token.client.Client.RemoteEndPoint).ToString();

                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool GetCryption(ushort connectionId, out Cryption cryption)
        {
            cryption = null;

            try
            {
                ClientToken token;
                if (clients.TryGetValue(connectionId, out token))
                {
                    cryption = token.cryption;
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        // disconnect (kick) a client
        public bool Disconnect(ushort connectionId)
        {
            try
            {
                ClientToken token;
                if (clients.TryGetValue(connectionId, out token))
                {
                    token.client.Close();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}
