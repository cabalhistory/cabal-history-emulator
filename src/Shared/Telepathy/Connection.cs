﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Net;
using Shared.Packet;

namespace Shared.Telepathy
{
    public class Connection : IDisposable
    {
        bool disposed = false;

        Server server;

        public ushort CID { get; private set; }
        public uint Tick { get; private set; }
        public RSA RSA { get; private set; }

        public bool Trusted { get; set; }

        public ConcurrentDictionary<string, object> Metadata { get; set; }

        public Cryption Cryption => GetCryption();
        public string FullAddress => GetFullAddress();
        public IPAddress Address => GetAddress();

        #region Public Methods
        public Connection(Server server, ushort connectionId)
        {
            this.server = server;

            CID = connectionId;
            Tick = Utils.TickSec;
            Trusted = false;
            Metadata = new ConcurrentDictionary<string, object>();
        }

        public void InitRSA()
        {
            RSA = new RSA();
        }

        public void Send(byte[] data)
        {
            server.Send(CID, data);
        }

        public void Send(PacketBuilder builder)
        {
            server.Send(CID, builder.Data);
        }

        public void Send<T1>(T1 opcode) where T1 : IConvertible
        {
            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(opcode);
                server.Send(CID, builder.Data);
            }
        }

        public void Send<T1, T2>(T1 opcode, params T2[] packet) where T1 : IConvertible where T2 : struct
        {
            using(PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(opcode);

                foreach(T2 t in packet)
                    builder.Write(t);

                server.Send(CID, builder.Data);
            }
        }

        public void Log(string text)
        {
            Logger.Log($"[CID { CID } ({ FullAddress })]: { text }");
        }

        public void LogWarning(string text)
        {
            Logger.Warn($"[CID { CID } ({ FullAddress })]: { text }");
        }

        public void LogError(string text)
        {
            Logger.Error($"[CID { CID } ({ FullAddress })]: { text }");
        }

        public void Disconnect(string errorLog = null)
        {
            if (!string.IsNullOrEmpty(errorLog))
                LogError(errorLog);

            server.Disconnect(CID);
            Dispose();
        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
        }
        #endregion

        #region Private Methods

        private IPAddress GetAddress()
        {
            if (!disposed && server.GetAddress(CID, out IPAddress address))
            {
                return address;
            }

            return null;
        }

        private string GetFullAddress()
        {
            string address;

            if (!disposed && server.GetFullAddress(CID, out address))
            {
                return address;
            }

            return null;
        }

        private Cryption GetCryption()
        {
            Cryption cryption;

            if (server.GetCryption(CID, out cryption))
            {
                return cryption;
            }

            return null;
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                //
                Metadata.Clear();

                disposed = true;
            }

            // Free any unmanaged objects here.
            //
            
        }
        #endregion
    }
}
