﻿using Shared.Packet;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Shared.Telepathy
{
    public class ClientDaemon
    {
        #region Public Members
        public Cryption Cryption { get; set; }

        public string DaemonName { get; private set; }
        public bool PersistentConnection { get; set; }

        public Client Client { get; private set; }

        public PacketHandler _packetsHandler;

        public bool Active
        {
            get
            {
                return Client.Connected || Client.Connecting || ((_clientThread != null) && _clientThread.ThreadState == System.Threading.ThreadState.Running);
            }
        }

        public EventHandler OnStart;
        public EventHandler OnStop;
        #endregion

        #region Private Members
        private IPAddress _serverAddr;
        private ushort _serverPort;
        private int _connection_timeout;
        private Thread _clientThread;
        private CancellationTokenSource _cancellationTokenSource;
        
        private int _max_connect_attempts;
        private int _connectAttemptNum;
        private bool _must_die;
        #endregion

        #region Public Methods
        public ClientDaemon(IPAddress sv_addr, ushort sv_port, int connection_timeout, int max_connect_attempts = 3)
        {
            DaemonName = GetType().Name;
            PersistentConnection = false;

            Client = new Client();
            Cryption = null;

            _packetsHandler = new PacketHandler(DaemonName);

            _serverAddr = sv_addr;
            _serverPort = sv_port;
            _connection_timeout = connection_timeout;
            _max_connect_attempts = max_connect_attempts;
            _must_die = false;
        }

        public bool Connect()
        {
            if (Active)
                return false;

            Logger.Log($"{DaemonName} connecting to {_serverAddr}:{_serverPort}");

            _connectAttemptNum = 1;

            while (true)
            {
                if (ConnectClient())
                    break;

                _connectAttemptNum++;

                if (_connectAttemptNum > _max_connect_attempts)
                {
                    return false;
                }
            }

            _cancellationTokenSource = new CancellationTokenSource();
            _clientThread = new Thread(() => DoClientWork(_cancellationTokenSource.Token));
            _clientThread.Name = $"{DaemonName} Worker";
            _clientThread.Priority = ThreadPriority.AboveNormal;
            _clientThread.IsBackground = false;
            _clientThread.Start();

            Logger.Log($"{DaemonName} successfuly connected to {_serverAddr}:{_serverPort}");
            OnStart?.Invoke(this, EventArgs.Empty);

            return true;
        }

        private bool ConnectClient()
        {
            try
            {
                Client.Connect(_serverAddr, _serverPort, Cryption);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error($"{DaemonName} failed to connect ({_connectAttemptNum} of {_max_connect_attempts}). Reason: {ex.Message}");
                OnStop?.Invoke(this, EventArgs.Empty);
                return false;
            }
        }

        private void DoClientWork(CancellationToken token)
        {
            Message[] messages;
            BlockingCollection<Task> logicTasks = new BlockingCollection<Task>();
    
            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    _cancellationTokenSource.Dispose();
                    break;
                }

                if (Client.GetMessages(out messages))
                {
                    for (int j = 0; j < messages.Length; j++)
                    {
                        // DONT put Connect and Disconnect events on parallelism!
                        if (messages[j].eventType != EventType.Data)
                        {
                            ProcessMessage(messages[j]);
                            continue;
                        }

                        // Put logic on parallelism
                        Message message = messages[j];
                        logicTasks.Add(Task.Factory.StartNew(() => ProcessMessage(message))
                            .ContinueWith(task => {
                                logicTasks.TryTake(out task);
                            })
                        );
                    }
                }

                Thread.Sleep(1000 / 60);
            }

            logicTasks.CompleteAdding();

            // Wait all tasks complete before stop
            foreach (Task task in logicTasks)
                task.Wait();

            logicTasks.Dispose();

            OnStop?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnConnect()
        {
            
        }

        protected virtual void OnReceiveData(PacketReader reader)
        {
        }

        protected virtual void OnDisconnect()
        {
            Logger.Log($"{DaemonName} disconnected");

            // Try again
            if (PersistentConnection && !_must_die)
            {
                while (true)
                {
                    if (Connect())
                        break;
                }
            }
        }

        public bool Send(byte[] data)
        {
            return Client.Send(data);
        }

        public void Send<T1, T2>(T1 opcode, params T2[] packet) where T1 : IConvertible where T2 : struct
        {
            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(opcode);

                foreach (T2 t in packet)
                    builder.Write(t);

                Client.Send(builder.Data);
            }
        }

        public void Disconnect(bool mustDie = false)
        {
            _must_die = mustDie;

            Client.Disconnect();
        }
        #endregion

        #region Private Methods
        private void ProcessMessage(Message message)
        {
            try
            {
                switch (message.eventType)
                {
                    case EventType.Connected:
                        OnConnect();
                        break;

                    case EventType.Data:
                        using(PacketReader reader = new PacketReader(ref message.payload, message.payloadSize, 6))
                        {
                            OnReceiveData(reader);
                        }

                        break;

                    case EventType.Disconnected:
                        _cancellationTokenSource.Cancel();
                        OnDisconnect();
                        break;
                }
            }
            catch (TargetInvocationException ex)
            {
                // logic exception generated
                Logger.Error($"Logic Exception [{DaemonName}::{ex.InnerException.Source}]: {ex.InnerException.Message}\n{ex.InnerException.StackTrace}");
                Client.Disconnect();
                return;
            }
            catch (Exception ex)
            {
                // common exception generated
                Logger.Error($"Exception [{DaemonName}::{ex.Source}]: {ex.Message}\n{ex.StackTrace}");
                Client.Disconnect();
                return;
            }
        }
        #endregion
    }
}
