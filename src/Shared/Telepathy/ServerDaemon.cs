﻿using Shared.Packet;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shared.Telepathy
{
    public class ServerDaemon
    {
        #region Public Members
        public string DaemonName { get; private set; }

        public Server server;

        public PacketHandler _packetsHandler;
        public ConcurrentDictionary<int, Connection> _connections;

        public IPAddress _serverAddr { get; private set; }
        public ushort _serverPort { get; private set; }
        public ushort _sv_threads { get; private set; }
        public ushort _sv_frequency { get; private set; }

       
        public bool Active {
            get
            {
                return server.Active || _svThreads.Count > 0;
            }
        }

        public EventHandler OnStart;
        public EventHandler OnStop;
        #endregion

        #region Private Members
        private Dictionary<ushort, Thread> _svThreads;
        private Dictionary<ushort, CancellationTokenSource> _svTokens;
        #endregion

        #region Public Methods
        public ServerDaemon(IPAddress sv_addr, ushort sv_port, ushort sv_threads, ushort sv_frequency)
        {
            DaemonName = GetType().Name;

            server = new Server();

            _packetsHandler = new PacketHandler(DaemonName);
            _connections = new ConcurrentDictionary<int, Connection>();

            _serverAddr = sv_addr;
            _serverPort = sv_port;
            _sv_threads = sv_threads;
            _sv_frequency = sv_frequency;

            _svThreads = new Dictionary<ushort, Thread>();
            _svTokens = new Dictionary<ushort, CancellationTokenSource>();
        }

        public bool Start()
        {
            Logger.Log($"{DaemonName} Starting TCP server with {_sv_threads} workers at {_sv_frequency} frequency");

            // Start telepathy server and spawn threads
            if (server.Start(_serverAddr, _serverPort))
            {
                for (ushort i = 1; i <= _sv_threads; i++)
                {
                    CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
                    Thread tServerWorker = new Thread(() => DoServerWork(cancellationTokenSource.Token, i));

                    tServerWorker.Name = $"{DaemonName} Server Worker";
                    tServerWorker.Priority = ThreadPriority.AboveNormal;
                    tServerWorker.IsBackground = false;
                    tServerWorker.Start();

                    _svThreads.Add(i, tServerWorker);
                    _svTokens.Add(i, cancellationTokenSource);
                }

                Logger.Log($"{DaemonName} TCP server successfully started and accepting connections at {_serverAddr}:{_serverPort}");

                OnStart?.Invoke(this, EventArgs.Empty);

                return true;
            }

            return false;
        }

        public void Stop()
        {
            Logger.Log($"{DaemonName} shutdown");

            server.Stop();

            // Request threads finalization
            foreach (CancellationTokenSource token in _svTokens.Values)
                token.Cancel();

            // TODO: Wait all worker threads finish before call OnStop
            OnStop?.Invoke(this, EventArgs.Empty);
        }

        #region Send raw byte[] methods
        public bool Send(ushort connectionId, byte[] data)
        {
            return server.Send(connectionId, data);
        }
        #endregion

        #region Send packet byte[] methods
        public void Send<T1>(T1 opcode, ushort connectionId, byte[] packetbytes) where T1 : IConvertible
        {
            if (!_connections.ContainsKey(connectionId))
                return;

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(opcode);

                builder.Write(packetbytes);

                server.Send(connectionId, builder.Data);
            }
        }

        public void Send<T1>(T1 opcode, IEnumerable<ushort> connections, byte[] packetbytes) where T1 : IConvertible
        {
            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(opcode);

                builder.Write(packetbytes);

                foreach (var connectionId in connections)
                {
                    if (!_connections.ContainsKey(connectionId))
                        continue;

                    server.Send(connectionId, builder.Data);
                }
            }
        }
        #endregion

        #region Send packet typed methods
        public void Send<T1, T2>(T1 opcode, ushort connectionId, params T2[] packet) where T1 : IConvertible where T2 : struct
        {
            if (!_connections.ContainsKey(connectionId))
                return;

            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(opcode);

                foreach (T2 t in packet)
                    builder.Write(t);

                server.Send(connectionId, builder.Data);
            }
        }

        public void Send<T1, T2>(T1 opcode, IEnumerable<ushort> connections, params T2[] packet) where T1 : IConvertible where T2 : struct
        {
            using (PacketBuilder builder = new PacketBuilder())
            {
                builder.WriteOpcode(opcode);

                foreach (T2 t in packet)
                    builder.Write(t);

                foreach(var connectionId in connections)
                {
                    if (!_connections.ContainsKey(connectionId))
                        continue;

                    server.Send(connectionId, builder.Data);
                }
            }
        }
        #endregion

        #region Connection helpers
        public bool GetConnection(ushort cid, out Connection connection)
            => GetConnection(cid, null, out connection);

        public bool GetConnection(ushort cid, uint? tick, out Connection connection)
        {
            connection = null;
            if (!_connections.ContainsKey(cid) || (tick != null && _connections[cid].Tick != tick))
            {
                MethodBase caller = new StackTrace().GetFrame(1)?.GetMethod();
                Logger.Error($"{DaemonName}::{ (caller != null ? (caller.DeclaringType.Name + "::" + caller.Name) : "") }: Can't find connection. (CID: {cid}{(tick != null ? ", Tick: " + tick : "")})");
                return false;
            }

            connection = _connections[cid];
            return true;
        }
        #endregion

        public bool Disconnect(ushort connectionId)
        {
            return server.Disconnect(connectionId);
        }

        protected virtual void OnConnect(Connection connection)
        {
#if DEBUG
            connection.Log("connected");
#endif
        }

        protected virtual void OnReceiveData(Connection connection, PacketReader reader)
        {
        }

        protected virtual void OnDisconnect(Connection connection)
        {
#if DEBUG
            connection.Log("disconnected");
#endif
        }
        #endregion

        #region Private Methods
        private void DoServerWork(CancellationToken token, ushort workerNum)
        {
            Message[] messages;
            BlockingCollection<Task> logicTasks = new BlockingCollection<Task>();

            while (true)
            {
                if (server.GetMessages(out messages))
                {
                    for (int j = 0; j < messages.Length; j++)
                    {
                        // DONT put Connect and Disconnect events on parallelism!
                        if (messages[j].eventType != EventType.Data)
                        {
                            ProcessMessage(messages[j]);
                            continue;
                        }

                        // Put logic on parallelism
                        Message message = messages[j];
                        logicTasks.Add(Task.Factory.StartNew(() => ProcessMessage(message)).ContinueWith(task => logicTasks.TryTake(out task)));
                    }
                } else
                {
                    if (token.IsCancellationRequested)
                        break;
                }

                Thread.Sleep(1000 / _sv_frequency);
            }

            logicTasks.CompleteAdding();

            Task.WaitAll(logicTasks.ToArray());

            // Wait all tasks complete before end
            //foreach (Task task in logicTasks)
            //    task.Wait();

            logicTasks.Dispose();
            _svThreads.Remove(workerNum);
        }

        private void ProcessMessage(Message msg)
        {
            try
            {
                switch (msg.eventType)
                {
                    case EventType.Connected:
                        Connection connection = new Connection(server, msg.connectionId);
                        _connections.TryAdd(msg.connectionId, connection);

                        OnConnect(connection);
                        break;

                    case EventType.Data:
                        if (_connections.ContainsKey(msg.connectionId))
                        {
                            using (PacketReader reader = new PacketReader(ref msg.payload, msg.payloadSize, server.HeaderSize))
                            {
                                OnReceiveData(_connections[msg.connectionId], reader);
                            }
                        }
                        break;

                    case EventType.Disconnected:
                        if (_connections.ContainsKey(msg.connectionId))
                        {
                            if (_connections.TryRemove(msg.connectionId, out Connection connectionDataClosed))
                            {
                                OnDisconnect(connectionDataClosed);
                                connectionDataClosed.Dispose();
                            }
                        }
                        break;
                }
            }
            catch (TargetInvocationException ex)
            {
                // logic exception generated
                Logger.Error($"Logic Exception [{DaemonName}::{ex.InnerException.Source}]: {ex.InnerException.Message}\n{ex.InnerException.StackTrace}");
                server.Disconnect(msg.connectionId);
                return;
            }
            catch (Exception ex)
            {
                // common exception generated
                Logger.Error($"Exception [{DaemonName}::{ex.Source}]: {ex.Message}\n{ex.StackTrace}");
                server.Disconnect(msg.connectionId);
                return;
            }
        }
        #endregion
    }
}
