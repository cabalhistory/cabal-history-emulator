﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Shared
{
    public class SCPReader
    {
        private Dictionary<string, Dictionary<string, Dictionary<string, string>>> _data;
        private Dictionary<string, string> _variables;

        public SCPReader(string filePath)
        {
            using (StreamReader stream = new StreamReader(filePath, Encoding.UTF8))
            {
                _data = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();
                _variables = new Dictionary<string, string>();

                string line;
                string[] splitted;
                string[] currentSection = null;

                while (true)
                {
                    line = stream.ReadLine();
                    if (line == null)
                        break;

                    if (line == string.Empty)
                        continue;

                    // Variable
                    if (line.StartsWith("@"))
                    {
                        splitted = line.Split('=');
                        _variables.Add(splitted[0].Trim(), splitted[1].Trim());
                        continue;
                    }

                    splitted = line.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                    // New section
                    if (line.StartsWith("["))
                    {
                        currentSection = splitted;

                        _data.Add(currentSection[0], new Dictionary<string, Dictionary<string, string>>());

                        continue;
                    }

                    // Insert row to section
                    if(currentSection != null && splitted.Length == currentSection.Length)
                    {
                        _data[currentSection[0]].Add(splitted[0], new Dictionary<string, string>());

                        for (int i = 1; i < splitted.Length; i++)
                        {
                            _data[currentSection[0]][splitted[0]].Add(currentSection[i], GetValue(splitted[i]));
                        }
                    }
                }
            }
        }

        public Dictionary<string, Dictionary<string, string>> GetSection(string section)
        {
            return _data[section];
        }
        
        private string GetValue(string originalValue)
        {
            if (originalValue == "<null>")
                return null;

            string returnValue = originalValue;

            foreach(string variable in _variables.Keys)
            {
                if(Regex.Match(returnValue, variable).Success)
                {
                    returnValue = Regex.Replace(returnValue, variable, _variables[variable]);
                }
            }

            return returnValue;
        }
    }
}
