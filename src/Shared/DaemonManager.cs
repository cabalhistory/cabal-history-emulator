﻿using Shared.Telepathy;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
    public static class DaemonManager
    {

        private static List<ServerDaemon> _serverDaemons = new List<ServerDaemon>();
        private static List<ClientDaemon> _clientDaemons = new List<ClientDaemon>();

        public static void Register(ServerDaemon serverDaemon)
        {
            _serverDaemons.Add(serverDaemon);
        }

        public static void Register(ClientDaemon clientDaemon)
        {
            _clientDaemons.Add(clientDaemon);
        }

        public static void StartAll()
        {
            foreach (ClientDaemon clientDaemon in _clientDaemons)
            {
                if (!clientDaemon.Connect())
                {
                    StopAll();
                    return;
                }
                    
            }

            foreach (ServerDaemon serverDaemon in _serverDaemons)
            {
                if (!serverDaemon.Start())
                {
                    StopAll();
                    return;
                }
            }
        }

        public static void StopAll()
        {
            foreach (ServerDaemon serverDaemon in _serverDaemons)
            {
                if (serverDaemon.Active)
                    serverDaemon.Stop();
            }

            foreach (ClientDaemon clientDaemon in _clientDaemons)
            {
                if (clientDaemon.Active)
                    clientDaemon.Disconnect(true);
            }

            Logger.Stop();
        }
    }
}
