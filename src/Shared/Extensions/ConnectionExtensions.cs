﻿using Shared.Context;
using Shared.Telepathy;
using System;

namespace Shared.Extensions
{
    public static class ConnectionExtensions
    {
        const string CTX_ACCOUNT_KEY = "CTX_ACCOUNT";

        public static bool InitAccountContext(this Connection connection, out AccountContext accountContext)
        {
            accountContext = null;

            try
            {
                accountContext = new AccountContext(connection);
                if (!connection.Metadata.TryAdd(CTX_ACCOUNT_KEY, accountContext))
                    throw new Exception($"Can't add accountContext to connection metadata (CID {connection.CID}, Tick {connection.Tick})");

                return true;
            }
            catch(Exception ex)
            {
                Logger.Error($"ConnectionExtensions::InitAccountContext(): {ex.Message}");
                return false;
            }
        }

        public static bool GetAccountContext(this Connection connection, out AccountContext accountContext)
        {
            accountContext = null;

            try
            {
                if(!connection.Metadata.TryGetValue(CTX_ACCOUNT_KEY, out object accountContextObj))
                    throw new Exception($"Can't get accountContext from connection metadata (CID {connection.CID}, Tick {connection.Tick})");

                accountContext = (AccountContext)accountContextObj;

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error($"ConnectionExtensions::GetAccountContext(): {ex.Message}");
                return false;
            }
        }
    }
}
