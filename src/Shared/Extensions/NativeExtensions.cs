﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Extensions
{
    public static class NativeExtensions
    {
        public static bool AsBool(this int value) => Convert.ToBoolean(value);
    }
}
