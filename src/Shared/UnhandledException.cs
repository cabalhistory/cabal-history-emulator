﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Shared
{
    public static class UnhandledException
    {
        public static void HandleUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = default(Exception);
            ex = (Exception)e.ExceptionObject;

            string logFileName = string.Format("{0}.crashreport_{1}.log", AppDomain.CurrentDomain.FriendlyName, DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
            StreamWriter logFile = null;

            if (!File.Exists(logFileName))
                logFile = new StreamWriter(logFileName);
            else
                logFile = File.AppendText(logFileName);

            logFile.WriteLine(ex.Message);
            logFile.WriteLine(ex.StackTrace);

            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);

            logFile.Close();
            logFile.Dispose();

            Environment.Exit(-1);
        }
    }
}
