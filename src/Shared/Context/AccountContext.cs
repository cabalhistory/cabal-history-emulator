﻿using Shared;
using Shared.Telepathy;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Context
{
    public class AccountContext
    {
        public readonly Connection Connection;

        public int UserNum { get; set; }
        public string ID { get; set; }

        public bool IsACSubPasswordAuthorized { get; set; }
        public bool IsWHSubPasswordAuthorized { get; set; }
        public bool IsEQSubPasswordAuthorized { get; set; }
        public bool IsACSecretAnswerAuthorized { get; set; }
        public bool IsWHSecretAnswerAuthorized { get; set; }
        public bool IsEQSecretAnswerAuthorized { get; set; }

        public int QtdChars { get; set; }
        public int CharSlotOrder { get; set; }

        public bool UseACSUB { get; set; }
        public bool UseWHSUB { get; set; }
        public bool UseEQSUB { get; set; }
        public bool IsWHLOCK { get; set; }
        public bool IsEQLOCK { get; set; }

        public int ServiceKind { get; set; }
        public int ExpireDate { get; set; }
        public int ExtendedCharCreation { get; set; }

        public int ACSubPasswordExpiration { get; set; }
        public bool IsACSubPasswordRemembered
        {
            get
            {
                return ACSubPasswordExpiration > Utils.UnixTimestamp;
            }
        }

        public readonly long TickCount;
        public int PlayTime {
            get
            {
                return (int)TimeSpan.FromTicks(Utils.TickCount - TickCount).TotalMinutes;
            }
        }
        
        public void ResetSubpasswordAuthorizations()
        {
            IsACSubPasswordAuthorized = false;
            IsWHSubPasswordAuthorized = false;
            IsEQSubPasswordAuthorized = false;
            IsACSecretAnswerAuthorized = false;
            IsWHSecretAnswerAuthorized = false;
            IsEQSecretAnswerAuthorized = false;
        }

        public AccountContext(Connection connection)
        {
            Connection = connection;
            TickCount = Utils.TickCount;
        }
    }
}
