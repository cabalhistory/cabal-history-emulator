﻿using System;

namespace Shared.Packet
{
    public class FieldAttribute : Attribute
    {
        public int Length;
        public string LengthFromField;

        public FieldAttribute()
        {
            Length = 0;
        }
    }
}
