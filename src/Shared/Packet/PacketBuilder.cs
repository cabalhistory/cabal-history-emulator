﻿using Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;

namespace Shared.Packet
{
    public class PacketBuilder : IDisposable
    {
        List<byte> _data;
        bool _compressed;
        int _size;

        private bool disposed = false;

        public byte[] Data
        {
            get
            {
                _data[2] = (byte)_size;
                _data[3] = (byte)(_size >> 8);

                return _data.ToArray();
            }
        }

        public int Size
        {
            get
            {
                return _size;
            }
        }

        public PacketBuilder(bool compressed = false)
        {
            _data = new List<byte>();
            _compressed = compressed;

            if (!compressed)
            {
                _data.AddRange(new byte[6]);

                _data[0] = 0xE2;
                _data[1] = 0xB7;

                _size = 6;
            }
            else
            {
                _data.AddRange(new byte[8]);

                _data[0] = 0xF3;
                _data[1] = 0xC8;

                _size = 8;
            }
        }

        public void WriteOpcode<TOpcode>(TOpcode opcode) where TOpcode : IConvertible
        {
            if (Enum.IsDefined(typeof(TOpcode), opcode))
            {
                if (!_compressed)
                {
                    _data[4] = (byte)((ushort)(IConvertible)opcode);
                    _data[5] = (byte)((ushort)(IConvertible)opcode >> 8);
                }
                else
                {
                    _data[6] = (byte)((ushort)(IConvertible)opcode);
                    _data[7] = (byte)((ushort)(IConvertible)opcode >> 8);
                }
            }
        }

        /// <summary>Appends data to the end of the packet.</summary>
        /// <param name="packet">this</param>
        /// <param name="value">The data to be added to the end of the packet.</param>
        /// <returns>this</returns>
        ///
        public void Write<T>(T packet)
        {
            TypedReference refPacket = __makeref(packet);
            Type tPacket = packet.GetType();

            Type tField;

            object fieldInstance;
            int fieldLength = 0;

            MethodInfo method;

            FieldAttribute packetFieldAttribute;
            FieldInfo packetFieldAttributeLengthField;

            int length = 0;

            byte[] placeholder;

            if (tPacket.IsArray)
            {
                foreach(object packetVal in (((Array)(object)packet)))
                {
                    Write(packetVal);
                }

                return;
            }

            // Foreach all fields
            foreach (FieldInfo field in tPacket.GetFields())
            {
                tField = field.FieldType;
                fieldInstance = field.GetValueDirect(refPacket);

                // Recursive if field is array
                if (tField.IsArray && tField != typeof(byte[]))
                {
                    foreach (object arrayVal in ((Array)fieldInstance))
                    {
                        Write(arrayVal);
                    }

                    continue;
                }

                // Inheritance
                if (tField.IsValueType && !tField.IsPrimitive)
                {
                    Write(fieldInstance);
                    continue;
                }

                if (tField == typeof(string) || tField == typeof(byte[]))
                {
                    packetFieldAttribute = field.GetCustomAttribute<FieldAttribute>();

                    if (packetFieldAttribute == null)
                        throw new Exception($"({tPacket.FullName} {field.Name}) must have fixed or variable length attribute");

                    length = packetFieldAttribute.Length;

                    if (!string.IsNullOrEmpty(packetFieldAttribute.LengthFromField))
                    {
                        packetFieldAttributeLengthField = tPacket.GetField(packetFieldAttribute.LengthFromField);
                        if(packetFieldAttributeLengthField == null)
                            throw new Exception($"({tPacket.FullName} {field.Name}) not found lengthable field ({packetFieldAttribute.LengthFromField})");

                        length = (int)Convert.ChangeType(packetFieldAttributeLengthField.GetValueDirect(refPacket), typeof(int));
                    }

                    if (tField == typeof(string))
                    {
                        // Populate with placeholder (string)
                        if (fieldInstance != null)
                        {
                            fieldLength = ((string)fieldInstance).Length;
                            if (fieldLength > length)
                                throw new Exception($"({tPacket.FullName} {field.Name}) wrong length ({fieldLength})! Max: {length}");

                            placeholder = Encoding.ASCII.GetBytes(new string('\0', length));
                            Array.ConstrainedCopy(Encoding.ASCII.GetBytes((string)fieldInstance), 0, placeholder, 0, fieldLength);

                            fieldInstance = Encoding.ASCII.GetString(placeholder);
                        }
                        else
                        {
                            // Default instance
                            fieldInstance = new string('\0', length);
                        }
                    }
                    else if (tField == typeof(byte[]))
                    {
                        // Populate with placeholder (byte[])
                        if (fieldInstance != null)
                        {
                            fieldLength = ((byte[])fieldInstance).Length;
                            if (fieldLength > length)
                                throw new Exception($"({tPacket.FullName} {field.Name}) wrong length ({fieldLength})! Max: {length}");

                            placeholder = new byte[length];
                            Array.ConstrainedCopy((byte[])fieldInstance, 0, placeholder, 0, fieldLength);

                            fieldInstance = placeholder;
                            //placeholder.CopyTo((byte[])fieldInstance, 0);
                        }
                        else
                        {
                            // Default instance
                            fieldInstance = new byte[length];
                        }
                    }
                }

                // Get method
                method = GetType().GetMethod("Write", BindingFlags.Public | BindingFlags.Instance, null, new Type[] { tField }, null);
                if (method == null)
                    throw new Exception($"({tPacket.FullName} {field.Name}) Cannot find PacketBuilder Write method for this type");

                // Write it
                method.Invoke(this, new object[] { fieldInstance });
            }
        }

        public void Write(byte[] value)
        {
            _data.AddRange(value);
            _size += value.Length;
        }

        public void Write(long value)
        {
            _data.AddRange(BitConverter.GetBytes(value));
            _size += 8;
        }

        public void Write(ulong value)
        {
            _data.AddRange(BitConverter.GetBytes(value));
            _size += 8;
        }

        public void Write(int value)
        {
            _data.AddRange(BitConverter.GetBytes(value));
            _size += 4;
        }

        public void Write(uint value)
        {
            _data.AddRange(BitConverter.GetBytes(value));
            _size += 4;
        }

        public void Write(short value)
        {
            _data.AddRange(BitConverter.GetBytes(value));
            _size += 2;
        }

        public void Write(ushort value)
        {
            _data.AddRange(BitConverter.GetBytes(value));
            _size += 2;
        }

        public void Write(byte value)
        {
            _data.Add(value);
            _size += 1;
        }

        public void Write(sbyte value)
        {
            _data.Add((byte)value);
            _size += 1;
        }

        public void Write(float value)
        {
            _data.AddRange(BitConverter.GetBytes(value));
            _size += 4;
        }

        public void Write(double value)
        {
            _data.AddRange(BitConverter.GetBytes(value));
            _size += 8;
        }

        public void WriteStringSized(string value)
        {
            Write(value.Length);
            Write(value);
        }

        public void Write(string value)
        {
            Write(Encoding.ASCII.GetBytes(value));
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                _data.Clear();
            }

            disposed = true;
        }
    }
}
