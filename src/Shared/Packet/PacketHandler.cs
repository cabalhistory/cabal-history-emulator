﻿using Shared.Telepathy;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Shared.Packet
{
    public class PacketHandler
    {
        struct Packet
        {
            public bool trustedOnly;
            public MethodInfo methodInfo;
            public string procName;
        }

        public string DaemonName { get; private set; }
        public int RegisteredCount => _packets.Count;

        private Dictionary<ushort, Packet> _packets = new Dictionary<ushort, Packet>();

        public PacketHandler(string daemonName)
        {
            DaemonName = daemonName;
        }

        #region Client Execution
        public void Execute(PacketReader reader)
        {
            if (!_packets.ContainsKey(reader.Opcode))
            {
                Logger.Warn($"[{DaemonName} <- IPC] Invalid packet opcode {reader.Opcode}");
                return;
            }

            Logger.Debug($"[{DaemonName} <- IPC] {_packets[reader.Opcode].procName} (OPCODE {reader.Opcode})");

            _packets[reader.Opcode].methodInfo.Invoke(new object { }, new object[] { reader });
        }
        #endregion

        #region Server Execution
        public void Execute(Connection connection, PacketReader reader)
        {
            if (!_packets.ContainsKey(reader.Opcode))
            {
                Logger.Warn($"Invalid packet opcode {reader.Opcode} received from CID {connection.CID} ({connection.FullAddress})");
                return;
            }

            if (_packets[reader.Opcode].trustedOnly && !connection.Trusted)
            {
                connection.Disconnect();
                return;
            }

            Logger.Debug($"[{DaemonName} <- {connection.FullAddress} (CID {connection.CID})] {_packets[reader.Opcode].procName} (OPCODE {reader.Opcode})");

            _packets[reader.Opcode].methodInfo.Invoke(new object { }, new object[] { connection, reader });
        }
        #endregion

        #region Register
        public void Register<TOpcode>(TOpcode opcode, Action<PacketReader> target) where TOpcode : IConvertible
        {
            Register(opcode, target.Method, false);
        }

        public void Register<TOpcode>(TOpcode opcode, Action<Telepathy.Connection, PacketReader> target, bool trustedOnly = true) where TOpcode : IConvertible
        {
            Register(opcode, target.Method, trustedOnly);
        }

        private void Register<TOpcode>(TOpcode opcode, MethodInfo method, bool trustedOnly = true) where TOpcode : IConvertible
        {
            if (Enum.IsDefined(typeof(TOpcode), opcode))
            {
                _packets.Add((ushort)(IConvertible)opcode, new Packet()
                {
                    trustedOnly = trustedOnly,
                    methodInfo = method,
                    procName = opcode.ToString()
                });
            }
        }
        #endregion
    }
}
