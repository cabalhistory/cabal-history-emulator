﻿using Shared;
using System;
using System.Reflection;

namespace Shared.Packet
{
    public class PacketReader : IDisposable
    {
        bool disposed = false;

        int _index;

        byte[] _data;
        int _size;
        int _headersize;

        ushort _magicCode;
        int _payloadLength;
        uint _checkSum;
        ushort _opcode;

        public byte[] Data
        { get { return _data; } }

        public ushort Magic
        { get { return _magicCode; } }

        public int Size
        { get { return _size; } }

        public uint Checksum
        { get { return _checkSum; } }

        public ushort Opcode
        { get { return _opcode; } }

        public int Headersize
        { get { return _headersize; } }

        public int Payloadsize
        { get { return _size - _headersize; } }

        public PacketReader(ref byte[] data, int size, int headerSize = 10)
        {
            _data = data;
            _size = size;
            _headersize = headerSize;

            _magicCode = BitConverter.ToUInt16(_data, 0);
            _payloadLength = BitConverter.ToUInt16(_data, 2);

            switch (headerSize)
            {
                case 10:

                    _checkSum = BitConverter.ToUInt32(_data, 4);
                    _opcode = BitConverter.ToUInt16(_data, 8);

                    break;

                case 6:

                    _payloadLength = BitConverter.ToUInt16(_data, 2);
                    _opcode = BitConverter.ToUInt16(_data, 4);

                    break;

                default:
                    throw new Exception($"Invalid header size");
            }

            Reset();
        }

        public T Packet<T>() where T : new()
        {
            T instance = new T();

            ReadToStruct(__makeref(instance), instance.GetType());

            return instance;
        }

        private void ReadToStruct(TypedReference r, Type type)
        {
            FieldAttribute packetFieldAttribute;
            FieldInfo packetFieldAttributeLengthField;
            int length = 0;

            object fieldInstance;

            // Inheritance
            Type fieldElementType;
            Array fieldElementArray;
            object fieldElementInstanceCopy;

            MethodInfo readerReadMethod;
            Type[] readerMethodParamsType;
            object[] readerMethodParamsValues;
            bool isLengthableCall;

            foreach (FieldInfo fieldInfo in type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                try
                {
                    isLengthableCall = fieldInfo.FieldType.IsArray || (fieldInfo.FieldType == typeof(byte[]) || fieldInfo.FieldType == typeof(string));
                    if (isLengthableCall)
                    {
                        packetFieldAttribute = fieldInfo.GetCustomAttribute<FieldAttribute>();

                        if (packetFieldAttribute == null)
                            throw new Exception($"({type.FullName} {fieldInfo.Name}) must have fixed or variable length attribute");

                        length = packetFieldAttribute.Length;

                        if (!string.IsNullOrEmpty(packetFieldAttribute.LengthFromField))
                        {
                            packetFieldAttributeLengthField = type.GetField(packetFieldAttribute.LengthFromField);
                            if(packetFieldAttributeLengthField == null)
                                throw new Exception($"({type.FullName} {fieldInfo.Name}) not found lengthable field ({packetFieldAttribute.LengthFromField})");

                            length = (int)Convert.ChangeType(packetFieldAttributeLengthField.GetValueDirect(r), typeof(int));
                        }
                    }

                    // Inheritance
                    if ((fieldInfo.FieldType.IsValueType || fieldInfo.FieldType.IsArray) && !fieldInfo.FieldType.IsPrimitive && !fieldInfo.FieldType.IsEnum)
                    {
                        if (isLengthableCall)
                        {
                            fieldElementType = fieldInfo.FieldType.GetElementType();

                            fieldElementArray = Array.CreateInstance(fieldElementType, length);

                            for (int i = 0; i < length; i++)
                            {
                                fieldElementInstanceCopy = fieldElementArray.GetValue(i);

                                ReadToStruct(__makeref(fieldElementInstanceCopy), fieldElementType);

                                fieldElementArray.SetValue(fieldElementInstanceCopy, i);
                            }

                            fieldInstance = fieldElementArray;

                            fieldInfo.SetValueDirect(r, fieldInstance);

                            continue;
                        }

                        fieldInstance = Activator.CreateInstance(fieldInfo.FieldType);
                        ReadToStruct(__makeref(fieldInstance), fieldInfo.FieldType);
                        fieldInfo.SetValueDirect(r, fieldInstance);

                        continue;
                    }

                    readerMethodParamsType = isLengthableCall
                        ? new Type[] { typeof(int), fieldInfo.FieldType.MakeByRefType() }
                        : new Type[] { fieldInfo.FieldType.MakeByRefType() };

                    readerMethodParamsValues = isLengthableCall
                        ? new object[] { length, null }
                        : new object[] { null };

                    readerReadMethod = GetType().GetMethod("Read", BindingFlags.Public | BindingFlags.Instance, null, readerMethodParamsType, null);

                    readerReadMethod.Invoke(this, readerMethodParamsValues);
                    fieldInfo.SetValueDirect(r, isLengthableCall ? readerMethodParamsValues[1] : readerMethodParamsValues[0]);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Cannot read field ({fieldInfo.FieldType.FullName} {fieldInfo.Name}). {ex.Message}");
                }
            }
        }

        #region Read methods
        public void Read(int length, out byte[] result)
        {
            result = new byte[length];

            Array.ConstrainedCopy(_data, _index, result, 0, length);

            _index += length;
        }

        public void Read(out long result)
        {
            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((long*)pd);
                    _index += 8;
                }
            }
        }

        public void Read(out ulong result)
        {
            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((ulong*)pd);
                    _index += 8;
                }
            }
        }

        public void Read(out int result)
        {
            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((int*)pd);
                    _index += 4;
                }
            }
        }

        public void Read(out uint result)
        {
            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((uint*)pd);
                    _index += 4;
                }
            }
        }

        public void Read(out short result)
        {
            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((short*)pd);
                    _index += 2;
                }
            }
        }

        public void Read(out ushort result)
        {
            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((ushort*)pd);
                    _index += 2;
                }
            }
        }

        public void Read(out byte result)
        {
            result = _data[_index];
            _index += 1;
        }

        public void Read(out float result)
        {
            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((float*)pd);
                    _index += 4;
                }
            }
        }

        public void Read(out double result)
        {
            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((double*)pd);
                    _index += 8;
                }
            }
        }

        public void Read(int length, out string result)
        {
            result = System.Text.Encoding.ASCII.GetString(_data, _index, length).Trim(char.MinValue);

            _index += length;
        }
        #endregion

        #region TESTE
        public byte[] ReadBytes(int count)
        {
            var result = new byte[count];

            Array.ConstrainedCopy(_data, _index, result, 0, count);

            _index += count;

            return result;
        }

        public long ReadLong()
        {
            long result;

            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((long*)pd);
                    _index += 8;
                }
            }

            return result;
        }

        public ulong ReadULong()
        {
            ulong result;

            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((ulong*)pd);
                    _index += 8;
                }
            }

            return result;
        }

        public int ReadInt()
        {
            int result;

            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((int*)pd);
                    _index += 4;
                }
            }

            return result;
        }

        public uint ReadUInt()
        {
            uint result;

            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((uint*)pd);
                    _index += 4;
                }
            }

            return result;
        }

        public short ReadShort()
        {
            short result;

            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((short*)pd);
                    _index += 2;
                }
            }

            return result;
        }

        public ushort ReadUShort()
        {
            ushort result;

            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((ushort*)pd);
                    _index += 2;
                }
            }

            return result;
        }

        public byte ReadByte()
        {
            var result = _data[_index];
            _index += 1;

            return result;
        }

        public float ReadFloat()
        {
            float result;

            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((float*)pd);
                    _index += 4;
                }
            }

            return result;
        }

        public double ReadDouble()
        {
            double result;

            unsafe
            {
                fixed (byte* pdata = _data)
                {
                    byte* pd = pdata;
                    pd += _index;

                    result = *((double*)pd);
                    _index += 8;
                }
            }

            return result;
        }

        public string ReadString(int length)
        {
            var result = System.Text.Encoding.ASCII.GetString(_data, _index, length);

            _index += length;

            return result;
        }
        #endregion

        // Reset index method
        public void Reset()
        {
            _index = _headersize;
        }

        #region Dispose
        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Dispose managed resources

                _data = null;
            }

            // Disponse unmanaged resources

        }
        #endregion
    }
}
