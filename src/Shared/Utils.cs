﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection.Emit;
using System.Text;
using System.Threading;

namespace Shared
{
    public static class Utils
    {
        static long initTick = DateTime.Now.Ticks;

        public static int UnixTimestamp => (int)DateTimeOffset.UtcNow.ToUnixTimeSeconds();

        public static long TickCount {
            get {
                return DateTime.Now.Ticks - initTick;
            }
        }

        public static uint TickSec
        {
            get
            {
                return (uint)TimeSpan.FromTicks(TickCount).TotalMilliseconds;
            }
        }

        public static void SetCulture()
        {
            if (Thread.CurrentThread.CurrentCulture.Name != "en-US")
            {
                CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");

                CultureInfo.DefaultThreadCurrentCulture = culture;
                CultureInfo.DefaultThreadCurrentUICulture = culture;

                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;
            }
        }

        public static int ConvertDateTimeToUnixTimestamp(DateTime dateTime)
        {
            return (int)(new DateTimeOffset(dateTime)).ToUnixTimeSeconds();
        }
    }
}
