﻿namespace Shared
{
    public enum Opcode : ushort
    {
        IPC_CONNECT,

        #region ================================ GlobalMgrServer ================================
        IPC_SERVERLIST = 7001,
        IPC_REGISTER_SERVICE = 7002,

        IPC_REQ_LINK = 7003,
        IPC_LINK = 7004,

        IPC_GAME_SUBPASSWORD_SET = 7005,
        IPC_GAME_SUBPASSWORD_GET_QA = 7006,
        IPC_GAME_SUBPASSWORD_ANSWER_QA = 7007,
        IPC_GAME_SUBPASSWORD_AUTH = 7008,
        IPC_GAME_SUBPASSWORD_DEL = 7009,
        #endregion

        #region ================================  GlobalDBAgent  ================================
        IPC_GAME_AUTH_ACCOUNT = 4001,
        IPC_SET_ACCOUNT_STT = 4002,
        #endregion

        #region ================================     DBAgent     ================================
        IPC_REQ_LIST_CHARACTERS = 5001,
        IPC_SET_CHAR_SLOTORDER = 5002,
        #endregion
    }
}
