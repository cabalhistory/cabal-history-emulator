﻿namespace Shared
{
    public static class Typedef
    {
        public const int RSA_KEYLENGTH = 2048;

        #region IAC
        public const int IAC_GLOBALMGRSERVER = 0xCF48D8;
        public const int IAC_GLOBALDBAGENT = 0x049817;
        public const int IAC_DBAGENT = 0x018892;
        #endregion

        #region Account
        public const int MAX_ACCOUNT_USERNAME_LEN = 64;
        public const int MAX_ACCOUNT_PASSWORD_LEN = 64;
        #endregion

        #region Subpassword
        public const int MAX_SUBPASSWORD_REMEMBER_HOURS = 10;
        public const int MAX_SUBPASSWORD_QUESTION_ID = 10;

        public const int MAX_SUBPASSWORD_PASSWORD_LEN = 11;
        public const int MAX_SUBPASSWORD_ANSWER_LEN = 16;

        public enum SubpasswordType : short
        {
            Account = 1,
            Wharehouse,
            Equipment
        }

        public enum SubpasswordSetMode : short
        {
            Create,
            Update
        }

        public enum SubpasswordAuthMode : short
        {
            DeleteSubpassword,
            Update
        }
        #endregion

        #region Account AuthTypes
        public enum AccountAuthType : short
        {
            Inactive,
            Normal,
            Blocked,
            SubpasswordBlocked
        }
        #endregion
    }
}
