﻿using Shared.Packet;

namespace Shared
{
    public static class Protodef
    {
        public const ushort MAGIC_CODE = 0xB7E2;
        public const ushort UNCOMPRESSED_MAGIC_OFFSET = 0x1111;

        public struct C2S_CONNECT2SERV
        {
            public byte ServerIdx;
            public byte GroupIdx;
            public byte Reserved0;
            public byte Reserved1;
        }

        public struct S2C_CONNECT2SERV
        {
            public uint Recv2ndXorSeed;
            public uint AuthKey;
            public ushort Index;
            public ushort RecvXorKeyIdx;
        }

        public struct C2S_VERIFYLINKS
        {
            public uint TargetTick;
            public ushort TargetCID;
            public byte ServerIdx;
            public byte GroupIdx;
            public int NormalClientMagicKey;
        }
        
        public struct S2C_VERIFYLINKS
        {
            public byte GroupIdx;
            public byte ServerIdx;
            public byte Status;
        }

        #region Inter Process Communication

        public struct IPS_CONNECT
        {
            public int IAC;
        }

        #region ================================ GlobalMgrServer ================================

        #region IPC_SERVERLIST
        public struct IPS_SERVERLIST_REQUEST_SERVER
        {
            public byte ServerIdx;
        }

        public struct IPS_SERVERLIST
        {
            public byte ServerCount;

            [Field(LengthFromField = "ServerCount")]
            public _IPS_SERVERLIST_SERVER[] Servers;
        }

        public struct _IPS_SERVERLIST_SERVER
        {
            public byte GroupIdx;
            public int Unk0;
            public short Unk1;
            public byte ChannelCount;

            [Field(LengthFromField = "ChannelCount")]
            public _IPS_SERVERLIST_CHANNEL[] Channels;
        }

        public struct _IPS_SERVERLIST_CHANNEL
        {
            public byte GroupIdx;
            public byte ServerIdx;
            public ushort CurPlayers;

            [Field(Length = 21)]
            public byte[] Unk0;

            public byte Unk1;

            public ushort MaxPlayers;

            [Field(Length = 4)]
            public byte[] IP;
            public ushort Port;

            public long ChannelType;
        }
        #endregion

        #region IPC_REGISTER_SERVICE
        public struct IPS_REGISTER_SERVICE
        {
            public byte GroupIdx;
            public byte ServerIdx;

            [Field(Length = 4)]
            public byte[] IP;
            public ushort Port;

            public ushort MaxConnections;

            public long Type;
        }
        #endregion

        #region IPC_LINK
        public struct IPS_REQ_LINK
        {
            public uint SrcTick;
            public ushort SrcCID;

            public uint DstTick;
            public ushort DstCID;

            public byte GroupIdx;
            public byte ServerIdx;

            public _IPS_LINK_DATA LinkData;
        }

        public struct IPS_LINK
        {
            public byte CallerGroupIdx;
            public byte CallerServerIdx;

            public uint SrcTick;
            public ushort SrcCID;

            public uint DstTick;
            public ushort DstCID;

            public _IPS_LINK_DATA LinkData;
        }

        public struct IPS_LINK_REPLY
        {
            public byte TargetGroupIdx;
            public byte TargetServerIdx;

            public uint DstTick;
            public ushort DstCID;

            public byte GroupIdx;
            public byte ServerIdx;
            public byte Status;
        }

        public struct IPS_REQ_LINK_REPLY
        {
            public uint DstTick;
            public ushort DstCID;

            public byte GroupIdx;
            public byte ServerIdx;
            public byte Status;
        }

        public struct _IPS_LINK_DATA
        {
            public int UserNum;

            [Field(Length = Typedef.MAX_ACCOUNT_USERNAME_LEN)]
            public string ID;

            public int SubpasswordExpiration;

            public int QtdChars;
            public int CharSlotOrder;

            public byte UseACSUB;
            public byte UseWHSUB;
            public byte UseEQSUB;
            public byte IsWHLOCK;
            public byte IsEQLOCK;

            public int ServiceKind;
            public int ExpireDate;
            public int ExtendedCharCreation;
        }
        #endregion

        #region IPC_GAME_SUBPASSWORD_SET
        public struct IPS_GAME_SUBPASSWORD_SET
        {
            public byte GroupIdx;
            public byte ServerIdx;

            public uint SrcTick;
            public ushort SrcCID;

            public int UserNum;

            [Field(Length = 10)]
            public string Password;

            public int Question;

            [Field(Length = 16)]
            public string Answer;

            public int PwType;
            public int Mode;
        }

        public struct IPS_GAME_SUBPASSWORD_SET_REPLY
        {
            public byte GroupIdx;
            public byte ServerIdx;

            public uint DstTick;
            public ushort DstCID;

            public byte Status;
            public int PwType;
            public int Mode;
        }
        #endregion

        #region IPC_GAME_SUBPASSWORD_GET_QA
        public struct IPS_GAME_SUBPASSWORD_GET_QA
        {
            public byte GroupIdx;
            public byte ServerIdx;

            public uint SrcTick;
            public ushort SrcCID;

            public int UserNum;
            public int PwType;
        }

        public struct IPS_GAME_SUBPASSWORD_GET_QA_REPLY
        {
            public byte GroupIdx;
            public byte ServerIdx;

            public uint DstTick;
            public ushort DstCID;

            public int QuestionId;
            public int PwType;
        }
        #endregion

        #region IPC_GAME_SUBPASSWORD_ANSWER_QA
        public struct IPS_GAME_SUBPASSWORD_ANSWER_QA
        {
            public byte GroupIdx;
            public byte ServerIdx;

            public uint SrcTick;
            public ushort SrcCID;

            public int UserNum;
            public int PwType;

            [Field(Length = Typedef.MAX_SUBPASSWORD_ANSWER_LEN)]
            public string Answer;
        }

        public struct IPS_GAME_SUBPASSWORD_ANSWER_QA_REPLY
        {
            public byte GroupIdx;
            public byte ServerIdx;

            public uint DstTick;
            public ushort DstCID;

            public int Status;
            public int PwType;
            public byte TryNum;
        }
        #endregion

        #region IPC_GAME_SUBPASSWORD_AUTH
        public struct IPS_GAME_SUBPASSWORD_AUTH
        {
            public byte GroupIdx;
            public byte ServerIdx;

            public uint SrcTick;
            public ushort SrcCID;

            public int UserNum;
            public int RememberHours;
            public int PwType;

            public byte IsFastAuth;

            [Field(Length = Typedef.MAX_SUBPASSWORD_PASSWORD_LEN)]
            public string Password;
        }

        public struct IPS_GAME_SUBPASSWORD_AUTH_REPLY
        {
            public byte GroupIdx;
            public byte ServerIdx;

            public uint DstTick;
            public ushort DstCID;

            public int Status;
            public int PwType;
            public byte TryNum;
            public int RememberHours;

            public byte IsFastAuth;
        }
        #endregion

        #region IPC_GAME_SUBPASSWORD_DEL
        public struct IPS_GAME_SUBPASSWORD_DEL
        {
            public byte GroupIdx;
            public byte ServerIdx;

            public uint SrcTick;
            public ushort SrcCID;

            public int UserNum;
            public int PwType;
        }

        public struct IPS_GAME_SUBPASSWORD_DEL_REPLY
        {
            public byte GroupIdx;
            public byte ServerIdx;

            public uint DstTick;
            public ushort DstCID;

            public int Status;
            public int PwType;
        }
        #endregion

        #endregion

        #region ================================  GlobalDBAgent  ================================

        #region IPC_GAME_AUTH_ACCOUNT
        public struct IPS_GAME_AUTH_ACCOUNT
        {
            public uint FromLoginTick;
            public ushort FromLoginCID;

            [Field(Length = Typedef.MAX_ACCOUNT_USERNAME_LEN)]
            public string Username;

            [Field(Length = Typedef.MAX_ACCOUNT_PASSWORD_LEN)]
            public string Password;

            [Field(Length = 4)]
            public byte[] IP;
        }

        public struct IPS_GAME_AUTH_ACCOUNT_REPLY
        {
            public uint DstTick;
            public ushort DstCID;

            public byte Status;
            public int AuthStatus;

            public int UserNum;
            public int IDLen;

            [Field(LengthFromField = "IDLen")]
            public string ID;

            [Field(Length = 32)]
            public string Authkey;

            public byte UseACSUB;
            public byte UseWHSUB;
            public byte UseEQSUB;
            public byte IsWHLOCK;
            public byte IsEQLOCK;
            public int ServiceKind;
            public int ExpireDate;
            public int ExtendedCharCreation;
        }
        #endregion

        #region IPC_SET_ACCOUNT_STT
        public struct IPS_SET_ACCOUNT_STT
        {
            public int UserNum;
            public int GroupIdx;
            public int PlayTime;
        }
        #endregion

        #endregion

        #region ================================     DBAgent     ================================

        #region IPC_REQ_LIST_CHARACTERS
        public struct IPS_REQ_LIST_CHARACTERS
        {
            public uint FromWorldTick;
            public ushort FromWorldCID;
            public int UserNum;
        }

        public struct IPS_REQ_LIST_CHARACTERS_REPLY
        {
            public uint ToWorldTick;
            public ushort ToWorldCID;

            public byte QtdChars;
            public int LastCharacter;
            public int SlotOrder;

            [Field(LengthFromField = "QtdChars")]
            public _IPS_REQ_LIST_CHARACTERS_REPLY_CHARACTER[] Characters;
        }

        public struct _IPS_REQ_LIST_CHARACTERS_REPLY_CHARACTER
        {
            public int CharacterIdx;

            [Field(Length = Typedef.MAX_ACCOUNT_USERNAME_LEN)]
            public string Name;

            public int LEV;
            public int Style;
            public int WorldIdx;
            public int PosX;
            public int PosY;
            public int CreateDate;
        }
        #endregion

        #region IPC_SET_CHAR_SLOTORDER
        public struct IPS_SET_CHAR_SLOTORDER
        {
            public uint FromWorldTick;
            public ushort FromWorldCID;

            public int UserNum;
            public int SlotOrder;
        }

        public struct IPS_SET_CHAR_SLOTORDER_REPLY
        {
            public uint ToWorldTick;
            public ushort ToWorldCID;

            public byte Status;
        }
        #endregion

        #endregion

        #endregion
    }
}
