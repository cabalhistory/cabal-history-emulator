﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Shared.Database
{
    public class  MSSQLProcWrapper
    {
        string connectionString;

        public MSSQLProcWrapper(string dataSource, string initialCatalog, string userId, string password, string appname = "Cabal")
        {
            if (string.IsNullOrEmpty(dataSource) || string.IsNullOrEmpty(initialCatalog))
                throw new Exception("Invalid connection string");

            string trusted = "false";
            if (string.IsNullOrEmpty(userId))
                trusted = "true";

            connectionString = $"Data Source={dataSource};" +
                               $"Initial Catalog={initialCatalog};" +
                               $"User ID={userId};" +
                               $"Password={password};" + 
                               $"Application Name={appname};" +
                               $"Trusted_Connection={trusted}";
        }

        public int ExecuteNonQuery(string storedProcedure)
        {
            using (var conn = GetConnection())
            using (var command = GetSqlCommand(storedProcedure, conn))
            {
                conn.Open();

                return command.ExecuteNonQuery();
            }
        }

        public int ExecuteNonQuery(string storedProcedure, Dictionary<string, object> parameters)
        {
            using (var conn = GetConnection())
            using (var command = GetSqlCommand(storedProcedure, conn))
            {
                conn.Open();

                command.Parameters.AddRange(GetParameters(parameters));

                return command.ExecuteNonQuery();
            }
        }

        public object[] Execute(string storedProcedure, bool forceAsMultipleRows = false)
        {
            using (var conn = GetConnection())
            using (var command = GetSqlCommand(storedProcedure, conn))
            using (var dataAdapter = new SqlDataAdapter(command))
            using (var dataTable = new DataTable())
            {
                dataAdapter.Fill(dataTable);

                object[] response;

                if (!forceAsMultipleRows && dataTable.Rows.Count == 1)
                {
                    response = dataTable.Rows[0].ItemArray;
                }
                else
                {
                    response = new object[dataTable.Rows.Count];
                    for (var i = 0; i < dataTable.Rows.Count; i++)
                        response[i] = dataTable.Rows[i].ItemArray;
                }

                return response;
            }
        }

        public object[] Execute(string storedProcedure, Dictionary<string, object> parameters, bool forceAsMultipleRows = false)
        {
            using (var conn = GetConnection())
            using (var command = GetSqlCommand(storedProcedure, conn))
            using (var dataAdapter = new SqlDataAdapter(command))
            using (var dataTable = new DataTable())
            {
                var dataSet = new DataTable();
                object[] response;

                command.Parameters.AddRange(GetParameters(parameters));
                dataAdapter.Fill(dataTable);

                if (!forceAsMultipleRows && dataTable.Rows.Count == 1)
                {
                    response = dataTable.Rows[0].ItemArray;
                } else
                {
                    response = new object[dataTable.Rows.Count];
                    for (var i = 0; i < dataTable.Rows.Count; i++)
                        response[i] = dataTable.Rows[i].ItemArray;
                }

                return response;
            }
        }

        #region Private methods
        private SqlConnection GetConnection()
        {
            return new SqlConnection(connectionString);
        }

        private static SqlCommand GetSqlCommand(string storedProcedureName, SqlConnection conn)
        {
            var command = new SqlCommand(storedProcedureName, conn);
            command.CommandType = CommandType.StoredProcedure;

            return command;
        }

        private static SqlParameter[] GetParameters(Dictionary<string, object> parameters)
        {
            var parameterList = new List<SqlParameter>();

            foreach (var parameter in parameters)
            {
                parameterList.Add(new SqlParameter($"@{parameter.Key}", parameter.Value));
            }

            return parameterList.ToArray();
        }
        #endregion
    }
}