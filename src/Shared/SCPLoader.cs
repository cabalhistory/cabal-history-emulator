﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace Shared
{
    public static class SCPLoader
    {
        public static Dictionary<int, T> ToDictionary<T>(Dictionary<string, Dictionary<string, string>> section) where T : struct
        {
            Dictionary<int, T> dictionary = new Dictionary<int, T>();

            Type entryType = typeof(T);
            Type fieldType;

            foreach (var row in section)
            {
                T entry = new T();

                foreach(FieldInfo field in entryType.GetFields())
                {
                    if(!row.Value.ContainsKey(field.Name))
                        throw new Exception($"({field.Name}) not found in current SCP row");

                    fieldType = field.FieldType;

                    try
                    {
                        field.SetValueDirect(__makeref(entry), GetChangedTypeValue(row.Value[field.Name], fieldType));
                    } catch (Exception ex)
                    {
                        throw new Exception($"Failed to set field value ({fieldType.Name} {field.Name}): {ex.Message}");
                    }
                }

                dictionary.Add(int.Parse(row.Key), entry);
            }

            return dictionary;
        }

        private static object GetChangedTypeValue(string value, Type type)
        {
            if(type == typeof(string))
            {
                return value;
            }

            if (!type.IsPrimitive || !type.IsValueType)
                throw new Exception($"must be a string or primitive value type");

            if (string.IsNullOrEmpty(value))
            {
                return Activator.CreateInstance(type);
            }
           
            if (type == typeof(bool))
            {
                if (value == "1")
                    return true;

                return false;
            }

            return Convert.ChangeType(value, type);
        }
    }
}
