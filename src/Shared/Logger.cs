﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;

#if DEBUG
using Colorful;
using Console = Colorful.Console;
#endif

namespace Shared
{
    public static class Logger
    {
        public static bool IsRunning {
            get
            {
                return _listenThread != null && _listenThread.IsAlive;
            }
        }

        public static string AppName { get; private set; }
        public static string FolderPath { get; private set; }
        public static string FilePath { get; private set; }
        public static LOG_LEVEL LogLevel { get; private set; }

        private static Thread _listenThread;
        private static CancellationTokenSource _listenThreadCancellationTokenSource;
        private static ConcurrentQueue<Message> _messageQueue = new ConcurrentQueue<Message>();
#if DEBUG
        private static Dictionary<LOG_LEVEL, StyleSheet> _stylesheets = new Dictionary<LOG_LEVEL, StyleSheet>();
#endif
        public static void Start(string appName, string fileFolderPath, LOG_LEVEL logLevel)
        {
            AppName = appName;
            FolderPath = fileFolderPath;
            FilePath = String.Format(@"{0}\{1}--{2}.log", fileFolderPath, appName, DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss"));

            LogLevel = logLevel;

            _listenThreadCancellationTokenSource = new CancellationTokenSource();

#if DEBUG
            Console.Title = appName;
            Console.CursorVisible = false;
            CreateStylesheets();
            ShowIntro();
#endif

            _messageQueue.Enqueue(new Message($"Initializing logger service [AppName = {AppName}, LogLevel = {logLevel}]", LOG_LEVEL.INFO));

            _listenThread = new Thread(() => Listen(_listenThreadCancellationTokenSource.Token));
            _listenThread.Name = "Logger Thread";
            _listenThread.Priority = ThreadPriority.Normal;
            _listenThread.Start();
        }

        public static void Stop()
        {
            _listenThreadCancellationTokenSource.Cancel();
        }

#region Public Methods
        public static void Log(string text)
        {
            EnqueueMessage(text, LOG_LEVEL.INFO);
        }

        public static void Error(string text)
        {
            EnqueueMessage(text, LOG_LEVEL.ERROR);
        }

        public static void Warn(string text)
        {
            EnqueueMessage(text, LOG_LEVEL.WARNING);
        }

        public static void Debug(string text)
        {
            EnqueueMessage(text, LOG_LEVEL.DEBUG);
        }
#endregion

#region Private Methods
        private static void Listen(CancellationToken cancellationToken)
        {
            do
            {
                if (_messageQueue.TryDequeue(out Message message))
                {
#if DEBUG
                    WriteToConsole(message);
#endif

                    WriteToStream(message.Text);
                }

                Thread.Sleep(100);
            } while (!cancellationToken.IsCancellationRequested || _messageQueue.Count > 0);
        }

        private static void EnqueueMessage(string text, LOG_LEVEL logLvl)
        {
            if (!IsRunning || _listenThreadCancellationTokenSource.IsCancellationRequested || LogLevel < logLvl)
                return;

            _messageQueue.Enqueue(new Message(text, logLvl));
        }
#if DEBUG
        private static void CreateStylesheets()
        {
            StyleSheet errorStylesheet = new StyleSheet(Color.DarkRed);
            errorStylesheet.AddStyle("##ERROR##", Color.Red);

            StyleSheet warningStylesheet = new StyleSheet(Color.Gold);
            warningStylesheet.AddStyle("WARNING!", Color.Yellow);

            StyleSheet infoStylesheet = new StyleSheet(Color.FloralWhite);

            StyleSheet debugStylesheet = new StyleSheet(Color.DarkCyan);
            debugStylesheet.AddStyle("DEBUG >", Color.Cyan);

            _stylesheets.Add(LOG_LEVEL.ERROR, errorStylesheet);
            _stylesheets.Add(LOG_LEVEL.WARNING, warningStylesheet);
            _stylesheets.Add(LOG_LEVEL.INFO, infoStylesheet);
            _stylesheets.Add(LOG_LEVEL.DEBUG, debugStylesheet);
        }
#endif

#if DEBUG
        private static void ShowIntro()
        {
            List<Color> allColors = new List<Color>();

            foreach (PropertyInfo property in typeof(Color).GetProperties())
            {
                if (property.PropertyType == typeof(Color))
                {
                    allColors.Add((Color)property.GetValue(null));
                }
            }

            Random rnd = new Random();
            Color selected = allColors[rnd.Next(allColors.Count)];

            Console.WriteAscii(AppName, selected);
        }
#endif

#if DEBUG
        private static void WriteToConsole(Message message)
        {
            _stylesheets.TryGetValue(message.LogLevel, out StyleSheet styleSheet);

            if (styleSheet != null)
            {
                Console.WriteLineStyled(message.Text, styleSheet);
            }
        }
#endif

        private static void WriteToStream(string text)
        {
            if (!Directory.Exists(FolderPath))
                Directory.CreateDirectory(FolderPath);

            StreamWriter stream = File.Exists(FilePath) ? File.AppendText(FilePath) : new StreamWriter(FilePath);
            stream.WriteLine(text);
            stream.Close();
            stream.Dispose();
        }
#endregion

#region Other
        class Message
        {
            public string Text { get; set; }
            public LOG_LEVEL LogLevel { get; set; }

            public Message(string text, LOG_LEVEL log_level)
            {
                LogLevel = log_level;

                Text = $"[{ DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") }] ";

                switch (log_level)
                {
                    case LOG_LEVEL.ERROR:
                        Text += "##ERROR## ";
                        break;

                    case LOG_LEVEL.WARNING:
                        Text += "WARNING! ";
                        break;

                    case LOG_LEVEL.DEBUG:
                        Text += "DEBUG > ";
                        break;
                }

                Text += text;
            }
        }

        public enum LOG_LEVEL
        {
            ERROR = 1,
            WARNING = 2,
            INFO = 3,
            DEBUG = 4
        }
#endregion
    }
}
