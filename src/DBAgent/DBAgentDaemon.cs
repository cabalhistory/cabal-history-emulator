﻿using DBAgent.Daemons.DBAgent;
using Shared.Packet;
using Shared.Telepathy;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DBAgent
{
    class DBAgentDaemon : ServerDaemon
    {
        public DBAgentDaemon(IPAddress address, ushort port, ushort threads, ushort frequency)
            : base(address, port, threads, frequency)
        {
            server.UseEncryption = false;
            server.HeaderSize = 6;

            Procs.Register(_packetsHandler);
        }

        protected override void OnConnect(Connection connection)
        {
            base.OnConnect(connection);
        }

        protected override void OnReceiveData(Connection connection, PacketReader reader)
        {
            base.OnReceiveData(connection, reader);

            _packetsHandler.Execute(connection, reader);
        }

        protected override void OnDisconnect(Connection connection)
        {
            base.OnDisconnect(connection);
        }
    }
}
