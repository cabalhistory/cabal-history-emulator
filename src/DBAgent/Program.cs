﻿using Shared;
using Shared.Database;
using System;

namespace DBAgent
{
    class Program
    {
        public static DBAgentDaemon DBAgentDaemon { get; private set; }

        public static MSSQLProcWrapper DBGameWrapper { get; private set; }

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledException.HandleUnhandledException;

            // Load .ini configuration
            Config.Load(args[1]);

            // Start logger service
            Logger.Start(args[0], Config.LogPath, Config.LogLevel);

            DBGameWrapper = new MSSQLProcWrapper(Config.DBGame_DataSource, Config.DBGame_InitialCatalog, Config.DBGame_UserId, Config.DBGame_Password, args[0]);

            #region Daemons
            DBAgentDaemon = new DBAgentDaemon(Config.IP, Config.Port, Config.Threads, Config.Frequency);

            DaemonManager.Register(DBAgentDaemon);
            #endregion

            DaemonManager.StartAll();
        }
    }
}
