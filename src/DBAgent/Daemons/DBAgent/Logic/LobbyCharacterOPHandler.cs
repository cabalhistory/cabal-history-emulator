﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBAgent.Daemons.DBAgent.Logic
{
    static class LobbyCharacterOPHandler
    {
        public static object[] GetCharacters(Dictionary<string, object> parameters)
            => Program.DBGameWrapper.Execute("h_sp_get_characters", parameters, true);

        public static object[] GetLastLoginCharacter(Dictionary<string, object> parameters)
            => Program.DBGameWrapper.Execute("h_sp_last_login_character", parameters);

        public static int SetLastLoginCharacter(Dictionary<string, object> parameters)
            => Program.DBGameWrapper.ExecuteNonQuery("h_sp_last_login_character", parameters);
    }
}
