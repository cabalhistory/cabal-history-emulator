﻿using System;
using Shared.Packet;
using Shared.Telepathy;
using System.Collections.Generic;
using static Shared.Protodef;
using DBAgent.Daemons.DBAgent.Logic;

namespace DBAgent.Daemons.DBAgent.Proc
{
    static class LobbyCharacters
    {
        public static void OnIPC_REQ_LIST_CHARACTERS(Connection connection, PacketReader reader)
        {
            var ipsReqListCharacters = reader.Packet<IPS_REQ_LIST_CHARACTERS>();

            var responseGetCharacters = LobbyCharacterOPHandler.GetCharacters(new Dictionary<string, object>()
            {
                { "usernum", ipsReqListCharacters.UserNum }
            });

            var responseLastLoginCharacter = LobbyCharacterOPHandler.GetLastLoginCharacter(new Dictionary<string, object>()
            {
                { "mode", 2 },
                { "index", ipsReqListCharacters.UserNum },
                { "value", 0 }
            });

            var ipsReqListCharactersReply = new IPS_REQ_LIST_CHARACTERS_REPLY();
            ipsReqListCharactersReply.ToWorldTick = ipsReqListCharacters.FromWorldTick;
            ipsReqListCharactersReply.ToWorldCID = ipsReqListCharacters.FromWorldCID;
            ipsReqListCharactersReply.QtdChars = (byte)responseGetCharacters.Length;
            ipsReqListCharactersReply.LastCharacter = (int)responseLastLoginCharacter[0];
            ipsReqListCharactersReply.SlotOrder = (int)responseLastLoginCharacter[1];

            var ipsReqListCharactersReplyCharacters = new _IPS_REQ_LIST_CHARACTERS_REPLY_CHARACTER[responseGetCharacters.Length];
            for(int i = 0; i < responseGetCharacters.Length; i++)
            {
                ipsReqListCharactersReplyCharacters[i].CharacterIdx = (int)((object[])responseGetCharacters[i])[0];
                ipsReqListCharactersReplyCharacters[i].Name = ((object[])responseGetCharacters[i])[1].ToString();
                ipsReqListCharactersReplyCharacters[i].LEV = (int)((object[])responseGetCharacters[i])[2];
                ipsReqListCharactersReplyCharacters[i].Style = (int)((object[])responseGetCharacters[i])[3];
                ipsReqListCharactersReplyCharacters[i].WorldIdx = (int)((object[])responseGetCharacters[i])[4];
                ipsReqListCharactersReplyCharacters[i].PosX = (int)((object[])responseGetCharacters[i])[5];
                ipsReqListCharactersReplyCharacters[i].PosY = (int)((object[])responseGetCharacters[i])[6];
                ipsReqListCharactersReplyCharacters[i].CreateDate = (int)((object[])responseGetCharacters[i])[7];
            }

            ipsReqListCharactersReply.Characters = ipsReqListCharactersReplyCharacters;

            connection.Send(Shared.Opcode.IPC_REQ_LIST_CHARACTERS, ipsReqListCharactersReply);
        }

        public static void OnIPC_SET_CHAR_SLOTORDER(Connection connection, PacketReader reader)
        {
            var ipsSetCharSlotOrder = reader.Packet<IPS_SET_CHAR_SLOTORDER>();

            int rowsAffected = LobbyCharacterOPHandler.SetLastLoginCharacter(new Dictionary<string, object>()
            {
                { "mode", 1 },
                { "index", ipsSetCharSlotOrder.UserNum },
                { "value", ipsSetCharSlotOrder.SlotOrder }
            });

            var ipsSetCharSLotOrderReply = new IPS_SET_CHAR_SLOTORDER_REPLY();
            ipsSetCharSLotOrderReply.ToWorldTick = ipsSetCharSlotOrder.FromWorldTick;
            ipsSetCharSLotOrderReply.ToWorldCID = ipsSetCharSlotOrder.FromWorldCID;
            ipsSetCharSLotOrderReply.Status = Convert.ToByte(rowsAffected == 1);

            connection.Send(Shared.Opcode.IPC_SET_CHAR_SLOTORDER, ipsSetCharSLotOrderReply);
        }
    }
}
