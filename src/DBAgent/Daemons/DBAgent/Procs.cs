﻿using DBAgent.Daemons.DBAgent.Proc;
using Shared.Packet;

namespace DBAgent.Daemons.DBAgent
{
    static class Procs
    {
        public static void Register(PacketHandler packetsHandler)
        {
            packetsHandler.Register(Shared.Opcode.IPC_CONNECT, IPCConnection.OnIPC_CONNECT, false);

            packetsHandler.Register(Shared.Opcode.IPC_REQ_LIST_CHARACTERS, LobbyCharacters.OnIPC_REQ_LIST_CHARACTERS);
            packetsHandler.Register(Shared.Opcode.IPC_SET_CHAR_SLOTORDER, LobbyCharacters.OnIPC_SET_CHAR_SLOTORDER);
        }
    }
}
