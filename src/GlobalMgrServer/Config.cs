﻿using System;
using System.Net;
using Shared;

namespace GlobalMgrServer
{
    class Config
    {
        public static IPAddress IP { get; set; }
        public static ushort Port { get; set; }

        public static Logger.LOG_LEVEL LogLevel { get; set; }
        public static string LogPath { get; set; }
        public static ushort Threads { get; set; }
        public static ushort Frequency { get; set; }

        public static IPAddress GDAIP { get; set; }
        public static ushort GDAPort { get; set; }
        public static int GDAConnectionTimeOut { get; set; }

        public static void Load(string iniFileName)
        {
            IniReader parser = new IniReader(iniFileName);

            const string LISTEN_SECTION = "Listen";
            IP = IPAddress.Parse(parser.GetValue(LISTEN_SECTION, "IP"));
            Port = ushort.Parse(parser.GetValue(LISTEN_SECTION, "Port"));

            const string SERVER_SECTION = "Server";
            Enum.TryParse(parser.GetValue(SERVER_SECTION, "LogLevel"), out Logger.LOG_LEVEL _logLevel);

            LogLevel = _logLevel;
            LogPath = parser.GetValue(SERVER_SECTION, "LogPath");
            Threads = ushort.Parse(parser.GetValue(SERVER_SECTION, "Threads"));
            Frequency = ushort.Parse(parser.GetValue(SERVER_SECTION, "Frequency"));

            const string GLOBALDBASERVER_SECTION = "GlobalDBAgent";
            GDAIP = IPAddress.Parse(parser.GetValue(GLOBALDBASERVER_SECTION, "GDAIP"));
            GDAPort = ushort.Parse(parser.GetValue(GLOBALDBASERVER_SECTION, "GDAPort"));
            GDAConnectionTimeOut = int.Parse(parser.GetValue(GLOBALDBASERVER_SECTION, "GDAConnectionTimeOut"));
        }
    }
}
