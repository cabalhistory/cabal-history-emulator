﻿using GlobalMgrServer.Context;
using Shared.Telepathy;
using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalMgrServer.Extensions
{
    static class ConnectionExtensions
    {
        const string CTX_SERVICE_KEY = "CTX_SERVICE";

        public static bool InitServiceContext(this Connection connection, out ServiceContext serviceContext)
        {
            serviceContext = new ServiceContext(connection);
            return connection.Metadata.TryAdd(CTX_SERVICE_KEY, serviceContext);
        }

        public static bool GetServiceContext(this Connection connection, out ServiceContext channelContext)
        {
            channelContext = null;

            bool tryGet = connection.Metadata.TryGetValue(CTX_SERVICE_KEY, out object channelContextObj);

            if (tryGet)
                channelContext = (ServiceContext)channelContextObj;

            return tryGet;
        }
    }
}
