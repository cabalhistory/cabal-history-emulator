﻿using Shared.Packet;
using GlobalMgrServer.Daemons.GlobalMgrServer.Proc;

namespace GlobalMgrServer.Daemons.GlobalMgrServer
{
    static class Procs
    {
        public static void Register(PacketHandler packetsHandler)
        {
            packetsHandler.Register(Shared.Opcode.IPC_CONNECT, IPCConnection.OnIPC_CONNECT, false);

            packetsHandler.Register(Shared.Opcode.IPC_SERVERLIST, GameServices.OnIPC_SERVERLIST);
            packetsHandler.Register(Shared.Opcode.IPC_REGISTER_SERVICE, GameServices.OnIPC_REGISTER_SERVICE);

            packetsHandler.Register(Shared.Opcode.IPC_REQ_LINK, Link.OnIPC_REQ_LINK);
            packetsHandler.Register(Shared.Opcode.IPC_LINK, Link.OnIPC_LINK);

            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_SET, Proxy.SendToGlobalDBAgent);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_GET_QA, Proxy.SendToGlobalDBAgent);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_ANSWER_QA, Proxy.SendToGlobalDBAgent);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_AUTH, Proxy.SendToGlobalDBAgent);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_DEL, Proxy.SendToGlobalDBAgent);
        }
    }
}
