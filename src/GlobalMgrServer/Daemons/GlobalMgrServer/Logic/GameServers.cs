﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using GlobalMgrServer.Context;

namespace GlobalMgrServer.Daemons.GlobalMgrServer.Logic
{
    static class GameServers
    {
        public const int LOGINSERVER_GROUPIDX = 128;

        static ConcurrentDictionary<byte, GameServer> _servers = new ConcurrentDictionary<byte, GameServer>();

        public static IEnumerable<GameServer> GetServers()
        {
            return _servers.Values.Where(_ => _.GroupIdx != LOGINSERVER_GROUPIDX);
        }

        public static bool RegisterServer(byte groupIdx)
        {
            return _servers.TryAdd(groupIdx, new GameServer(groupIdx));
        }

        public static bool GetService(byte groupIdx, byte serverIdx, out ServiceContext serviceContext)
        {
            serviceContext = null;

            if (!_servers.ContainsKey(groupIdx))
                return false;

            return _servers[groupIdx].Services.TryGetValue(serverIdx, out serviceContext);
        }

        public static bool RegisterService(ServiceContext serviceContext)
        {
            if (!_servers.ContainsKey(serviceContext.GroupIdx))
            {
                if (!RegisterServer(serviceContext.GroupIdx))
                    return false;
            }

            return _servers[serviceContext.GroupIdx].Services.TryAdd(serviceContext.ServerIdx, serviceContext);
        }

        public static void RemoveService(byte groupIdx, byte serverIdx)
        {
            if (_servers.ContainsKey(groupIdx))
            {
                if (_servers[groupIdx].Services.ContainsKey(serverIdx))
                {
                    ((IDictionary<byte, ServiceContext>)_servers[groupIdx].Services).Remove(serverIdx);
                }
            }
        }
    }

    internal class GameServer
    {
        public byte GroupIdx;
        public ConcurrentDictionary<byte, ServiceContext> Services;

        public GameServer(byte groupIdx)
        {
            GroupIdx = groupIdx;

            Services = new ConcurrentDictionary<byte, ServiceContext>();
        }
    }
}
