﻿using Shared;
using Shared.Packet;
using Shared.Telepathy;
using static Shared.Protodef;

namespace GlobalMgrServer.Daemons.GlobalMgrServer.Proc
{
    static class IPCConnection
    {
        public static void OnIPC_CONNECT(Connection connection, PacketReader reader)
        {
            int pCode = reader.ReadInt();

            if (pCode != Typedef.IAC_GLOBALMGRSERVER)
            {
                connection.Disconnect($"Invalid IAC (c:{pCode}, s:{Typedef.IAC_GLOBALMGRSERVER})");
                return;
            }

            connection.Trusted = true;

            connection.Send(Shared.Opcode.IPC_CONNECT);
        }
    }
}
