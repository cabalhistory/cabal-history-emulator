﻿using GlobalMgrServer.Context;
using GlobalMgrServer.Daemons.GlobalMgrServer.Logic;
using GlobalMgrServer.Extensions;
using Shared.Packet;
using Shared.Telepathy;
using static Shared.Protodef;

namespace GlobalMgrServer.Daemons.GlobalMgrServer.Proc
{
    static class Link
    {
        public static void OnIPC_REQ_LINK(Connection connection, PacketReader reader)
        {
            if (!connection.GetServiceContext(out ServiceContext callerServiceContext))
                return;

            var ipsReqLink = reader.Packet<IPS_REQ_LINK>();

            if (!GameServers.GetService(ipsReqLink.GroupIdx, ipsReqLink.ServerIdx, out ServiceContext targetServiceContext))
                return;

            var ipsLink = new IPS_LINK();
            ipsLink.CallerGroupIdx = callerServiceContext.GroupIdx;
            ipsLink.CallerServerIdx = callerServiceContext.ServerIdx;

            ipsLink.SrcTick = ipsReqLink.SrcTick;
            ipsLink.SrcCID = ipsReqLink.SrcCID;

            ipsLink.DstTick = ipsReqLink.DstTick;
            ipsLink.DstCID = ipsReqLink.DstCID;

            ipsLink.LinkData = new _IPS_LINK_DATA();
            ipsLink.LinkData.UserNum = ipsReqLink.LinkData.UserNum;
            ipsLink.LinkData.ID = ipsReqLink.LinkData.ID;

            ipsLink.LinkData.SubpasswordExpiration = ipsReqLink.LinkData.SubpasswordExpiration;

            ipsLink.LinkData.QtdChars = ipsReqLink.LinkData.QtdChars;
            ipsLink.LinkData.CharSlotOrder = ipsReqLink.LinkData.CharSlotOrder;

            ipsLink.LinkData.UseACSUB = ipsReqLink.LinkData.UseACSUB;
            ipsLink.LinkData.UseWHSUB = ipsReqLink.LinkData.UseWHSUB;
            ipsLink.LinkData.UseEQSUB = ipsReqLink.LinkData.UseEQSUB;
            ipsLink.LinkData.IsWHLOCK = ipsReqLink.LinkData.IsWHLOCK;
            ipsLink.LinkData.IsEQLOCK = ipsReqLink.LinkData.IsEQLOCK;

            ipsLink.LinkData.ServiceKind = ipsReqLink.LinkData.ServiceKind;
            ipsLink.LinkData.ExpireDate = ipsReqLink.LinkData.ExpireDate;
            ipsLink.LinkData.ExtendedCharCreation = ipsReqLink.LinkData.ExtendedCharCreation;

            targetServiceContext.Connection.Send(Shared.Opcode.IPC_LINK, ipsLink);
        }
        
        public static void OnIPC_LINK(Connection connection, PacketReader reader)
        {
            var ipsLinkReply = reader.Packet<IPS_LINK_REPLY>();

            if (!GameServers.GetService(ipsLinkReply.TargetGroupIdx, ipsLinkReply.TargetServerIdx, out ServiceContext serviceContext))
                return;
            
            var ipsReqLinkReply = new IPS_REQ_LINK_REPLY();

            ipsReqLinkReply.DstTick = ipsLinkReply.DstTick;
            ipsReqLinkReply.DstCID = ipsLinkReply.DstCID;
            ipsReqLinkReply.ServerIdx = ipsLinkReply.ServerIdx;
            ipsReqLinkReply.GroupIdx = ipsLinkReply.GroupIdx;
            ipsReqLinkReply.Status = ipsLinkReply.Status;

            serviceContext.Connection.Send(Shared.Opcode.IPC_REQ_LINK, ipsReqLinkReply);
        }
    }
}
