﻿using Shared;
using Shared.Packet;
using Shared.Telepathy;
using System.Net;
using System.Linq;
using GlobalMgrServer.Context;
using GlobalMgrServer.Daemons.GlobalMgrServer.Logic;
using GlobalMgrServer.Extensions;
using static Shared.Protodef;

namespace GlobalMgrServer.Daemons.GlobalMgrServer.Proc
{
    static class GameServices
    {
        public static void OnIPC_SERVERLIST(Connection connection, PacketReader reader)
        {
            var gameServers = GameServers.GetServers();
            int gameServersCount = (byte)gameServers.Count();

            var ipsServerList = new IPS_SERVERLIST();
            ipsServerList.ServerCount = (byte)gameServersCount;

            var ipsServerListServers = new _IPS_SERVERLIST_SERVER[gameServersCount];

            int i = 0, j = 0;
            foreach(var server in gameServers)
            {
                var serverChannels = server.Services.Values;
                int channelsCount = serverChannels.Count();

                ipsServerListServers[i].GroupIdx = server.GroupIdx;
                ipsServerListServers[i].Unk0 = 0;
                ipsServerListServers[i].Unk1 = 0;
                ipsServerListServers[i].ChannelCount = (byte)channelsCount;

                ipsServerListServers[i].Channels = new _IPS_SERVERLIST_CHANNEL[channelsCount];

                j = 0;
                foreach (var channel in serverChannels)
                {
                    ipsServerListServers[i].Channels[j].GroupIdx = channel.GroupIdx;
                    ipsServerListServers[i].Channels[j].ServerIdx = channel.ServerIdx;
                    ipsServerListServers[i].Channels[j].CurPlayers = channel.CurConnections;
                    ipsServerListServers[i].Channels[j].Unk0 = new byte[21];
                    ipsServerListServers[i].Channels[j].Unk1 = 0xFF;
                    ipsServerListServers[i].Channels[j].MaxPlayers = channel.MaxConnections;
                    ipsServerListServers[i].Channels[j].IP = channel.IPAddress.GetAddressBytes();
                    ipsServerListServers[i].Channels[j].Port = channel.Port;
                    ipsServerListServers[i].Channels[j].ChannelType = channel.Type;

                    j++;
                }

                i++;
            }

            ipsServerList.Servers = ipsServerListServers;

            connection.Send(Shared.Opcode.IPC_SERVERLIST, ipsServerList);
        }

        public static void OnIPC_REGISTER_SERVICE(Connection connection, PacketReader reader)
        {
            var ipsRegisterService = reader.Packet<IPS_REGISTER_SERVICE>();

            if(!connection.InitServiceContext(out ServiceContext serviceContext))
            {
                Logger.Error($"Can't init service context (GroupIdx: {ipsRegisterService.GroupIdx}, ServerIdx: {ipsRegisterService.ServerIdx})");
                connection.Disconnect();
                return;
            }

            serviceContext.GroupIdx = ipsRegisterService.GroupIdx;
            serviceContext.ServerIdx = ipsRegisterService.ServerIdx;

            serviceContext.IPAddress = new IPAddress(ipsRegisterService.IP);
            serviceContext.Port = ipsRegisterService.Port;

            serviceContext.MaxConnections = ipsRegisterService.MaxConnections;

            serviceContext.Type = ipsRegisterService.Type;

            if (!GameServers.RegisterService(serviceContext))
            {
                Logger.Error($"Can't register service (GroupIdx: {ipsRegisterService.GroupIdx}, ServerIdx: {ipsRegisterService.ServerIdx})");
                connection.Disconnect();
            }
        }
    }
}
