﻿using Shared.Packet;
using Shared.Telepathy;

namespace GlobalMgrServer.Daemons.GlobalMgrServer.Proc
{
    static class Proxy
    {

        public static void SendToGlobalDBAgent(Connection connection, PacketReader reader)
            => Program.GlobalDBAgentClientDaemon.Send(reader.Data);

    }
}
