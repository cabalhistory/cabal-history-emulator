﻿using GlobalMgrServer.Context;
using GlobalMgrServer.Daemons.GlobalMgrServer.Logic;
using Shared;
using Shared.Packet;

namespace GlobalMgrServer.Daemons.GlobalDBAgentClient.Proc
{
    static class Proxy
    {
        public static void SendToWorldServer(PacketReader reader)
        {
            reader.Read(out byte groupIdx);
            reader.Read(out byte serverIdx);

            if(!GameServers.GetService(groupIdx, serverIdx, out ServiceContext serviceContext))
            {
                Logger.Error($"Can't proxy packet ({reader.Opcode}) to WorldServer: Service not found (GroupIdx: {groupIdx}, ServerIdx: {serverIdx}). Are you sure that packet has first 2 bytes with worldserver information?");
                return;
            }

            serviceContext.Connection.Send(reader.Data);
        }
    }
}
