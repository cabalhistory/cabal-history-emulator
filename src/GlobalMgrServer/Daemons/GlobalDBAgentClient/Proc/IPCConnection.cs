﻿using Shared.Packet;
using Shared;

namespace GlobalMgrServer.Daemons.GlobalDBAgentClient.Proc
{
    static class IPCConnection
    {
        public static void OnIPC_CONNECT(PacketReader reader)
        {
            Logger.Log("GlobalDBAgentClient IAC authenticated");
        }
    }
}
