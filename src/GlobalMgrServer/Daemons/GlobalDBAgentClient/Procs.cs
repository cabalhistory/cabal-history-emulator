﻿using Shared.Packet;
using GlobalMgrServer.Daemons.GlobalDBAgentClient.Proc;

namespace GlobalMgrServer.Daemons.GlobalDBAgentClient
{
    static class Procs
    {
        public static void Register(PacketHandler packetsHandler)
        {
            packetsHandler.Register(Shared.Opcode.IPC_CONNECT, IPCConnection.OnIPC_CONNECT);

            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_SET, Proxy.SendToWorldServer);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_GET_QA, Proxy.SendToWorldServer);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_ANSWER_QA, Proxy.SendToWorldServer);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_AUTH, Proxy.SendToWorldServer);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_DEL, Proxy.SendToWorldServer);
        }
    }
}
