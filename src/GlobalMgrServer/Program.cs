﻿using System;
using System.IO;
using Shared;
using GlobalMgrServer.Daemons;

namespace GlobalMgrServer
{
    class Program
    {
        public static GlobalMgrServerDaemon GlobalMgrServer { get; private set; }
        public static GlobalDBAgentClientDaemon GlobalDBAgentClientDaemon { get; private set; }

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledException.HandleUnhandledException;

            // Load .ini configuration
            Config.Load($"GlobalMgrServer.ini");

            // Start logger service
            Logger.Start("GlobalMgrServer", Config.LogPath, Config.LogLevel);

            #region Daemons
            GlobalMgrServer = new GlobalMgrServerDaemon(Config.IP, Config.Port, Config.Threads, Config.Frequency);
            GlobalDBAgentClientDaemon = new GlobalDBAgentClientDaemon(Config.GDAIP, Config.GDAPort, Config.GDAConnectionTimeOut);

            DaemonManager.Register(GlobalDBAgentClientDaemon);
            DaemonManager.Register(GlobalMgrServer);
            #endregion

            DaemonManager.StartAll();
        }
    }
}
