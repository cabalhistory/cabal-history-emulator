﻿using Shared;
using System.Net;
using Shared.Packet;
using Shared.Telepathy;
using GlobalMgrServer.Daemons.GlobalDBAgentClient;
using static Shared.Protodef;


namespace GlobalMgrServer
{
    class GlobalDBAgentClientDaemon : ClientDaemon
    {
        public GlobalDBAgentClientDaemon(IPAddress sv_addr, ushort sv_port, int connection_timeout) :
            base(sv_addr, sv_port, connection_timeout)
        {
            Procs.Register(_packetsHandler);
        }

        protected override void OnConnect()
        {
            base.OnConnect();

            IPS_CONNECT ipsConnect = new IPS_CONNECT();
            ipsConnect.IAC = Typedef.IAC_GLOBALDBAGENT;

            Send(Shared.Opcode.IPC_CONNECT, ipsConnect);
        }

        protected override void OnReceiveData(PacketReader reader)
        {
            base.OnReceiveData(reader);

            _packetsHandler.Execute(reader);
        }

        protected override void OnDisconnect()
        {
            base.OnDisconnect();
        }
    }
}
