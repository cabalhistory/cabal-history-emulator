﻿using System.Net;
using Shared.Telepathy;
using GlobalMgrServer.Daemons.GlobalMgrServer;
using Shared.Packet;
using GlobalMgrServer.Extensions;
using GlobalMgrServer.Context;
using GlobalMgrServer.Daemons.GlobalMgrServer.Logic;

namespace GlobalMgrServer
{
    class GlobalMgrServerDaemon : ServerDaemon
    {
        public GlobalMgrServerDaemon(IPAddress sv_addr, ushort sv_port, ushort sv_threads, ushort sv_frequency)
            : base(sv_addr, sv_port, sv_threads, sv_frequency)
        {
            // Execute before server starts

            // Server Configuration
            server.HeaderSize = 6;
            server.UseEncryption = false;

            // Register Packets
            Procs.Register(_packetsHandler);
        }

        protected override void OnConnect(Connection connection)
        {
            base.OnConnect(connection);
        }

        protected override void OnReceiveData(Connection connection, PacketReader reader)
        {
            base.OnReceiveData(connection, reader);

            // Execute logic
            _packetsHandler.Execute(connection, reader);
        }

        protected override void OnDisconnect(Connection connection)
        {
            base.OnDisconnect(connection);

            if(connection.GetServiceContext(out ServiceContext channelContext))
            {
                GameServers.RemoveService(channelContext.GroupIdx, channelContext.ServerIdx);
            }
        }
    }
}
