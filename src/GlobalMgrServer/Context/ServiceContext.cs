﻿using Shared;
using Shared.Telepathy;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace GlobalMgrServer.Context
{
    class ServiceContext
    {
        public readonly Connection Connection;

        public IPAddress IPAddress { get; set; }
        public ushort Port { get; set; }

        public byte GroupIdx { get; set; }
        public byte ServerIdx { get; set; }

        public ushort CurConnections { get; set; }
        public ushort MaxConnections { get; set; }
       
        public long Type { get; set; }

        private readonly long _initTick;
        public double Uptime
        {
            get
            {
                return TimeSpan.FromTicks(Utils.TickCount - _initTick).TotalSeconds;
            }
        }

        public ServiceContext(Connection connection)
        {
            Connection = connection;

            _initTick = Utils.TickCount;
        }
    }
}
