﻿using Shared.Packet;
using GlobalDBAgent.Daemons.GlobalDBAgent.Proc;

namespace GlobalDBAgent.Daemons.GlobalDBAgent
{
    class Procs
    {
        public static void Register(PacketHandler packetsHandler)
        {
            packetsHandler.Register(Shared.Opcode.IPC_CONNECT, IPCConnection.OnIPC_CONNECT, false);

            packetsHandler.Register(Shared.Opcode.IPC_GAME_AUTH_ACCOUNT, GameAuth.OnIPC_GAME_AUTH_ACCOUNT);
            packetsHandler.Register(Shared.Opcode.IPC_SET_ACCOUNT_STT, GameAuth.OnIPC_SET_ACCOUNT_STT);

            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_SET, GameSubpassword.OnIPC_GAME_SUBPASSWORD_SET);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_GET_QA, GameSubpassword.OnIPC_GAME_SUBPASSWORD_GET_QA);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_ANSWER_QA, GameSubpassword.OnIPC_GAME_SUBPASSWORD_ANSWER_QA);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_AUTH, GameSubpassword.OnIPC_GAME_SUBPASSWORD_AUTH);
            packetsHandler.Register(Shared.Opcode.IPC_GAME_SUBPASSWORD_DEL, GameSubpassword.OnIPC_GAME_SUBPASSWORD_DEL);
        }
    }
}
