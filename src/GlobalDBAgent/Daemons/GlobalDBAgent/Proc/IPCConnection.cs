﻿using Shared;
using Shared.Packet;
using Shared.Telepathy;
using static Shared.Protodef;

namespace GlobalDBAgent.Daemons.GlobalDBAgent.Proc
{
    static class IPCConnection
    {
        public static void OnIPC_CONNECT(Connection connection, PacketReader reader)
        {
            var ipsConnect = reader.Packet<IPS_CONNECT>();

            if (ipsConnect.IAC != Typedef.IAC_GLOBALDBAGENT)
            {
                Logger.Error($"Invalid IAC (c: {ipsConnect.IAC}, s: {Typedef.IAC_GLOBALDBAGENT})");
                connection.Disconnect();
            }

            // Set connection as trusted
            connection.Trusted = true;

            connection.Send(Shared.Opcode.IPC_CONNECT);
        }
    }
}
