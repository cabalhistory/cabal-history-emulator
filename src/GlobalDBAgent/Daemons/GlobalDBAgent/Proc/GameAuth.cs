﻿using GlobalDBAgent.Daemons.GlobalDBAgent.Logic;
using Shared.Packet;
using Shared.Telepathy;
using System.Collections.Generic;
using System.Net;
using static Shared.Protodef;

namespace GlobalDBAgent.Daemons.GlobalDBAgent.Proc
{
    static class GameAuth
    {
        public static void OnIPC_GAME_AUTH_ACCOUNT(Connection connection, PacketReader reader)
        {
            var ipsGameAuthAccount = reader.Packet<IPS_GAME_AUTH_ACCOUNT>();

            IPAddress address = new IPAddress(ipsGameAuthAccount.IP);

            var response = AccountOPHandler.AuthAccount(new Dictionary<string, object>()
            {
                { "username", ipsGameAuthAccount.Username },
                { "password", ipsGameAuthAccount.Password },
                { "ip", address.ToString() }
            });

            var ipsReply = new IPS_GAME_AUTH_ACCOUNT_REPLY();
            ipsReply.DstTick = ipsGameAuthAccount.FromLoginTick;
            ipsReply.DstCID = ipsGameAuthAccount.FromLoginCID;
            
            ipsReply.AuthStatus = (int)response[0];
            
            if (ipsReply.AuthStatus == 0x20)
            {
                ipsReply.Status = 1;

                ipsReply.UserNum = (int)response[1];

                ipsReply.IDLen = response[2].ToString().Length;
                ipsReply.ID = response[2].ToString();
                
                ipsReply.Authkey = response[3].ToString();

                ipsReply.UseACSUB = (byte)response[4];
                ipsReply.UseWHSUB = (byte)response[5];
                ipsReply.UseEQSUB = (byte)response[6];
                ipsReply.IsWHLOCK = (byte)response[7];
                ipsReply.IsEQLOCK = (byte)response[8];

                ipsReply.ServiceKind = (int)response[9];
                ipsReply.ExpireDate = (int)response[10];
                ipsReply.ExtendedCharCreation = (int)response[11];
            }

            connection.Send(Shared.Opcode.IPC_GAME_AUTH_ACCOUNT, ipsReply);
        }

        public static void OnIPC_SET_ACCOUNT_STT(Connection connection, PacketReader reader)
        {
            var ipsSetAccountStt = reader.Packet<IPS_SET_ACCOUNT_STT>();

            AccountOPHandler.AuthStt(new Dictionary<string, object>()
            {
                { "usernum", ipsSetAccountStt.UserNum },
                { "groupidx", ipsSetAccountStt.GroupIdx },
                { "playtime", ipsSetAccountStt.PlayTime }
            });
        }
    }
}
