﻿using GlobalDBAgent.Daemons.GlobalDBAgent.Logic;
using Shared;
using Shared.Packet;
using Shared.Telepathy;
using System.Collections.Generic;
using static Shared.Protodef;

namespace GlobalDBAgent.Daemons.GlobalDBAgent.Proc
{
    static class GameSubpassword
    {
        public static void OnIPC_GAME_SUBPASSWORD_SET(Connection connection, PacketReader reader)
        {
            var ipsGameSubpasswordSet = reader.Packet<IPS_GAME_SUBPASSWORD_SET>();

            var ipsReply = new IPS_GAME_SUBPASSWORD_SET_REPLY();
            ipsReply.GroupIdx = ipsGameSubpasswordSet.GroupIdx;
            ipsReply.ServerIdx = ipsGameSubpasswordSet.ServerIdx;

            ipsReply.DstTick = ipsGameSubpasswordSet.SrcTick;
            ipsReply.DstCID = ipsGameSubpasswordSet.SrcCID;

            ipsReply.PwType = ipsGameSubpasswordSet.PwType;
            ipsReply.Mode = ipsGameSubpasswordSet.Mode;

            var resSubpasswordSet = SubpasswordOPHandler.SubpasswordSet(new Dictionary<string, object>()
            {
                { "usernum", ipsGameSubpasswordSet.UserNum },
                { "pwtype", ipsGameSubpasswordSet.PwType },
                { "password", ipsGameSubpasswordSet.Password }
            });

            if((byte)resSubpasswordSet[0] == 1 && ipsGameSubpasswordSet.Mode == (short)Typedef.SubpasswordSetMode.Create)
            {
                var resSubpasswordSetQA = SubpasswordOPHandler.SubpasswordSetQA(new Dictionary<string, object>()
                {
                    { "usernum", ipsGameSubpasswordSet.UserNum },
                    { "pwtype", ipsGameSubpasswordSet.PwType },
                    { "question", ipsGameSubpasswordSet.Question },
                    { "answer", ipsGameSubpasswordSet.Answer }
                });

                ipsReply.Status = (byte)resSubpasswordSetQA[0];
            }
            else
            {
                ipsReply.Status = (byte)resSubpasswordSet[0];
            }

            connection.Send(Shared.Opcode.IPC_GAME_SUBPASSWORD_SET, ipsReply);
        }

        public static void OnIPC_GAME_SUBPASSWORD_GET_QA(Connection connection, PacketReader reader)
        {
            var ipsGameSubpasswordGetQa = reader.Packet<IPS_GAME_SUBPASSWORD_GET_QA>();

            var ipsReply = new IPS_GAME_SUBPASSWORD_GET_QA_REPLY();
            ipsReply.GroupIdx = ipsGameSubpasswordGetQa.GroupIdx;
            ipsReply.ServerIdx = ipsGameSubpasswordGetQa.ServerIdx;

            ipsReply.DstTick = ipsGameSubpasswordGetQa.SrcTick;
            ipsReply.DstCID = ipsGameSubpasswordGetQa.SrcCID;

            ipsReply.PwType = ipsGameSubpasswordGetQa.PwType;

            var response = SubpasswordOPHandler.SubpasswordGetQA(new Dictionary<string, object>()
            {
                { "usernum", ipsGameSubpasswordGetQa.UserNum },
                { "pwtype", ipsGameSubpasswordGetQa.PwType }
            });

            if ((byte)response[0] == 1)
            {
                ipsReply.QuestionId = (int)response[1];
            }

            connection.Send(Shared.Opcode.IPC_GAME_SUBPASSWORD_GET_QA, ipsReply);
        }

        public static void OnIPC_GAME_SUBPASSWORD_ANSWER_QA(Connection connection, PacketReader reader)
        {
            var ipsGameSubpasswordAnswerQa = reader.Packet<IPS_GAME_SUBPASSWORD_ANSWER_QA>();

            var ipsReply = new IPS_GAME_SUBPASSWORD_ANSWER_QA_REPLY();
            ipsReply.GroupIdx = ipsGameSubpasswordAnswerQa.GroupIdx;
            ipsReply.ServerIdx = ipsGameSubpasswordAnswerQa.ServerIdx;

            ipsReply.DstTick = ipsGameSubpasswordAnswerQa.SrcTick;
            ipsReply.DstCID = ipsGameSubpasswordAnswerQa.SrcCID;

            ipsReply.PwType = ipsGameSubpasswordAnswerQa.PwType;

            var response = SubpasswordOPHandler.SubpasswordAnswerQA(new Dictionary<string, object>()
            {
                { "usernum", ipsGameSubpasswordAnswerQa.UserNum },
                { "pwtype", ipsGameSubpasswordAnswerQa.PwType },
                { "answer", ipsGameSubpasswordAnswerQa.Answer }
            });

            ipsReply.Status = (byte)response[0];

            if (ipsReply.Status == 0)
                ipsReply.TryNum = (byte)(int)response[1];

            connection.Send(Shared.Opcode.IPC_GAME_SUBPASSWORD_ANSWER_QA, ipsReply);
        }

        public static void OnIPC_GAME_SUBPASSWORD_AUTH(Connection connection, PacketReader reader)
        {
            var ipsGameSubpasswordAuth = reader.Packet<IPS_GAME_SUBPASSWORD_AUTH>();

            var ipsReply = new IPS_GAME_SUBPASSWORD_AUTH_REPLY();
            ipsReply.GroupIdx = ipsGameSubpasswordAuth.GroupIdx;
            ipsReply.ServerIdx = ipsGameSubpasswordAuth.ServerIdx;

            ipsReply.DstTick = ipsGameSubpasswordAuth.SrcTick;
            ipsReply.DstCID = ipsGameSubpasswordAuth.SrcCID;

            ipsReply.PwType = ipsGameSubpasswordAuth.PwType;
            ipsReply.IsFastAuth = ipsGameSubpasswordAuth.IsFastAuth;
            ipsReply.RememberHours = ipsGameSubpasswordAuth.RememberHours;

            var response = SubpasswordOPHandler.SubpasswordAuth(new Dictionary<string, object>()
            {
                { "usernum", ipsGameSubpasswordAuth.UserNum },
                { "pwtype", ipsGameSubpasswordAuth.PwType },
                { "password", ipsGameSubpasswordAuth.Password },
                { "remember", ipsGameSubpasswordAuth.RememberHours }
            });

            ipsReply.Status = (byte)response[0];

            if(ipsReply.Status == 0)
                ipsReply.TryNum = (byte)(int)response[1];

            connection.Send(Shared.Opcode.IPC_GAME_SUBPASSWORD_AUTH, ipsReply);
        }

        public static void OnIPC_GAME_SUBPASSWORD_DEL(Connection connection, PacketReader reader)
        {
            var ipsGameSubpasswordDel = reader.Packet<IPS_GAME_SUBPASSWORD_DEL>();

            var ipsReply = new IPS_GAME_SUBPASSWORD_DEL_REPLY();
            ipsReply.GroupIdx = ipsGameSubpasswordDel.GroupIdx;
            ipsReply.ServerIdx = ipsGameSubpasswordDel.ServerIdx;

            ipsReply.DstTick = ipsGameSubpasswordDel.SrcTick;
            ipsReply.DstCID = ipsGameSubpasswordDel.SrcCID;

            ipsReply.PwType = ipsGameSubpasswordDel.PwType;

            var response = SubpasswordOPHandler.SubpasswordDel(new Dictionary<string, object>()
            {
                { "usernum", ipsGameSubpasswordDel.UserNum },
                { "pwtype", ipsGameSubpasswordDel.PwType }
            });

            ipsReply.Status = (byte)response[0];

            connection.Send(Shared.Opcode.IPC_GAME_SUBPASSWORD_DEL, ipsReply);
        }
    }
}
