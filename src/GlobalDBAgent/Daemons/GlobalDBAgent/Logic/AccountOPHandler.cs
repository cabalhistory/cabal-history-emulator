﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalDBAgent.Daemons.GlobalDBAgent.Logic
{
    static class AccountOPHandler
    {
        public static object[] AuthAccount(Dictionary<string, object> parameters)
            => Program.DBAccountWrapper.Execute("h_sp_auth_account", parameters);

        public static int AuthStt(Dictionary<string, object> parameters)
            => Program.DBAccountWrapper.ExecuteNonQuery("h_sp_auth_stt", parameters);
    }
}
