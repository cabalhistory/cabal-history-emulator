﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalDBAgent.Daemons.GlobalDBAgent.Logic
{
    static class SubpasswordOPHandler
    {
        public static object[] SubpasswordSet(Dictionary<string, object> parameters)
            => Program.DBAccountWrapper.Execute("h_sp_subpassword_set", parameters);

        public static object[] SubpasswordSetQA(Dictionary<string, object> parameters)
            => Program.DBAccountWrapper.Execute("h_sp_subpassword_set_qa", parameters);

        public static object[] SubpasswordGetQA(Dictionary<string, object> parameters)
            => Program.DBAccountWrapper.Execute("h_sp_subpassword_get_qa", parameters);

        public static object[] SubpasswordAnswerQA(Dictionary<string, object> parameters)
            => Program.DBAccountWrapper.Execute("h_sp_subpassword_answer_qa", parameters);

        public static object[] SubpasswordAuth(Dictionary<string, object> parameters)
            => Program.DBAccountWrapper.Execute("h_sp_subpassword_auth", parameters);

        public static object[] SubpasswordDel(Dictionary<string, object> parameters)
            => Program.DBAccountWrapper.Execute("h_sp_subpassword_del", parameters);
    }
}
