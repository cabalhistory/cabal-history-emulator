﻿using System;
using System.IO;
using Shared;
using Shared.Database;

namespace GlobalDBAgent
{
    class Program
    {
        public static GlobalDBAgentDaemon GlobalDBAgentDaemon { get; private set; }

        public static MSSQLProcWrapper DBAccountWrapper { get; private set; }

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledException.HandleUnhandledException;

            // Load .ini configuration
            Config.Load($"GlobalDBAgent.ini");

            // Start logger service
            Logger.Start("GlobalDBAgent", Config.LogPath, Config.LogLevel);

            DBAccountWrapper = new MSSQLProcWrapper(Config.DBAccount_DataSource, Config.DBAccount_InitialCatalog, Config.DBAccount_UserId, Config.DBAccount_Password, "GlobalDBAgent");

            #region Daemons
            GlobalDBAgentDaemon = new GlobalDBAgentDaemon(Config.IP, Config.Port, Config.Threads, Config.Frequency);

            DaemonManager.Register(GlobalDBAgentDaemon);
            #endregion

            DaemonManager.StartAll();
        }
    }
}
