﻿using System;
using System.Net;
using Shared;

namespace GlobalDBAgent
{
    class Config
    {
        public static IPAddress IP { get; set; }
        public static ushort Port { get; set; }

        public static Logger.LOG_LEVEL LogLevel { get; set; }
        public static string LogPath { get; set; }
        public static ushort Threads { get; set; }
        public static ushort Frequency { get; set; }

        public static string DBAccount_DataSource{ get; set; }
        public static string DBAccount_InitialCatalog { get; set; }
        public static string DBAccount_UserId { get; set; }
        public static string DBAccount_Password { get; set; }

        public static void Load(string iniFileName)
        {
            IniReader parser = new IniReader(iniFileName);

            const string LISTEN_SECTION = "Listen";
            IP = IPAddress.Parse(parser.GetValue(LISTEN_SECTION, "IP"));
            Port = ushort.Parse(parser.GetValue(LISTEN_SECTION, "Port"));

            const string SERVER_SECTION = "Server";
            Enum.TryParse(parser.GetValue(SERVER_SECTION, "LogLevel"), out Logger.LOG_LEVEL _logLevel);
            
            LogLevel = _logLevel;
            LogPath = parser.GetValue(SERVER_SECTION, "LogPath");
            Threads = ushort.Parse(parser.GetValue(SERVER_SECTION, "Threads"));
            Frequency = ushort.Parse(parser.GetValue(SERVER_SECTION, "Frequency"));

            const string DATABASE_SECTION = "DBAccount";
            DBAccount_DataSource = parser.GetValue(DATABASE_SECTION, "DataSource");
            DBAccount_InitialCatalog = parser.GetValue(DATABASE_SECTION, "InitialCatalog");
            DBAccount_UserId = parser.GetValue(DATABASE_SECTION, "UserID");
            DBAccount_Password = parser.GetValue(DATABASE_SECTION, "Password");
        }
    }
}
